from datetime import datetime

import yaml

from bfg_tools.library import analyse_transactions, xlsx_reader, \
    xlsx_writer


def main():
    config = "daily_task_analyser.yml"

    with open(config, 'r', encoding="utf-8") as stream:
        cur_config = yaml.load(stream)

    session = analyse_transactions.TransactionSession()

    last_session_file = "control_last_session.yml"
    with open(last_session_file, 'r', encoding="utf-8") as stream:
        last_session = yaml.load(stream)["session"]

    main_session = session.get_main_session(url=cur_config["url"],
                                            login=str(cur_config["login"]),
                                            password=str(
                                                cur_config["password"]))

    if main_session == last_session:
        return

    session.read_from_rest(url=cur_config["url"],
                           login=str(cur_config["login"]),
                           password=str(cur_config["password"]),
                           tz=None, session="main")

    input_path = cur_config["input_path"]
    output_path = cur_config["output_path"]
    date_prefix = cur_config["date_prefix"]

    for xlsx in cur_config["filenames"]:
        input_xlsx = input_path + date_prefix + xlsx + ".xlsx"
        try:
            daily_task = xlsx_reader.XlsxReadData(xlsx=input_xlsx, title_row=2)
        except FileNotFoundError:
            print("Не найдено ССЗ на подразделение", xlsx)
            continue
        overwrite = True

        for equipment in daily_task.worksheets:
            for row in daily_task.worksheets[equipment]:
                del row["ROW_NUMBER"]
                if row["НОМЕР ЗАКАЗА"] is None:
                    row["НАИМЕНОВАНИЕ"] = "(?) " + row["НАИМЕНОВАНИЕ"]
                    continue
                batch = session.get_batch_with_order_name(
                    row["НОМЕР ЗАКАЗА"].replace("-", "_"))
                if batch is None:
                    operation_now = "ЗАКАЗ ЗАВЕРШЕН"
                else:
                    operation_now = batch.current_operation
                    row["КОЛ-ВО"] = str(row["КОЛ-ВО"]) + "=>" + str(
                        round(batch.initial_batch_size, ndigits=2))
                row["ТЕКУЩАЯ ОПЕРАЦИЯ"] += "=>" + operation_now[0:20]
                if (row["НОМЕР ОПЕРАЦИИ"][0:20] < operation_now[0:20]) and (
                        operation_now != "НЕ ЗАПУЩЕНА"):
                    row["НАИМЕНОВАНИЕ"] = "(+) " + row["НАИМЕНОВАНИЕ"]
                else:
                    row["НАИМЕНОВАНИЕ"] = "(-) " + row["НАИМЕНОВАНИЕ"]
            if date_prefix == "":
                report_date = str(datetime.now())[0:10] + "_"
            else:
                report_date = date_prefix
            write_xlsx = output_path + "control_" + report_date + xlsx + ".xlsx"
            xlsx_title = "({}) Отчет о выполнении производственного задания: {}".format(
                date_prefix, str(equipment))

            xlsx_writer.xlsx_write_data(xlsx=write_xlsx,
                                        worksheet=str(equipment)[0:30],
                                        data=daily_task.worksheets[equipment],
                                        title=xlsx_title,
                                        overwrite=overwrite)
            overwrite = False

    with open(last_session_file, 'w') as outfile:
        yaml.dump({"session": main_session}, outfile, default_flow_style=False)


if __name__ == "__main__":
    main()
