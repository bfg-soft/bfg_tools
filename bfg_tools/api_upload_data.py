import json
from time import sleep

import requests
from requests_toolbelt.multipart.encoder import MultipartEncoder


class BFGSession(object):

    def __init__(self, url):
        self.session = requests.Session()
        self.url = url
        self.user = 0

    def connect(self, login, password):
        data = dict()
        data["action"] = "login"
        data["data"] = dict()
        data["data"]["login"] = login
        data["data"]["password"] = password
        print("Попытка подключения к системе:")
        request = self.session.post(url=self.url + "/action/login",
                                    data=json.dumps(data)).json()
        print(request)
        self.user = request["data"]

    def import_data(self, import_batch):

        for import_type in import_batch:
            file = import_batch[import_type]["file"]
            path = import_batch[import_type]["path"]
            file_type = import_batch[import_type]["file_type"]
            data = MultipartEncoder(
                fields={
                    'data': (file, open(path + file, 'rb'), file_type)
                }
            )

            headers = {'Content-Type': data.content_type}
            print("Загружается файл {}{} на сервер".format(path, file))
            request = self.session.post(url=self.url + "/action/upload",
                                        headers=headers, data=data).json()
            import_batch[import_type]["server_filename"] = request["data"]

        data = dict()
        data["action"] = "update"
        data["data"] = {
            "session": 1,
            "subfolder": "Null",
            "types": {
            }
        }
        for import_type in import_batch:
            clear = import_batch[import_type]["clear"]
            filename = import_batch[import_type]["server_filename"]
            data["data"]["types"][import_type] = {"clear": clear,
                                                  "filename": filename}

        print("Начинаем импорт")
        print(data)
        print(json.dumps(data))
        r = self.session.post(url=self.url + "/action/import",
                              data=json.dumps(data))
        print(r)
        print(r.json())

        # проверяем начался ли импорт
        while self.session.get(
                url=self.url + "/action/import").status_code != 200:
            sleep(1)
            pass
        print("Импорт начался")

        # проверяем завершился ли импорт
        while self.session.get(
                url=self.url + "/action/import").status_code == 200:
            sleep(1)
            pass
        print("Импорт завершен")

    def import_plan(self, import_batch):
        from requests_toolbelt.multipart.encoder import MultipartEncoder
        from datetime import datetime

        file = import_batch["file"]
        path = import_batch["path"]
        file_type = import_batch["file_type"]
        data = MultipartEncoder(
            fields={
                'data': (file, open(path + file, 'rb'), file_type)
            }
        )

        headers = {'Content-Type': data.content_type}
        print("Загружается файл {}{} на сервер".format(path, file))
        request = self.session.post(url=self.url + "/action/upload",
                                    headers=headers, data=data).json()

        import_batch["server_filename"] = request["data"]
        data = {
            "action": "dynamic",
            "aggregate_order_entries": False,
            "filepath": import_batch["server_filename"]
        }
        request = self.session.post(url=self.url + "/action/import/plan",
                                    headers=headers, data=json.dumps(data))
        print(request.json()['errors'])
        plan = request.json()["data"]

        data = {
            "plan": [{"name": str(datetime.now())[0:16] + " " + file, "type": 1,
                      "user_id": self.user}]
        }
        request = self.session.post(url=self.url + "/rest/plan",
                                    headers=headers, data=json.dumps(data))
        print(request.json())
        plan_id = request.json()["plan"][0]["id"]

        orders = {
            "order": []
        }
        for row in plan:
            orders["order"].append({
                "date_from": row["date"],
                "date_to": row["date"],
                "name": row["name"],
                "plan_id": plan_id,
                "priority": row["priority"],
                "time_from": "00:00",
            })
        request = self.session.post(url=self.url + "/rest/order",
                                    headers=headers, data=json.dumps(orders))
        print(request.json())

        order_id = {}
        for row in request.json()["order"]:
            order_id[row["name"]] = row["id"]
        order_entries = {
            "order_entry": []
        }
        for row in plan:
            for details in row["details"]:
                order_entries["order_entry"].append({
                    "entity_id": details["id"],
                    "amount": details["amount"],
                    "order_id": order_id[row["name"]],
                    "priority": 0
                })
        request = self.session.post(url=self.url + "/rest/order_entry",
                                    headers=headers,
                                    data=json.dumps(order_entries))
        print(request.json())

    def import_wip(self, import_batch):
        from requests_toolbelt.multipart.encoder import MultipartEncoder

        file = import_batch["file"]
        path = import_batch["path"]
        file_type = import_batch["file_type"]
        data = MultipartEncoder(
            fields={
                'data': (file, open(path + file, 'rb'), file_type)
            }
        )

        headers = {'Content-Type': data.content_type}
        print("Загружается файл {}{} на сервер".format(path, file))
        request = self.session.post(url=self.url + "/action/upload",
                                    headers=headers, data=data).json()

        import_batch["server_filename"] = request["data"]
        data = {
            "action": "start",
            "data": {
                "filepath": import_batch["server_filename"]
            }
        }
        request = self.session.post(url=self.url + "/action/import/state",
                                    headers=headers, data=json.dumps(data))
        print(request.json()['errors'])
        wip_import_session = request.json()["data"]["id"]

        data = {
            "action": "accept",
            "data": wip_import_session
        }
        request = self.session.post(url=self.url + "/action/import/state",
                                    headers=headers, data=json.dumps(data))
        print(request.json())

    def close(self):
        print("Закрываем HTTP-сессию")
        self.session.close()


if __name__ == "__main__":
    import yaml

    config_file = "import.yml"
    with open(config_file, 'r', encoding="utf-8") as stream:
        config = yaml.load(stream)

    session = BFGSession(url=config["url"])

    try:
        session.connect(login=config["login"], password=config["password"])
        session.import_data(config["import_batch"])
        session.import_plan(config["plan"])
        session.import_wip(config["wip"])
    finally:
        session.close()
