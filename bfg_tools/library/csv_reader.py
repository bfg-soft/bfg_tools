import csv
from time import time


class CsvReadData(object):
    def __init__(self, csv_file, columns):
        t = time()
        print("Начинаем считывание csv файла \"{}\"".format(csv_file))
        with open(csv_file, newline='', encoding='utf-8') as file:
            dialect = csv.Sniffer().sniff(
                file.read(),
                delimiters=[',', ';']
            )
            file.seek(0)
            ws = csv.reader(file, dialect=dialect)
            # ws = csv.reader(file, delimiter=";")
            title = {}
            first_row = next(ws)
            for col in columns:
                title[col] = first_row.index(columns[col])

            self.rows = []
            i = 1

            for _row in ws:
                i += 1
                result_row = {"ROW_NUMBER": i}
                row_is_not_empty = False
                for key in title:
                    if _row[title[key]] is not None:
                        row_is_not_empty = True
                    result_row[key] = _row[title[key]]
                if row_is_not_empty:
                    self.rows.append(result_row)
        print("Считывание из файла \"{}\" "
              "успешно завершено за {:.2f} секунд".format(csv_file, time() - t))

        file.close()


if __name__ == "__main__":

    BATCH_SIZE_COLUMNS = {"CODE": "BATCH", "BATCH_SIZE": "BATCH_SIZE"}
    SPEC_COLUMNS = {"PARENT_CODE": "PARENT_CODE", "PARENT_NAME": "PARENT_NAME",
                    "PARENT_IDENTITY": "PARENT_IDENTITY", "CODE": "CODE",
                    "NAME": "NAME",
                    "IDENTITY": "IDENTITY", "AMOUNT": "AMOUNT"}
    TECH_COLUMNS = {"PRODUCT": "ROUTE_ID", "T_SHT": "T_SHT",
                    "T_PZ": "T_PZ", "EQUIPMENT": "EQUIPMENT_ID",
                    "DEPARTMENT": "DEPT_ID"}

    test_data = CsvReadData("../input/btk/batch_size.csv", BATCH_SIZE_COLUMNS)
    for row in test_data.rows:
        print(row)

    test_data = CsvReadData("../input/btk/spec.csv", SPEC_COLUMNS)
    for row in test_data.rows:
        print(row)

    test_data = CsvReadData("../input/btk/tech-nal.csv", TECH_COLUMNS)
    for row in test_data.rows:
        print(row)
