from collections import defaultdict
from datetime import datetime, timedelta

from bfg_tools.library import csv_reader, xlsx_reader, xlsx_writer


class Entities(object):
    SPEC_COLUMNS = {"PARENT_CODE": "PARENT_CODE", "PARENT_NAME": "PARENT_NAME",
                    "PARENT_IDENTITY": "PARENT_IDENTITY", "CODE": "CODE",
                    "NAME": "NAME",
                    "IDENTITY": "IDENTITY", "AMOUNT": "AMOUNT"}

    BATCH_SIZE_COLUMNS = {"CODE": "CODE", "BATCH_SIZE": "BATCH_SIZE"}

    ROUTE_COLUMNS = {"CODE": "CODE", "ROUTE_ID": "ROUTE_ID",
                     "BATCH_SIZE": "BATCH_SIZE"}

    def __init__(self):
        self.rows = {}
        self.routes = {}  # key - route_id, value - Product

    def get_product_with_route_id(self, route_id):
        try:
            return self.routes[route_id]
        except KeyError:
            return None

    def get_product_with_id(self, product_id):
        try:
            return self.rows[str(product_id)]
        except KeyError:
            return None

    def get_product_with_code_name(self, code, name):
        if (code is None) and (name is None):
            return None
        for entity in self.rows:
            if (str(self.rows[entity].code) == str(code)) and (
                    str(self.rows[entity].name) == str(name)):
                return self.rows[entity]
        return None

    def get_product_with_code(self, code):
        if code is None:
            return None
        for entity in self.rows:
            if str(self.rows[entity].code) == str(code):
                return self.rows[entity]
        return None

    def add(self, product_id, product_code, product_name):
        if product_id is None:
            return
        if product_name is None:
            product_name = ""
        if product_code is None:
            product_code = ""
        if self.get_product_with_id(product_id) is None:
            self.rows[str(product_id)] = Product(str(product_id),
                                                 str(product_code),
                                                 str(product_name))

    def add_tech_proc(self, product_id, route_id, batch_size):
        if product_id is None:
            return
        product = self.get_product_with_id(product_id)
        if product is None:
            print("Не найдено изделие с CODE {}".format(product_id))
        else:
            product.tech_proc[route_id] = TechProcess(name=route_id,
                                                      batch_size=batch_size)
            self.routes[route_id] = product

    def read_spec_from_xlsx(self, xlsx, spec_columns=None,
                            spec_worksheet="spec"):

        if spec_columns is None:
            spec_columns = self.SPEC_COLUMNS

        try:
            xlsx_data = \
            xlsx_reader.XlsxReadData(xlsx=xlsx, worksheet=spec_worksheet,
                                     columns=spec_columns).worksheets[
                spec_worksheet]
        except:
            print("Считать как xlsx не удалось, пробуем считать как csv")
            xlsx_data = csv_reader.CsvReadData(csv_file=xlsx,
                                               columns=spec_columns).rows

        for row in xlsx_data:
            if row["ROW_NUMBER"] == 1:
                continue
            parent_id = row["PARENT_CODE"]
            parent_code = row["PARENT_IDENTITY"]
            parent_name = row["PARENT_NAME"]
            child_id = row["CODE"]
            child_code = row["IDENTITY"]
            child_name = row["NAME"]
            try:
                if type(row["AMOUNT"]) is str:
                    amount = float(row["AMOUNT"].replace(",", "."))
                else:
                    amount = float(row["AMOUNT"])
            except(ValueError, AttributeError):
                amount = 0
                print("Неверное значение ячейки столбца "
                      "AMOUNT в строке {}, значение в ячейке \"{}\"".format(
                    row["ROW_NUMBER"], row["AMOUNT"]))

            self.add(parent_id, parent_code, parent_name)
            self.add(child_id, child_code, child_name)

            parent = self.get_product_with_id(parent_id)
            child = self.get_product_with_id(child_id)

            if (parent is not None) and (child is not None):
                parent.add_child(child, amount)

    def read_route_from_xlsx(self, xlsx, route_columns=None,
                             route_worksheet="entity-route"):

        if route_columns is None:
            route_columns = self.ROUTE_COLUMNS

        try:
            xlsx_data = xlsx_reader.XlsxReadData(xlsx=xlsx,
                                                 worksheet=route_worksheet,
                                                 columns=route_columns).worksheets[
                route_worksheet]
        except:
            print("Считать как xlsx не удалось, пробуем считать как csv")
            xlsx_data = csv_reader.CsvReadData(csv_file=xlsx,
                                               columns=route_columns).rows

        for row in xlsx_data:
            if row["ROW_NUMBER"] == 1:
                continue
            product_id = row["CODE"]
            route_id = row["ROUTE_ID"]
            batch_size = row["BATCH_SIZE"]

            self.add_tech_proc(product_id=product_id, route_id=route_id,
                               batch_size=batch_size)


class Product(object):  # изделие
    def __init__(self, prod_id, code, name):
        self.id = prod_id
        self.code = code
        self.name = name
        self.tech_proc = {}
        self.product_entry = []

    def __repr__(self):
        return self.description

    @property
    def is_raw_material(self):
        if self.tech_proc or self.product_entry:
            return False
        else:
            return True

    @property
    def is_simplest_part(self):
        for each in self.product_entry:
            if each.product.is_raw_material:
                pass
            else:
                return False
        return True

    @property
    def description(self):
        return "ИД:" + self.id + " Шифр:" + self.code + " Имя:" + self.name

    def add_techproc(self, name):
        self.tech_proc[name] = TechProcess(name=name)
        return self.tech_proc[name]

    def specification_report(self, _i=0):
        report = "\n" + "-" * _i
        if _i == 0:
            report += self.description + "\n" + "-"
        _i += 1

        for child in self.product_entry:
            report += str(child.product.description) + " Количество:" + str(
                child.amount) \
                      + "шт" + child.product.specification_report(_i)

        _i -= 1
        return report

    def add_child(self, child, amount):
        if child is None:
            return
        if amount == 0:
            return
        if not (self.is_product_in_spec(child)):
            self.product_entry.append(ProductEntry(child, amount))

    def is_product_in_spec(self, child):
        for spec_row in self.product_entry:
            if spec_row.product == child:
                return True
        return False

    def simplify_specification_dict(self, amount, _report=None):
        """
        Пробую сделать функцию, где на выходе словарь, состоящий только из деталей низшего уровня изделия
        :param amount: количество деталей в спецификации
        :param _report:
        :return:
        """
        if _report is None:
            _report = defaultdict(float)

        if self.is_raw_material:
            return _report

        if self.is_simplest_part:
            _report[self] += amount
            return _report

        for spec_row in self.product_entry:
            _report.update(spec_row.product.simplify_specification_dict(
                amount * spec_row.amount, _report))

        return _report


class ProductEntry(object):
    def __init__(self, product, amount):
        self.product = product
        self.amount = amount


class WorkCenter(object):  # рабочий центр
    def __init__(self, wc_id, name="", amount=1):
        self.id = wc_id
        self.name = name
        self.amount = amount

    def workload(self, plan, date_from=datetime.min, date_to=datetime.max):
        # загрузка рабочего центра по заданному плану
        workload = 0
        for entry in plan:
            if date_from < entry.date <= date_to:
                workload += entry.workload(self)
        return workload

    def workload_percents(self, plan,
                          period_hours):  # загрузка выбранного рабочего центра в %
        return round(self.workload(plan) * 100 / self.amount / period_hours, 1)


class Factory(
    object):  # завод, который состоит из большого количества рабочих центров
    EQUIPMENT_COLUMNS = {"EQUIPMENT_ID": "EQUIPMENT_ID", "AMOUNT": "AMOUNT",
                         "NAME": "NAME", "DEPT_ID": "DEPT_ID"}

    def __init__(self):
        self.workcenter = []

    def __iter__(self):
        return iter(self.workcenter)

    def __repr__(self):
        report = ""
        for each_workcenter in self:
            report += each_workcenter.id
        return report

    def add(self, wc_id, amount, name=None):
        if name is None:
            name = wc_id
        if not self.get_workcenter_with_id(wc_id):
            self.workcenter.append(
                WorkCenter(wc_id=str(wc_id), amount=amount, name=str(name)))
        else:
            self.get_workcenter_with_id(wc_id).amount += amount

    def get_workcenter_with_id(self, wc_id):
        for each_work_center in self:
            if each_work_center.id == wc_id:
                return each_work_center

    def workcenter_sorted_list(self, plan):
        from operator import itemgetter
        wc_workload = {}
        for each_work_center in self:
            wc_workload[each_work_center] = each_work_center.workload(plan) / \
                                            each_work_center.amount / \
                                            each_work_center.utilization_factor
        # wc_workload = sorted(wc_workload.items(), key=lambda t: t[1], reverse=True)
        wc_workload = sorted(wc_workload.items(), key=itemgetter(1),
                             reverse=True)
        wc_sorted_list = []
        for i in wc_workload:
            wc_sorted_list.append(i[0])
        return wc_sorted_list

    def read_from_xlsx(self, xlsx, equipment_columns=None,
                       equipment_worksheet="equipment"):
        if equipment_columns is None:
            equipment_columns = self.EQUIPMENT_COLUMNS

        try:
            xlsx_data = xlsx_reader.XlsxReadData(xlsx=xlsx,
                                                 worksheet=equipment_worksheet,
                                                 columns=equipment_columns).worksheets[
                equipment_worksheet]
        except:
            xlsx_data = csv_reader.CsvReadData(csv_file=csv,
                                               columns=equipment_columns).rows

        for row in xlsx_data:
            if row["ROW_NUMBER"] == 1:
                continue
            equipment_id = str(row["EQUIPMENT_ID"]) + "_" + str(row["DEPT_ID"])
            try:
                amount = int(row["AMOUNT"])
            except(ValueError, AttributeError):
                print("Неверное значение ячейки столбца "
                      "AMOUNT в строке {}, значение в ячейке \"{}\"".format(
                    row["ROW_NUMBER"], row["AMOUNT"]))
                continue
            name = row["NAME"]

            self.add(wc_id=equipment_id, amount=amount, name=name)


class Operation(object):  # операция
    def __init__(self, work_center, t_sht, t_pz, name):
        self.work_center = work_center
        self.t_sht = t_sht
        self.t_pz = t_pz
        self.name = name

    def operation_hours(self, batch_size):
        if batch_size == 0:
            print(self)
        return self.t_sht + self.t_pz / batch_size


class TechProcess(object):  # техпроцесс
    def __init__(self, name, batch_size):
        self.name = name
        self.batch_size = batch_size
        self.operations = []

    def __repr__(self):
        report = ""
        for operation in self:
            report += str(operation.work_center.id) + " Тпз:" + str(
                operation.t_pz) + "ч. Тшт" \
                      + str(operation.t_sht) + "\n"
        return report

    def __iter__(self):
        return iter(self.operations)

    def workload(self,
                 work_center):  # загрузка выбранного рабочего центра по техпроцессу
        workload = 0
        for operation in self:
            if operation.work_center == work_center:
                workload += operation.operation_hours(self.batch_size)
        return workload

    def add_operation(self, work_center, t_pz, t_sht,
                      name):  # добавляем операцию в технологический процесс
        self.operations.append(
            Operation(work_center=work_center, t_pz=t_pz, t_sht=t_sht,
                      name=name))


class PlanEntry(object):  # строка плана
    def __init__(self, product, amount, order_name, initial_name=None,
                 initial_order=None, initial_code=None,
                 entry_date=datetime.now()):
        self.product = product
        self.amount = amount
        self.date = entry_date
        self.order_name = order_name
        self.initial_name = initial_name
        self.initial_code = initial_code
        self.initial_order = initial_order

    def __repr__(self):
        return self.product.description + "Количество:" + str(
            self.amount) + " Дата:" + str(self.date) + "\n"

    @property
    def description(self):
        return self.product.description + "Количество:" + str(
            self.amount) + " \nДата:" + str(self.date) + "\n \n"

    @property
    def labor(self):
        return self.product.labor_per_part * self.amount


class Plan(object):  # план
    PLAN_COLUMNS = {"ORDER": "ORDER", "CODE": "CODE",
                    "AMOUNT": "AMOUNT", "DATE": "DATE",
                    "NAME": "NAME", "IDENTITY": "IDENTITY"}
    PLAN_TITLE = ("ORDER", "IDENTITY", "NAME", "AMOUNT", "DATE", "CODE")

    def __init__(self, entities, name=""):
        self.name = name
        self.plan_entry = []
        self.entities = entities

    def __repr__(self):
        report = "План:" + self.name + "\n"
        for entry in self.plan_entry:
            report += entry.description
        return report

    def __iter__(self):
        return iter(self.plan_entry)

    def concatenate_with(self, other):
        for entry in other.plan_entry:
            self.plan_entry.append(entry)

    @property
    def start_date(self):
        result = datetime.max
        for entry in self.plan_entry:
            result = min(result, entry.date)
        return result

    @property
    def finish_date(self):
        result = datetime.min
        for entry in self.plan_entry:
            result = max(result, entry.date)
        return result

    @property
    def number_of_entries(self):  # количество строк в плане
        return len(self.plan_entry)

    @property
    def total_amount(self):  # количество штук/метров в плане
        total_amount = 0
        for entry in self.plan_entry:
            total_amount += entry.amount
        return total_amount

    @property
    def product_set(self):  # множество уникальных продуктов в плане
        # product_set = set()
        # for entry in self.plan_entry:
        #     product_set.add(entry.product)
        # return product_set
        return set(self.plan_entry)

    @property
    def sorted_product_set(
            self):  # множество уникальных продуктов в плане, сортированных по трудоемкости
        return sorted(self.product_set,
                      key=lambda product: product.labor_per_plan(self),
                      reverse=True)

    @property
    def sorted_plan_entry_set(self):
        # строки плана, сортированные по трудомекости
        return sorted(self.plan_entry, key=lambda entry: entry.labor,
                      reverse=True)

    def exclude_dayoffs(self, weekdays, holidays_dates):
        add_days = 0
        for entry in sorted(self.plan_entry, key=lambda x: x.date):
            entry.date += timedelta(days=add_days)
            while (entry.date.isoweekday() in weekdays) or (
                    entry.date in holidays_dates):
                add_days += 1
                entry.date += timedelta(days=1)

    def simple_plan(self):
        """
        На входе план со сложными изделиями, на выходе план только с деталями в количестве,
        необходимом для производства всех изделий плана
        :return:
        """
        result_plan = Plan(self.entities)
        for entry in self.plan_entry:
            for part in entry.product.simplify_specification_dict(entry.amount):
                amount = \
                entry.product.simplify_specification_dict(entry.amount)[part]
                part_id = part.id
                result_plan.add(product_id=part_id, amount=amount,
                                date=entry.date,
                                order_name=entry.order_name)
        return result_plan

    def sorted_by_bottleneck_plan_entry_set(self, factory=Factory()):
        # строки плана, сортированные по загрузке самых загруженных классов РЦ
        wc_sorted_list = factory.workcenter_sorted_list(self)
        result = self.plan_entry
        # сортируем последовательно по 5 самым загруженным рабочим центрам
        for i in range(5, -1, -1):
            result = sorted(result,
                            key=lambda entry: entry.workload(wc_sorted_list[i]),
                            reverse=True)
        result = sorted(result, key=lambda entry: entry.date)

        return result

    def add(self, product_id, amount=0.0, date=datetime.now(),
            code=None, name=None, order_name=None):  # добавить строку в план
        product = self.entities.get_product_with_id(product_id)
        if product is None:
            print(
                "Не найден продукт {}, пробуем определять по поляем IDENTITY и NAME".format(
                    product_id))
            product = self.entities.get_product_with_code_name(code, name)
            if product is None:
                print(
                    "Не найден продукт {} {}, пробуем определить только по IDENTITY".format(
                        code, name))
                product = self.entities.get_product_with_code(code)
                if product is None:
                    print("Не найден продукт {}".format(code))
                    return
                else:
                    print("Найден продукт {}".format(product.id))
            else:
                print("Найден продукт {}".format(product.id))
        if amount == 0:
            print("Нулевое количество")
        self.plan_entry.append(
            PlanEntry(product=product, amount=amount, entry_date=date,
                      order_name=order_name,
                      initial_name=name, initial_code=code,
                      initial_order=order_name))

    def clean(self):  # удалить все строки из плана
        self.plan_entry.clear()

    def remove(self, entry):
        self.plan_entry.remove(entry)

    def copy(self, from_date, to_date):
        """
        Копирует строки плана в другой план, с ограничением по дате запуска строк плана
        :param from_date: от этой даты
        :type from_date: datetime
        :param to_date: и до этой даты
        :type to_date: datetime
        :return: возвращает план
        """
        temp_copy = Plan(self.entities)
        for each in self.plan_entry:
            if from_date <= each.date < to_date:
                temp_copy.add(product_id=each.product.id, amount=each.amount,
                              date=each.date, order_name=each.order_name,
                              code=each.initial_code, name=each.initial_name)
        return temp_copy

    def workload(self, work_center):
        workload = 0
        for entry in self:
            workload += entry.workload(work_center)
        return workload

    def amount_of_products(self, product, date_from=datetime.min,
                           date_to=datetime.max):
        count = 0
        for entry in self:
            if (entry.product == product) and (
                    date_from < entry.date <= date_to):
                count += entry.amount
        return count

    def labor_per_product(self, product):
        labor = 0
        for entry in self:
            if entry.product == product:
                labor += entry.product.labor_per_part * entry.amount
        return labor

    def get_product_with_id(self, product_id):
        for entry in self:
            if entry.product.id == product_id:
                return entry.product
        return None

    def read_plan_from_xlsx(self, xlsx, plan_columns=None,
                            plan_worksheet="plan"):  # считываем из xlsx файла план
        if plan_columns is None:
            plan_columns = self.PLAN_COLUMNS
        xlsx_data = \
        xlsx_reader.XlsxReadData(xlsx=xlsx, worksheet=plan_worksheet,
                                 columns=plan_columns).worksheets[
            plan_worksheet]
        for row in xlsx_data:
            if row["ROW_NUMBER"] == 1:
                continue
            order_name = row["ORDER"]
            product_id = row["CODE"]
            product_name = row["NAME"]
            product_code = row["IDENTITY"]
            try:
                if type(row["AMOUNT"]) is str:
                    entry_amount = float(row["AMOUNT"].replace(",", "."))
                else:
                    entry_amount = float(row["AMOUNT"])
            except(ValueError, AttributeError):
                print("Неверное значение ячейки столбца "
                      "AMOUNT в строке {}, значение ячейки {}".format(
                    row["ROW_NUMBER"], row["AMOUNT"]))
                continue

            try:
                entry_date = datetime.combine(datetime.date(row["DATE"]),
                                              datetime.time(row["DATE"]))
            except(TypeError, ValueError, AttributeError):
                entry_date = datetime.now()
                print("Неверное значение ячейки столбца "
                      "DATE в строке {}, значение ячейки {}".format(
                    row["ROW_NUMBER"], row["DATE"]))
            if entry_amount > 0:
                self.add(product_id=product_id, amount=entry_amount,
                         date=entry_date,
                         code=product_code, name=product_name,
                         order_name=order_name)
            else:
                print("Изделие {} из заказа {} в плане с "
                      "нулевым количеством. При расчете учтено не будет".format(
                    product_id, order_name))

    def unique_entities_in_plan_list(self):
        product_basket = defaultdict(
            lambda: {"Amount": 0.0, "Date": datetime.max})
        for entry in self:
            product_basket[entry.product]["Amount"] += entry.amount
            product_basket[entry.product]["Date"] = min(entry.date,
                                                        product_basket[
                                                            entry.product][
                                                            "Date"])
        return product_basket

    def collapse(self):
        """
        метод для "сворачиания" плана -- в нем остаются только уникальные изделия
        с суммами по количеству и самой ранней датой из всех строк
        """
        product_basket = self.unique_entities_in_plan_list()

        self.clean()  # почистим все entry в заказе
        for entry in product_basket.keys():  # и заполним заново
            self.add(entry.id,
                     product_basket[entry]["Amount"],
                     product_basket[entry]["Date"])

        print("Сворачивание плана проведено успешно")

    def moveto(self, other_plan, entry, date=datetime.now(), log=False,
               number_of_batches=1):

        num = 1

        while num <= number_of_batches:

            other_plan.add(product_id=entry.product.id,
                           amount=min(entry.product.batch_size,
                                      round(entry.amount, 3)),
                           date=date, order_name=entry.order_name,
                           code=entry.initial_code, name=entry.initial_name)
            if log:
                print(
                    "Добавлено изделие {} из заказа {}, размер партии {}".format(
                        entry.product.code, entry.order_name,
                        min(entry.product.batch_size,
                            round(entry.amount, 3))))
            if entry.amount > entry.product.batch_size:
                entry.amount -= entry.product.batch_size
            else:
                entry.amount = 0
                self.remove(entry)
                break

            num += 1

    def write_to_xlsx(self, xlsx, columns=PLAN_TITLE,
                      worksheet="plan"):  # запись плана в xlsx
        from bfg_tools.library import xlsx_writer

        i = 0
        report = []
        for entry in self:
            i += 1
            row = {}
            row["INITIAL_ORDER"] = str(entry.initial_order)
            row["INITIAL_IDENTITY"] = str(entry.initial_code)
            row["INITIAL_NAME"] = str(entry.initial_name)
            row["AMOUNT"] = entry.amount
            row["DATE"] = entry.date
            row["ORDER"] = str(entry.order_name) + "_" + str(
                i) + "_" + entry.product.code
            row["IDENTITY"] = entry.product.code
            row["NAME"] = entry.product.name
            row["CODE"] = entry.product.id
            report.append(row)
        xlsx_writer.xlsx_write_data(xlsx=xlsx, worksheet=worksheet, data=report)


class CalcSession(object):
    TECH_COLUMNS = {"ROUTE_ID": "ROUTE_ID", "T_SHT": "T_SHT",
                    "T_PZ": "T_PZ", "EQUIPMENT": "EQUIPMENT_ID",
                    "DEPARTMENT": "DEPT_ID", "NOP": "NOP", "NAME": "NAME"}

    def __init__(self):
        self.factory = Factory()
        self.entities = Entities()
        self.plan = Plan(self.entities)

    def read_tech_from_xlsx(self, xlsx, tech_worksheet, tech_columns=None):

        if tech_columns is None:
            tech_columns = self.TECH_COLUMNS

        try:
            xlsx_data = xlsx_reader.XlsxReadData(xlsx=xlsx,
                                                 worksheet=tech_worksheet,
                                                 columns=tech_columns).worksheets[
                tech_worksheet]
        except:
            print("Считать как xlsx не удалось, пробуем считать как csv")
            xlsx_data = csv_reader.CsvReadData(csv_file=xlsx,
                                               columns=tech_columns).rows

        for row in xlsx_data:
            if row["ROW_NUMBER"] == 1:
                continue
            route_id = row["ROUTE_ID"]
            name = row["NAME"]
            equipment_id = str(row["EQUIPMENT"]) + "_" + str(row["DEPARTMENT"])

            try:
                if type(row["T_SHT"]) is str:
                    t_sht = float(row["T_SHT"].replace(",", "."))
                else:
                    t_sht = float(row["T_SHT"])
            except(ValueError, AttributeError):
                t_sht = 0
                print("Неверное значение ячейки столбца "
                      "T_SHT в строке {}, значение в ячейке \"{}\"".format(
                    row["ROW_NUMBER"], row["T_SHT"]))

            try:
                if type(row["T_PZ"]) is str:
                    t_pz = float(row["T_PZ"].replace(",", "."))
                else:
                    t_pz = float(row["T_PZ"])
            except(ValueError, AttributeError):
                t_pz = 0
                print("Неверное значение ячейки столбца "
                      "T_PZ в строке {}, значение в ячейке \"{}\"".format(
                    row["ROW_NUMBER"], row["T_PZ"]))

            product = self.entities.get_product_with_route_id(route_id)
            equipment = self.factory.get_workcenter_with_id(equipment_id)
            if product is not None:
                product.tech_proc[route_id].add_operation(work_center=equipment,
                                                          t_sht=t_sht,
                                                          t_pz=t_pz, name=name)
            else:
                print("Не найдено изделие для маршрута с ROUTE_ID {}".format(
                    route_id))


# проверяем библиотеку
if __name__ == "__main__":

    alloys = xlsx_reader.XlsxReadData(xlsx="../input/kuzocm/CODE_ALLOY.xlsx",
                                      worksheet="Sheet1").worksheets["Sheet1"]
    session = CalcSession()
    session.entities.read_spec_from_xlsx(xlsx="../input/kuzocm/kuzocm1.xlsx",
                                         spec_worksheet="spec")
    session.plan.read_plan_from_xlsx(xlsx="../input/kuzocm/plan.xlsx",
                                     plan_worksheet="plan (2)")
    session.factory.read_from_xlsx(xlsx="../input/kuzocm/kuzocm1.xlsx",
                                   equipment_worksheet="equipment")
    session.entities.read_route_from_xlsx(xlsx="../input/kuzocm/kuzocm1.xlsx",
                                          route_worksheet="марш")
    session.read_tech_from_xlsx(xlsx="../input/kuzocm/kuzocm1.xlsx",
                                tech_worksheet="tech-nal")

    spec_report = []
    route_report = []
    tech_report = []
    plan_report = []
    from_date = datetime(year=2018, month=11, day=1, hour=0, minute=0, second=0)
    to_date = datetime(year=2018, month=1, day=11, hour=0, minute=0, second=0)
    plan_copy = session.plan.copy(from_date=from_date, to_date=to_date)
    for entry in plan_copy.simple_plan():
        product = entry.product
        break_test = False
        for route_id in product.tech_proc:
            route = product.tech_proc[route_id]
            for operation in route:
                if (operation.name == "Плавление и литье") and (
                        operation.work_center.name[0:3] == "Илк"):
                    break_test = True
                    plan_report.append({
                        "ORDER": entry.order_name,
                        "IDENTITY": entry.product.code,
                        "NAME": entry.product.name,
                        "AMOUNT": entry.amount,
                        "CODE": entry.product.id,
                        "DATE": entry.date
                    })
                if break_test:
                    break
            if break_test:
                break

    for product in session.plan.simple_plan().unique_entities_in_plan_list():
        for route_id in product.tech_proc:
            route = product.tech_proc[route_id]
            for operation in route:
                if (operation.name == "Плавление и литье") and (
                        operation.work_center.name[0:3] == "Илк"):
                    spec_row = {
                        "PARENT_CODE": product.id,
                        "PARENT_IDENTITY": product.code,
                        "PARENT_NAME": product.name,
                        "CODE": "",
                        "IDENTITY": "Отливка",
                        "NAME": "",
                        "AMOUNT": 1
                    }
                    for child in product.product_entry:
                        if child.product.code == "Отливка":
                            spec_row["NAME"] = child.product.name
                            spec_row["CODE"] = child.product.id

                    if spec_row["NAME"] == "":
                        for each_alloy in alloys:
                            if (each_alloy["СПЛАВ"] + " ") in spec_row[
                                "PARENT_NAME"]:
                                spec_row["NAME"] = each_alloy["КАТЕГОРИЯ"]
                                spec_row["CODE"] = each_alloy["СПЛАВ"]
                    spec_report.append(spec_row)

                    route_row = {
                        "CODE": spec_row["CODE"],
                        "ROUTE_ID": spec_row["CODE"] + "_" + spec_row[
                            "PARENT_CODE"] + "_" + operation.work_center.name,
                        "ALTERNATIVE": ""
                    }
                    route_report.append(route_row)

                    if operation.work_center.name == "Илк-1.6 с2 №3_16_71 1_2":
                        casting_time_hours = 1200 / 60 * operation.t_sht + operation.t_pz / 60
                    else:
                        casting_time_hours = 2400 / 60 * operation.t_sht + operation.t_pz / 60

                    tech_row = {
                        "CODE": route_row["CODE"],
                        "ROUTE_ID": route_row["ROUTE_ID"],
                        "ID": route_row["ROUTE_ID"],
                        "NAME": operation.name,
                        "NOP": "005",
                        "DEPT_ID": "1_2",
                        "EQUIPMENT_ID": operation.work_center.name,
                        "T_PZ": operation.t_pz,
                        "T_NAL": 0,
                        "T_SHT": operation.t_sht,
                        "CASTING_TIME_HR": casting_time_hours
                    }

                    tech_report.append(tech_row)

    spec_report = sorted(spec_report, key=lambda i: i["PARENT_CODE"])
    xlsx_writer.xlsx_write_data(xlsx="../input/kuzocm/ilk.xlsx",
                                worksheet="spec",
                                data=spec_report, overwrite=True)

    route_report = sorted(route_report, key=lambda i: i["CODE"])
    xlsx_writer.xlsx_write_data(xlsx="../input/kuzocm/ilk.xlsx",
                                worksheet="entity-route",
                                data=route_report, overwrite=False)

    tech_report = sorted(tech_report, key=lambda i: i["ROUTE_ID"])
    xlsx_writer.xlsx_write_data(xlsx="../input/kuzocm/ilk.xlsx",
                                worksheet="tech-nal",
                                data=tech_report, overwrite=False)

    plan_report = sorted(plan_report, key=lambda i: i["DATE"])
    xlsx_writer.xlsx_write_data(xlsx="../input/kuzocm/ilk.xlsx",
                                worksheet="plan",
                                data=plan_report, overwrite=False)
