from datetime import datetime, timedelta

from bfg_tools.library import csv_reader, csv_writer, xlsx_reader


class PlanEntry(object):  # строка плана
    def __init__(self, product_id, product_identity, product_name,
                 amount, order_name, row_number, entry_date=datetime.now(),
                 priority=1):
        self.product_id = product_id
        self.product_identity = product_identity
        self.product_name = product_name
        self.amount = amount
        self.date = entry_date
        self.order_name = order_name
        self.row_number = row_number
        self.priority = priority


class Plan(object):  # план
    PLAN_COLUMNS = {"ORDER": "ORDER", "CODE": "CODE",
                    "AMOUNT": "AMOUNT", "DATE": "DATE"}
    SPECIAL_ORDERS_COLUMNS = {"ORDER": "ORDER", "PRIORITY": "PRIORITY"}
    PLAN_TITLE = ("ORDER", "IDENTITY", "NAME", "AMOUNT", "DATE", "CODE")

    def __init__(self, name=""):
        self.name = name
        self.plan_entry = []

    def add(self, product_id, row_number=0, amount=0.0,
            date=datetime.now(), product_identity=None, product_name=None,
            order_name=None,
            priority=1):
        # добавить строку в план
        self.plan_entry.append(
            PlanEntry(product_id=product_id, product_identity=product_identity,
                      product_name=product_name, amount=amount,
                      entry_date=date, order_name=order_name,
                      row_number=row_number,
                      priority=priority))

    def get_entries_list_with_order_name(self, order_name):
        report = []
        for entry in self.plan_entry:
            if entry.order_name == order_name:
                report.append(entry)
        return report

    def read_plan_from_xlsx(self, xlsx, plan_columns=None,
                            plan_worksheet="plan"):  # считываем из xlsx файла план
        from openpyxl.utils import exceptions

        if plan_columns is None:
            plan_columns = self.PLAN_COLUMNS

        try:
            xlsx_data = xlsx_reader.XlsxReadData(xlsx=xlsx,
                                                 worksheet=plan_worksheet,
                                                 columns=plan_columns).worksheets[
                plan_worksheet]
        except exceptions.InvalidFileException:
            xlsx_data = csv_reader.CsvReadData(csv_file=xlsx,
                                               columns=plan_columns).rows
        for row in xlsx_data:
            if row["ROW_NUMBER"] == 1:
                continue
            row_number = row["ROW_NUMBER"]
            order_name = row["ORDER"]
            product_id = row["CODE"]
            product_name = row["NAME"]
            product_code = row["IDENTITY"]
            try:
                if type(row["AMOUNT"]) is str:
                    entry_amount = float(row["AMOUNT"].replace(",", "."))
                else:
                    entry_amount = float(row["AMOUNT"])
            except(ValueError, AttributeError):
                print("Неверное значение ячейки столбца "
                      "AMOUNT в строке {}, значение ячейки {}".format(
                    row["ROW_NUMBER"], row["AMOUNT"]))
                continue
            try:
                entry_date = datetime.combine(datetime.date(row["DATE"]),
                                              datetime.time(row["DATE"]))
            except(TypeError, ValueError, AttributeError):
                try:
                    entry_date = datetime.strptime(str(row["DATE"])[0:10],
                                                   "%Y-%m-%d")
                except TypeError:
                    entry_date = datetime.now()
                    print("Неверное значение ячейки столбца "
                          "DATE в строке {}, значение ячейки {}".format(
                        row["ROW_NUMBER"], row["DATE"]))
            self.add(product_id=product_id, amount=entry_amount,
                     date=entry_date,
                     product_identity=product_code, product_name=product_name,
                     order_name=order_name, row_number=row_number)

    def read_priority_from_xlsx(self, xlsx, special_orders_columns=None,
                                special_orders_worksheet="orders"):  # считываем из xlsx файла план
        from openpyxl.utils import exceptions

        if special_orders_columns is None:
            special_orders_columns = self.SPECIAL_ORDERS_COLUMNS

        try:
            xlsx_data = xlsx_reader.XlsxReadData(xlsx=xlsx,
                                                 worksheet=special_orders_worksheet,
                                                 columns=special_orders_columns).worksheets[
                special_orders_worksheet]
        except exceptions.InvalidFileException:
            xlsx_data = csv_reader.CsvReadData(csv_file=xlsx,
                                               columns=special_orders_columns).rows

        for row in xlsx_data:
            if row["ROW_NUMBER"] == 1:
                continue
            order_name = row["ORDER"]
            priority = row["PRIORITY"]
            if not (self.get_entries_list_with_order_name(order_name)):
                print("Заказ {} в плане не найден".format(order_name))

            for entry in self.get_entries_list_with_order_name(order_name):
                entry.priority = priority

    def sort_order(self):
        result_plan = Plan()
        entries = sorted(self.plan_entry, key=lambda i: i.row_number)
        entries = sorted(entries, key=lambda i: int(i.priority), reverse=True)
        for entry in entries:
            if int(entry.priority) == 0:
                date = datetime.now() + timedelta(10)
            else:
                date = entry.date
            result_plan.add(product_id=entry.product_id,
                            row_number=entry.row_number, amount=entry.amount,
                            date=date, product_identity=entry.product_identity,
                            product_name=entry.product_name,
                            order_name=entry.order_name,
                            priority=entry.priority)

        return result_plan

    def write_to_csv(self, csv_file, columns=PLAN_TITLE):  # запись плана в xlsx

        i = 0
        report = []
        for entry in self.plan_entry:
            i += 1
            row = dict()
            row["ORDER"] = entry.order_name
            row["IDENTITY"] = entry.product_identity
            row["NAME"] = entry.product_name
            row["AMOUNT"] = entry.amount
            row["DATE"] = entry.date
            row["CODE"] = entry.product_id
            row["PRIORITY"] = entry.priority
            row["ROW_NUMBER"] = entry.row_number
            report.append(row)
        csv_writer.csv_write_data(csv_file=csv_file, data=report)
