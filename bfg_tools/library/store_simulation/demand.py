from bfg_tools.library.store_simulation.entities import Entity
from bfg_tools.library.store_simulation.entities_list import EntitiesList
from bfg_tools.library.store_simulation.entity_demand import EntityDemand


class Demand(object):
    def __init__(self):
        self.entities_list = EntitiesList()
        self.entities_demand = {}

    def read_from_xlsx(self, xlsx, worksheet):
        from bfg_tools.library import xlsx_reader
        from datetime import datetime

        data = \
        xlsx_reader.XlsxReadData(xlsx=xlsx, worksheet=worksheet).worksheets[
            worksheet]

        for row in data:
            if row["Буфер"] is None:
                initial_buffer = 0
                initial_store = 0
                # continue
            else:
                initial_buffer = row["Буфер"]
                initial_store = row["Буфер"] * 1.5
            cur_entity = self.entities_list.list[row["Сплав"]] = Entity(
                identity=row["Сплав"], name=row["Сплав"],
                code=row["Группа сплавов"],
                initial_buffer=initial_buffer,
                initial_store=initial_store,
                supply_period_days=row["Срок поставки"],
                batch_size=row["BATCH_SIZE"])
            cur_demand = self.entities_demand[cur_entity] = EntityDemand(
                cur_entity)
            for key in row:
                try:
                    cur_date = datetime.strptime(key, "%Y/%m/%d")
                except ValueError:
                    continue
                cur_demand.release[cur_date] = row[key]

    def simulate_entity_store(self, entity):
        import math
        cur_demand = self.entities_demand[entity].release
        cur_buffer = entity.buffer.buffer
        start_date = min(cur_demand.keys())
        previous_store_size = entity.store_size[
            start_date] = entity.initial_store
        previous_buffer = entity.buffer.buffer[
            start_date] = entity.initial_buffer
        previous_deficit = 0
        check_green = 0
        check_red = 0
        supply_order = 0
        for cur_date in cur_demand:
            entity.buffer.buffer[cur_date] = previous_buffer
            entity.store_size[cur_date] = previous_store_size
            entity.store_size[cur_date] -= cur_demand[cur_date]
            entity.store_size[cur_date] -= previous_deficit
            entity.store_size[cur_date] += supply_order
            entity.store_size[cur_date] = round(entity.store_size[cur_date],
                                                ndigits=1)
            if entity.store_size[cur_date] > cur_buffer[cur_date] * 2:
                check_green += 1
            else:
                check_green = 0

            if entity.store_size[cur_date] < cur_buffer[cur_date]:
                check_red += 1
            else:
                check_red = 0

            if entity.store_size[cur_date] < cur_buffer[cur_date]:
                red_line = cur_buffer[cur_date] * 2
                number_of_batches = math.ceil((red_line - entity.store_size[
                    cur_date]) / entity.batch_size)
                entity.supply_in_process[cur_date] = round(
                    number_of_batches * entity.batch_size, ndigits=1)
            else:
                entity.supply_in_process[cur_date] = 0
            if entity.store_size[cur_date] < 0:
                entity.deficit[cur_date] = -entity.store_size[cur_date]
                entity.store_size[cur_date] = 0
            else:
                entity.deficit[cur_date] = 0
            previous_buffer = cur_buffer[cur_date]
            previous_store_size = entity.store_size[cur_date]
            supply_order = entity.supply_in_process[cur_date]
            previous_deficit = entity.deficit[cur_date]

    def simulate_store(self):
        for cur_entity in self.entities_list.list.values():
            self.simulate_entity_store(cur_entity)

    def generate_store_report(self):
        report = []
        for cur_entity in self.entities_list.list.values():
            row = {
                "Сплав": cur_entity.identity,
                "Буфер": cur_entity.initial_buffer
            }
            for date in cur_entity.store_size:
                row[date] = cur_entity.store_size[date]
            report.append(row)
        return report

    def generate_supply_orders_report(self):
        report = []
        for cur_entity in self.entities_list.list.values():
            row = {
                "Сплав": cur_entity.identity,
                "Буфер": cur_entity.initial_buffer
            }
            for date in cur_entity.supply_in_process:
                row[date] = cur_entity.supply_in_process[date]
            report.append(row)
        return report

    def generate_deficit_report(self):
        report = []
        for cur_entity in self.entities_list.list.values():
            row = {
                "Сплав": cur_entity.identity,
                "Буфер": cur_entity.initial_buffer
            }
            for date in cur_entity.deficit:
                row[date] = cur_entity.deficit[date]
            report.append(row)
        return report


if __name__ == "__main__":
    from bfg_tools.library import xlsx_writer

    demand = Demand()
    demand.read_from_xlsx("../../input/kuzocm/casting_alloy_report_1.xlsx",
                          "ten_days (3)")
    demand.simulate_store()
    xlsx_writer.xlsx_write_data("../../input/kuzocm/stores_med.xlsx", "stores",
                                data=demand.generate_store_report())
    xlsx_writer.xlsx_write_data("../../input/kuzocm/stores_med.xlsx", "orders",
                                data=demand.generate_supply_orders_report(),
                                overwrite=False)
    xlsx_writer.xlsx_write_data("../../input/kuzocm/stores_med.xlsx", "deficit",
                                data=demand.generate_deficit_report(),
                                overwrite=False)
