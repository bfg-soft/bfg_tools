from bfg_tools.library.store_simulation.buffer import Buffer


class Entity(object):
    def __init__(self, identity, code, name, initial_buffer, initial_store,
                 supply_period_days, batch_size):
        self.identity = identity
        self.code = code
        self.name = name
        self.initial_buffer = initial_buffer
        self.initial_store = initial_store
        self.buffer = Buffer(self)
        self.store_size = {}
        self.supply_in_process = {}
        self.supply_period_days = supply_period_days
        self.batch_size = batch_size
        self.deficit = {}
