from bfg_tools.library import xlsx_reader


class CastingShop(object):
    """"
    Класс литейный цех, состоит из станков
    """

    def __init__(self, name):
        """

        :param name: Наименование подразделения
        :type name: str
        """
        self.name = name
        self.casting_machines = {}  # {CastingEquipment.identity: CastingEquipment}

    def read_equipment_from_xlsx(self, xlsx, worksheet):
        data = \
        xlsx_reader.XlsxReadData(xlsx=xlsx, worksheet=worksheet).worksheets[
            worksheet]
        for row in data:
            self.casting_machines[str(row["ID"])] = CastingEquipment(
                identity=str(row["ID"]),
                name=str(row["NAME"]),
                class_identity=str(row["EQUIPMENT_ID"]),
                batch_size=float(row["BATCH_SIZE"]),
                initial_alloy=str(row["INITIAL_ALLOY"]).upper())


class CastingEquipment(object):
    """
    Класс литейная машина
    """

    def __init__(self, identity, name, class_identity, batch_size,
                 initial_alloy):
        """

        :param identity: идентификатор станка
        :type identity: str
        :param name: наименование станка
        :type name: str
        :param class_identity: идентификатор класса
        :type class_identity: str
        :param batch_size: размер партии на литейной машине
        :type batch_size: float
        :param initial_alloy: идентификатор сплава, на который в исходном состоянии налажен станок
        :type initial_alloy: str
        """
        self.identity = identity
        self.name = name
        self.class_identity = class_identity
        self.batch_size = batch_size
        self.setups = {}
        self.initial_alloy_id = initial_alloy
        self.initial_alloy = None
        self.casting_time = {}

    def __repr__(self):
        report = "Печь {}_{} класса {}, размер партии {}, исходное состояние наладки {}"
        report = report.format(self.identity, self.name, self.class_identity,
                               self.batch_size, self.initial_alloy_id)
        return report

    def get_setup(self, previous_alloy, alloy, no_setup_number_of_batches):
        if previous_alloy is None:
            setup_time = 0
            setup_volume = 0
        elif previous_alloy not in self.setups:
            setup_time = no_setup_number_of_batches * self.casting_time[
                previous_alloy]
            setup_volume = no_setup_number_of_batches * self.batch_size
        elif alloy not in self.setups[previous_alloy]:
            setup_time = no_setup_number_of_batches * self.casting_time[
                previous_alloy]
            setup_volume = no_setup_number_of_batches * self.batch_size
        else:
            number_of_setup_batches = self.setups[previous_alloy][
                                          alloy] / self.batch_size
            setup_time = number_of_setup_batches * self.casting_time[
                previous_alloy]
            setup_volume = number_of_setup_batches * self.batch_size
        return setup_time, setup_volume
