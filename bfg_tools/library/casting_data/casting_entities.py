class Alloy(object):
    """
    Класс сплав
    """

    def __init__(self, name, alloy_group):
        self.name = name
        self.alloy_group = alloy_group

    def __repr__(self):
        return self.name + "_" + self.alloy_group


class CastBar(object):
    """
    Класс отливка
    """

    def __init__(self, diameter, alloy):
        """

        :param diameter: диаметр отливки
        :param alloy: сплав отливки
        """
        self.identity = alloy.name + "_" + str(diameter)
        self.code = alloy.alloy_group
        self.name = alloy.name
        self.diameter = diameter
        self.alloy = alloy
        self.raw_materials = {}

    def __repr__(self):
        report = "Отливка из сплава {} диаметром {}, относится к группе сплавов {}"
        report = report.format(self.alloy.name, self.diameter,
                               self.alloy.alloy_group)
        return report
