from collections import defaultdict
from datetime import datetime
from math import ceil

from bfg_tools.library import xlsx_reader, xlsx_writer
from bfg_tools.library.casting_data.casting_entities import Alloy, CastBar
from bfg_tools.library.casting_data.casting_shop import CastingShop


class Plan(object):
    """
    План, который надо распределить по печкам и составить расписание
    """

    def __init__(self, name, casting_shop):
        """

        :param name: наименование плана
        :type name: str
        :param casting_shop: цех, в котором изготавливаем план
        :type casting_shop: CastingShop
        """
        self.name = name
        self.plan_entries_amount = defaultdict(float)  # {CastBar: amount}
        self.casting_shop = casting_shop  # CastingShop
        self.entities = {}  # {CastBar.identity: CastBar}
        self.schedules = {}  # {session_number: {CastingEquipment: Schedule}}
        self.alloys = {}  # {Alloy.name: Alloy}
        self.setups = {}

    def add_cast_bar(self, diameter, alloy_name, alloy_group):
        alloy = self.add_alloy(alloy_name, alloy_group)
        cast_bar_identity = alloy_name + "_" + str(diameter)
        if cast_bar_identity not in self.entities:
            self.entities[cast_bar_identity] = CastBar(diameter=diameter,
                                                       alloy=alloy)
        return self.entities[cast_bar_identity]

    def add_alloy(self, name, alloy_group):
        if name not in self.alloys:
            self.alloys[name] = Alloy(name, alloy_group)
        return self.alloys[name]

    def get_alloy_with_name(self, alloy_name):
        if alloy_name in self.alloys:
            return self.alloys[alloy_name]
        if alloy_name.upper() in self.alloys:
            return self.alloys[alloy_name.upper()]
        return None

    def alloy_plan(self):
        report = defaultdict(float)
        for entry in self.plan_entries_amount:
            report[entry.alloy] += self.plan_entries_amount[entry]
        return report

    def alloy_group_plan(self):
        report = defaultdict(float)
        for entry in self.plan_entries_amount:
            report[entry.alloy.alloy_group] += self.plan_entries_amount[entry]
        return report

    def get_cast_bar_with_identity(self, identity):
        return self.entities[identity]

    def read_spec_from_xlsx(self, xlsx, worksheet):
        data = \
        xlsx_reader.XlsxReadData(xlsx=xlsx, worksheet=worksheet).worksheets[
            worksheet]
        for row in data:
            cast_bar = self.add_cast_bar(diameter=int(row["PARENT_DIAMETER"]),
                                         alloy_name=str(
                                             row["PARENT_ALLOY"]).upper(),
                                         alloy_group=str(row["PARENT_NAME"]))
            cast_bar.raw_materials[row["IDENTITY"]] = {
                "NAME": str(row["NAME"]),
                "AMOUNT": float(row["AMOUNT"])
            }
        for equipment in self.casting_shop.casting_machines.values():
            equipment.initial_alloy = self.get_alloy_with_name(
                equipment.initial_alloy_id)

    def read_tech_from_xlsx(self, xlsx, worksheet):
        data = \
        xlsx_reader.XlsxReadData(xlsx=xlsx, worksheet=worksheet).worksheets[
            worksheet]
        for row in data:
            cast_bar = self.get_cast_bar_with_identity(str(row["CODE"]).upper())
            for equipment in self.casting_shop.casting_machines.values():
                if equipment.class_identity == row["EQUIPMENT_ID"]:
                    equipment.casting_time[cast_bar.alloy] = row[
                        "CASTING_TIME_HR"]

    def read_setups_from_xlsx(self, xlsx, worksheet):
        data = \
        xlsx_reader.XlsxReadData(xlsx=xlsx, worksheet=worksheet).worksheets[
            worksheet]
        absent_alloys = []
        for row in data:
            alloy_from = self.get_alloy_with_name(str(row["FROM"]).upper())
            alloy_to = self.get_alloy_with_name(str(row["TO"]).upper())
            if alloy_from is None:
                if row["FROM"] not in absent_alloys:
                    print(
                        "Не найден сплав {}, указанный в строке {} таблицы наладок".format(
                            row["FROM"],
                            row["ROW_NUMBER"]))
                    absent_alloys.append(row["FROM"])
                continue
            if alloy_to is None:
                if row["TO"] not in absent_alloys:
                    print(
                        "Не найден сплав {}, указанный в строке {} таблицы наладок".format(
                            row["TO"],
                            row["ROW_NUMBER"]))
                    absent_alloys.append(row["TO"])
                continue
            check = False
            for equipment in self.casting_shop.casting_machines.values():
                if equipment.class_identity == row["EQUIPMENT_ID"]:
                    if alloy_from not in equipment.setups:
                        equipment.setups[alloy_from] = {}
                    equipment.setups[alloy_from][alloy_to] = row["AMOUNT"]
                    check = True
            if not check:
                print("Оборудование {} не найдено".format(row["EQUIPMENT_ID"]))

    def read_plan_from_xlsx(self, xlsx, worksheet, date_from=datetime.min,
                            date_to=datetime.max):
        data = \
        xlsx_reader.XlsxReadData(xlsx=xlsx, worksheet=worksheet).worksheets[
            worksheet]
        for row in data:
            if date_from.date() <= row["DATE"].date() <= date_to.date():
                cast_bar = self.get_cast_bar_with_identity(
                    str(row["CODE"]).upper())
                self.plan_entries_amount[cast_bar] += float(row["AMOUNT"])

    def write_plan_report_to_xlsx(self, xlsx, worksheet):
        alloy_group_report = self.alloy_group_plan()
        alloy_report = self.alloy_plan()
        result_report = []
        for alloy_group in sorted(alloy_group_report,
                                  key=alloy_group_report.get, reverse=True):
            alloy_group_row = {
                "ГРУППА СПЛАВОВ": alloy_group,
                "СПЛАВ": "",
                "ОБЪЕМ": alloy_group_report[alloy_group]
            }
            for equipment_id in self.casting_shop.casting_machines:
                alloy_group_row["ПЛАВКИ_" + equipment_id] = 0
                alloy_group_row["ЧАСЫ_" + equipment_id] = 0
            for alloy in sorted(alloy_report, key=alloy_report.get,
                                reverse=True):
                if alloy.alloy_group == alloy_group:
                    row = {
                        "ГРУППА СПЛАВОВ": alloy_group,
                        "СПЛАВ": alloy.name,
                        "ОБЪЕМ": alloy_report[alloy]
                    }
                    for equipment_id in self.casting_shop.casting_machines:
                        casting_machine = self.casting_shop.casting_machines[
                            equipment_id]
                        if alloy not in casting_machine.casting_time:
                            row["ПЛАВКИ_" + equipment_id] = "---"
                            row["ЧАСЫ_" + equipment_id] = "---"
                            continue
                        number_of_batches = ceil(
                            alloy_report[alloy] / casting_machine.batch_size)
                        row["ПЛАВКИ_" + equipment_id] = number_of_batches
                        batch_casting_time = casting_machine.casting_time[alloy]
                        row[
                            "ЧАСЫ_" + equipment_id] = number_of_batches * batch_casting_time
                        alloy_group_row[
                            "ПЛАВКИ_" + equipment_id] += number_of_batches
                        alloy_group_row[
                            "ЧАСЫ_" + equipment_id] += number_of_batches * batch_casting_time
                    result_report.append(row)
            result_report.append(alloy_group_row)
        xlsx_writer.xlsx_write_data(xlsx=xlsx, worksheet=worksheet,
                                    data=result_report)
