import json

import requests
from dateutil import parser


def curl_read(url, login, password):
    data = dict()
    data["action"] = "login"
    data["data"] = dict()
    data["data"]["login"] = login
    data["data"]["password"] = password
    data = json.dumps(data)
    print(data)
    headers = {'content-type': 'application/json'}
    s = requests.Session()
    s.post(url=url + "action/login", headers=headers, data=data)
    r = s.get(url=url + "/action/primary_simulation_session").json()

    schedule_request = "/rest/collection/simulation_equipment?" \
                       "order_by=simulation_operation_task_equipment.simulation_operation_task.start_date&asc=true&" \
                       "order_by=simulation_operation_task_equipment.simulation_operation_task_id&asc=true&" \
                       "with=equipment&with_strict=false&with=equipment_class&" \
                       "with=simulation_operation_task_equipment&with=department&" \
                       "with=simulation_operation_task_equipment.simulation_operation_task&" \
                       "with=simulation_operation_task_equipment.simulation_operation_task.simulation_entity_batch&" \
                       "with=simulation_operation_task_equipment.simulation_operation_task.operation&" \
                       "with=simulation_operation_task_equipment.simulation_operation_task.operation.entity_route&" \
                       "with=simulation_operation_task_equipment.simulation_operation_task.simulation_entity_batch.entity&" \
                       "with=simulation_operation_task_equipment.simulation_operation_task.simulation_entity_batch.simulation_order_entity_batch&" \
                       "with_strict=false&with=simulation_operation_task_equipment.simulation_operation_task.simulation_entity_batch.simulation_order_entity_batch.order&" \
                       "filter={{simulation_session_id eq {}}}".format(
        r["data"])

    from bfg_tools.library import analyse_transactions

    calc_session = analyse_transactions.TransactionSession()

    r = s.get(url=url + schedule_request, headers=headers).json()

    report = {}
    for key in r:
        print(key)
        if key != "meta":
            report[key] = {}
            for row in r[key]:
                report[key].update({row["id"]: row})

    equipment_class_report = report["equipment_class"]
    department_report = report["department"]
    equipment_report = report["equipment"]
    simulation_equipment_report = report["simulation_equipment"]
    entity_report = report["entity"]
    operation_report = report["operation"]
    simulation_entity_batch_report = report["simulation_entity_batch"]
    simulation_operation_task_report = report["simulation_operation_task"]
    simulation_operation_task_equipment_report = report[
        "simulation_operation_task_equipment"]

    for row in simulation_operation_task_equipment_report.values():
        simulation_operation_task_id = row["simulation_operation_task_id"]
        simulation_entity_batch_id = \
        simulation_operation_task_report[simulation_operation_task_id][
            "simulation_entity_batch_id"]
        entity_id = simulation_entity_batch_report[simulation_entity_batch_id][
            "entity_id"]

        product = calc_session.add_product(entity_report[entity_id]["identity"])

        simulation_operation_task_equipment_id = row["id"]
        operation_id = \
        simulation_operation_task_report[simulation_operation_task_id][
            "operation_id"]

        operation = calc_session.add_operation(
            str(operation_report[operation_id]["nop"]) + "_" +
            str(operation_report[operation_id]["name"]))

        simulation_equipment_id = simulation_operation_task_equipment_report[
            simulation_operation_task_equipment_id][
            "simulation_equipment_id"]
        equipment_id = simulation_equipment_report[simulation_equipment_id][
            "equipment_id"]
        equipment_class_id = equipment_report[equipment_id][
            "equipment_class_id"]
        department_id = equipment_report[equipment_id]["department_id"]

        equipment = calc_session.add_equipment(
            department_report[department_id]["identity"] + "_" +
            equipment_class_report[equipment_class_id]["identity"] + "_" +
            equipment_class_report[equipment_class_id]["name"])

        batch_identity = \
        simulation_entity_batch_report[simulation_entity_batch_id]["identity"]
        batch = calc_session.add_batch(batch_identity, product, False)

        start_time = parser.parse(
            simulation_operation_task_report[simulation_operation_task_id][
                "start_date"])
        stop_time = parser.parse(
            simulation_operation_task_report[simulation_operation_task_id][
                "stop_date"])
        batch_size = \
        simulation_operation_task_report[simulation_operation_task_id][
            "entity_amount"]
        batch.add_transaction(start_time=start_time, stop_time=stop_time,
                              equipment=equipment,
                              size=batch_size, operation=operation,
                              operation_number=str(
                                  operation_report[operation_id]["nop"]))

        for batch in calc_session.batches.values():
            batch.sort_operations()

    return calc_session


if __name__ == "__main__":
    from bfg_tools.library import xlsx_writer
    from bfg_tools.library import yml_config

    session = curl_read("http://b.bfg-soft-2.mykom.ru/", "stolov", "1")

    cur_config = yml_config.ScheduleAnalyserConfig("../schedule_analyser.yml")

    date_from = cur_config.rules["from"]
    date_to = cur_config.rules["to"]

    plan = sorted(session.get_plan(date_from, date_to), key=lambda i: i["DATE"])
    xlsx_writer.xlsx_write_data(xlsx=cur_config.output_plan.file,
                                worksheet=cur_config.output_plan.worksheet,
                                data=plan)

    launch_report = sorted(session.launch_report(date_from, date_to),
                           key=lambda i: i["DATE"])
    xlsx_writer.xlsx_write_data(xlsx=cur_config.output_production_launch.file,
                                worksheet=cur_config.output_production_launch.worksheet,
                                data=launch_report)

    release_report = sorted(session.release_report(date_from, date_to),
                            key=lambda i: i["DATE"])
    xlsx_writer.xlsx_write_data(xlsx=cur_config.output_production_release.file,
                                worksheet=cur_config.output_production_release.worksheet,
                                data=release_report)

    equipment_report = sorted(session.equipment_report(date_from, date_to),
                              key=lambda i: i["EQUIPMENT_ID"])
    xlsx_writer.xlsx_write_data(xlsx=cur_config.output_equipment.file,
                                worksheet=cur_config.output_equipment.worksheet,
                                data=equipment_report)

    product_report = sorted(session.product_report(date_from, date_to),
                            key=lambda i: i["NUMBER_OF_BATCHES"],
                            reverse=True)
    xlsx_writer.xlsx_write_data(xlsx=cur_config.output_product.file,
                                worksheet=cur_config.output_product.worksheet,
                                data=product_report)
