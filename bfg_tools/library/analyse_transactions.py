from datetime import datetime, time, timedelta, timezone

from bfg_tools.library import xlsx_reader


class Batch(object):
    OPERATION_TYPES = {0: "ОПЕРАЦИЯ", 1: "ОПЕРАЦИЯ", 2: "ОПЕРАЦИЯ"}

    def __init__(self, batch_id, product, order, current_operation,
                 sum_batch_size_in_subtask=True):
        self.batch_id = batch_id
        self.product = product
        self.order = order
        self.sum_batch_size_in_subtask = sum_batch_size_in_subtask
        self.transactions = []
        self.current_operation = current_operation
        self.order_start_date = None

    @property
    def production_time(self):
        return self.finish_date - self.start_date

    @property
    def start_date(self):
        batch_start_time = datetime.max.replace(tzinfo=timezone.utc)
        for transaction in self.transactions:
            batch_start_time = min(batch_start_time, transaction.start_time)
        return batch_start_time

    @property
    def finish_date(self):
        batch_stop_time = datetime.min.replace(tzinfo=timezone.utc)
        for transaction in self.transactions:
            batch_stop_time = max(batch_stop_time, transaction.stop_time)
        return batch_stop_time

    @property
    def start_date_as_str(self):
        return {
            'month': '{}_{}'.format(
                str(self.start_date.date().year),
                str(self.start_date.date().month).zfill(2)
            ),
            'week': '{}_{}'.format(
                str(self.start_date.date().year),
                str(self.start_date.date().isocalendar()[1]).zfill(2)
            ),
            'day': '{}_{}_{}'.format(
                str(self.start_date.date().year),
                str(self.start_date.date().month).zfill(2),
                str(self.start_date.date().day).zfill(2)
            ),
        }

    @property
    def finish_date_as_str(self):
        return {
            'month': '{}_{}'.format(
                str(self.finish_date.date().year),
                str(self.finish_date.date().month).zfill(2)
            ),
            'week': '{}_{}'.format(
                str(self.finish_date.date().year),
                str(self.finish_date.date().isocalendar()[1]).zfill(2)
            ),
            'day': '{}_{}_{}'.format(
                str(self.finish_date.date().year),
                str(self.finish_date.date().month).zfill(2),
                str(self.finish_date.date().day).zfill(2)
            ),
        }

    @property
    def final_batch_size(self):
        transactions = sorted(self.transactions, key=lambda i: i.stop_time)
        if self.sum_batch_size_in_subtask:
            return transactions[-1].sum_size
        else:
            return transactions[-1].average_size

    @property
    def initial_batch_size(self):
        transactions = sorted(self.transactions, key=lambda i: i.start_time)
        if self.sum_batch_size_in_subtask:
            return transactions[0].sum_size
        else:
            return transactions[0].average_size

    def sort_operations(self):
        self.transactions = sorted(self.transactions, key=lambda i: (
        i.start_time, i.operation_number))

    def get_transaction_with_operation_number(self, operation_number,
                                              operation_type):
        for transaction in self.transactions:
            if (transaction.operation_number == operation_number) and (
                    transaction.operation_type == operation_type):
                return transaction
        return None

    def add_transaction(self, start_time, stop_time, equipment, size, operation,
                        operation_number, operation_type):
        if stop_time is None:
            stop_time = start_time

        operation_type = self.OPERATION_TYPES[operation_type]

        try:
            start_time.date
        except AttributeError:
            start_time = datetime.strptime(start_time, "%d.%m.%Y %H:%M:%S")
            stop_time = datetime.strptime(stop_time, "%d.%m.%Y %H:%M:%S")
        transaction = self.get_transaction_with_operation_number(
            operation_number, operation_type)
        if transaction is None:
            self.transactions.append(
                Transaction(start_time, stop_time, equipment, size,
                            operation, operation_number, operation_type))
        else:
            transaction.add_subtask(start_time, stop_time, equipment, size,
                                    operation)

    @property
    def total_work_time(self):
        report = timedelta(0)
        for transaction in self.transactions:
            report += transaction.work_time
        return report

    @property
    def machine_time(self):
        report = timedelta(0)
        for transaction in self.transactions:
            report += transaction.workload_time
        return report


class Transaction(object):
    def __init__(self, start_time, stop_time, equipment, size, operation,
                 operation_number, operation_type):
        self.equipment = equipment
        self.operation = operation
        self.operation_number = operation_number
        self.operation_type = operation_type
        self.subtasks = []
        self.add_subtask(start_time, stop_time, equipment, size, operation)

    @property
    def work_time(self):
        return self.stop_time - self.start_time

    @property
    def workload_time(self):
        report = timedelta(0)
        for subtask in self.subtasks:
            report += subtask.stop_time - subtask.start_time
        return report

    @property
    def start_time(self):
        start_time = datetime.max.replace(tzinfo=timezone.utc)
        for subtask in self.subtasks:
            start_time = min(start_time, subtask.start_time)
        return start_time

    @property
    def stop_time(self):
        stop_time = datetime.min.replace(tzinfo=timezone.utc)
        for subtask in self.subtasks:
            stop_time = max(stop_time, subtask.stop_time)
        return stop_time

    @property
    def sum_size(self):
        size = 0
        for subtask in self.subtasks:
            size += subtask.size
        return size

    @property
    def average_size(self):
        size = 0
        i = 0
        for subtask in self.subtasks:
            size += subtask.size
            i += 1
        return size / i

    def add_subtask(self, start_time, stop_time, equipment, size, operation):
        self.subtasks.append(
            Subtask(start_time, stop_time, equipment, size, operation))


class Subtask(object):
    def __init__(self, start_time, stop_time, equipment, size, operation):
        self.start_time = start_time
        self.stop_time = stop_time
        self.equipment = equipment
        self.size = size
        self.operation = operation


class Equipment(object):
    def __init__(self, equipment_id, department_id=None):
        self.equipment_id = equipment_id
        self.department_id = department_id


class Operation(object):
    def __init__(self, operation_id):
        self.operation_id = operation_id


class Product(object):
    def __init__(self, product_id):
        self.product_id = product_id


class TransactionSession(object):
    TRANSACTIONS_COLUMNS = {"START_TIME": "STARTTIME", "STOP_TIME": "ENDTIME",
                            "OPERATION": "OPERATION", "PRODUCT": "ITEMCODE",
                            "EQUIPMENT": "MACHINE",
                            "BATCH": "ORDERCODE", "SIZE": "STEPQUANTITY",
                            "NOP": "STEPNUMBER",
                            "ORDER": "CUSTOMERORDER"}

    def __init__(self):
        self.batches = {}
        self.products = {}
        self.equipment = {}
        self.operations = {}
        self.start_time = datetime.max.replace(tzinfo=timezone.utc)

    def get_product_with_id(self, product_id):
        try:
            return self.products[str(product_id)]
        except KeyError:
            return None

    def get_batch_with_id(self, batch_id):
        try:
            return self.batches[str(batch_id)]
        except KeyError:
            return None

    def get_batch_with_order_name(self, order_name):
        for batch in self.batches:
            if self.batches[batch].order == order_name:
                return self.batches[batch]
        return None

    def get_equipment_with_id(self, equipment_id):
        try:
            return self.equipment[str(equipment_id)]
        except KeyError:
            return None

    def get_operation_with_id(self, operation_id):
        try:
            return self.operations[str(operation_id)]
        except KeyError:
            return None

    def add_product(self, product_id):
        if self.get_product_with_id(product_id) is None:
            self.products[str(product_id)] = Product(product_id)
        return self.get_product_with_id(product_id)

    def add_batch(self, batch_id, product, order, sum_batch_size_in_subtask,
                  current_operation):
        if self.get_batch_with_id(batch_id) is None:
            self.batches[str(batch_id)] = Batch(batch_id=batch_id,
                                                product=product, order=order,
                                                sum_batch_size_in_subtask=sum_batch_size_in_subtask,
                                                current_operation=current_operation)
        return self.get_batch_with_id(batch_id)

    def add_equipment(self, equipment_id, department_id=None):
        if self.get_equipment_with_id(equipment_id) is None:
            self.equipment[str(equipment_id)] = Equipment(equipment_id,
                                                          department_id)
        return self.get_equipment_with_id(equipment_id)

    def add_operation(self, operation_id):
        if self.get_operation_with_id(operation_id) is None:
            self.operations[operation_id] = Operation(operation_id)
        return self.get_operation_with_id(operation_id)

    def average_production_time(self, date_from, date_to, product):
        report = []
        for batch_id in self.batches:
            batch = self.batches[batch_id]
            if (date_from <= batch.start_date < date_to) and (
                    batch.product == product):
                report.append(batch.production_time)
        try:
            return sum(report, timedelta(0)) / len(report)
        except ZeroDivisionError:
            return None

    def number_of_batches(self, date_from, date_to, product):
        i = 0
        for batch_id in self.batches:
            batch = self.batches[batch_id]
            if (date_from <= batch.start_date < date_to) and (
                    batch.product == product):
                i += 1
        return i

    def get_plan(self, date_from, date_to):
        plan = []
        for batch_id in self.batches:
            batch = self.batches[batch_id]
            if date_from <= batch.start_date < date_to:
                plan.append({"CODE": batch.product.product_id,
                             "ORDER": batch.batch_id,
                             "IDENTITY": "",
                             "NAME": "",
                             "AMOUNT": batch.initial_batch_size,
                             "DATE": batch.start_date,
                             "FIRST_OPERATION_EQUIPMENT": batch.transactions[
                                 0].equipment.equipment_id,
                             "LAST_OPERATION_DATE": batch.finish_date
                             })
        return plan

    def product_report(self, date_from, date_to):
        from statistics import median, mean
        production_cycle = {}
        work_time = {}
        initial_batch_size = {}
        final_batch_size = {}
        start_date = {}
        stop_date = {}
        machine_time = {}

        for batch_id in self.batches:
            batch = self.batches[batch_id]

            if date_from <= batch.start_date < date_to:
                pass
            else:
                continue

            if batch.product not in production_cycle.keys():
                production_cycle[batch.product] = []
            production_cycle[batch.product].append(
                batch.production_time.total_seconds() / 3600)

            if batch.product not in work_time.keys():
                work_time[batch.product] = []
            work_time[batch.product].append(
                batch.total_work_time.total_seconds() / 3600)

            if batch.product not in machine_time.keys():
                machine_time[batch.product] = []
            machine_time[batch.product].append(
                batch.machine_time.total_seconds() / 3600)

            if batch.product not in initial_batch_size.keys():
                initial_batch_size[batch.product] = []
            initial_batch_size[batch.product].append(batch.initial_batch_size)

            if batch.product not in final_batch_size.keys():
                final_batch_size[batch.product] = []
            final_batch_size[batch.product].append(batch.final_batch_size)

            if batch.product not in start_date.keys():
                start_date[batch.product] = []
            start_date[batch.product].append(batch.start_date)

            if batch.product not in stop_date.keys():
                stop_date[batch.product] = []
            stop_date[batch.product].append(batch.finish_date)

        report = []
        for product in self.products.values():
            if (product in production_cycle.keys()) and (
                    product in work_time.keys()):
                report.append({"PRODUCT_ID": product.product_id,
                               "NUMBER_OF_BATCHES": self.number_of_batches(
                                   date_from, date_to, product),
                               "TOTAL_QUANTITY": sum(final_batch_size[product]),
                               "AVERAGE_CYCLE": mean(production_cycle[product]),
                               "AVERAGE_MACHINE_TIME": mean(
                                   machine_time[product]),
                               "AVERAGE_WORK_TIME": mean(work_time[product]),
                               "AVERAGE_INITIAL_BATCH_SIZE": mean(
                                   initial_batch_size[product]),
                               "AVERAGE_FINAL_BATCH_SIZE": mean(
                                   final_batch_size[product]),
                               "MEDIAN_CYCLE": median(
                                   production_cycle[product]),
                               "MEDIAN_MACHINE_TIME": median(
                                   machine_time[product]),
                               "MEDIAN_WORK_TIME": median(work_time[product]),
                               "MEDIAN_INITIAL_BATCH_SIZE": median(
                                   initial_batch_size[product]),
                               "MEDIAN_FINAL_BATCH_SIZE": median(
                                   final_batch_size[product]),
                               "EARLIEST_START_DATE": min(start_date[product]),
                               "LATEST_START_DATE": max(start_date[product]),
                               "EARLIEST_STOP_DATE": min(stop_date[product]),
                               "LATEST_STOP_DATE": max(stop_date[product])
                               })

        return report

    def equipment_report(self, date_from, date_to, product=None):
        from statistics import median, mean

        work_time = {}
        wait_time = {}
        for batch_id in self.batches:
            batch = self.batches[batch_id]
            if (batch.product != product) and (product is not None):
                continue
            if date_from <= batch.start_date < date_to:
                pass
            else:
                continue
            previous_operation_stop_time = batch.start_date
            for transaction in batch.transactions:
                if transaction.equipment.equipment_id is None:
                    continue
                if transaction.equipment not in work_time.keys():
                    work_time[transaction.equipment] = []
                if transaction.equipment not in wait_time.keys():
                    wait_time[transaction.equipment] = []
                td_work = transaction.stop_time - transaction.start_time
                td_wait = transaction.start_time - previous_operation_stop_time
                work_time[transaction.equipment].append(
                    td_work.total_seconds() / 3600)
                wait_time[transaction.equipment].append(
                    td_wait.total_seconds() / 3600)
                previous_operation_stop_time = transaction.stop_time
        report = []
        for equipment in self.equipment.values():
            if (equipment in work_time.keys()) and (
                    equipment in wait_time.keys()):
                report.append({"EQUIPMENT_ID": equipment.equipment_id,
                               "AVERAGE_WORK_TIME": mean(work_time[equipment]),
                               "AVERAGE_WAIT_TIME": mean(wait_time[equipment]),
                               "MEDIAN_WORK_TIME": median(work_time[equipment]),
                               "MEDIAN_WAIT_TIME": median(wait_time[equipment]),
                               "NUMBER_OF_TRANSACTIONS": len(
                                   wait_time[equipment]),
                               "TOTAL_WORK_TIME": sum(work_time[equipment])})
        return report

    def daily_equipment_machinetime_report(self, date_from, date_to):
        from collections import defaultdict

        machine_time = dict()
        all_dates = []
        for batch_id in self.batches:
            batch = self.batches[batch_id]

            for transaction in batch.transactions:
                if transaction.equipment.equipment_id is None:
                    continue

                if transaction.equipment not in machine_time.keys():
                    machine_time[transaction.equipment] = defaultdict(float)

                for subtask in transaction.subtasks:
                    if (date_from <= subtask.start_time <= date_to) or (
                            date_from <= subtask.stop_time <= date_to):
                        pass
                    else:
                        continue

                    if subtask.start_time.date() == subtask.stop_time.date():
                        machine_time[transaction.equipment][
                            subtask.start_time.date()] += \
                            (
                                        subtask.stop_time - subtask.start_time).seconds / 3600
                    else:
                        current_date = subtask.start_time + timedelta(1)
                        while True:
                            if current_date.date() >= subtask.stop_time.date():
                                break
                            if current_date.date() not in all_dates:
                                all_dates.append(current_date.date())
                            machine_time[transaction.equipment][
                                current_date] += timedelta(1).seconds / 3600
                            current_date += timedelta(1)

                        days_border = datetime.combine(
                            date=subtask.start_time.date() + timedelta(1),
                            time=time(hour=0, minute=0))
                        first_day_machine_time = (
                                                             days_border - subtask.start_time.replace(
                                                         tzinfo=None)).seconds / 3600
                        days_border = datetime.combine(
                            date=subtask.stop_time.date(),
                            time=time(hour=0, minute=0))
                        second_day_machine_time = (subtask.stop_time.replace(
                            tzinfo=None) - days_border).seconds / 3600

                        if subtask.start_time.date() not in all_dates:
                            all_dates.append(subtask.start_time.date())

                        if subtask.stop_time.date() not in all_dates:
                            all_dates.append(subtask.stop_time.date())

                        machine_time[transaction.equipment][
                            subtask.start_time.date()] += first_day_machine_time
                        machine_time[transaction.equipment][
                            subtask.stop_time.date()] += second_day_machine_time

        report = []
        for equipment in self.equipment.values():
            row = dict()
            row["EQUIPMENT_ID"] = equipment.equipment_id
            for date in sorted(all_dates):
                # row[date] = round(machine_time[equipment][date].seconds/3600,
                #                   ndigits=2)
                row[date] = round(machine_time[equipment][date], ndigits=2)
            report.append(row)
        return report

    def daily_tasks_report(self, tasks_period_hours, date_from=None,
                           equipment_id=None):

        def get_shift(task_time, shifts=None):
            if shifts is None:
                shifts = dict()
                shifts["1_утро"] = [time(hour=7, minute=0),
                                    time(hour=15, minute=30)]
                shifts["2_вечер"] = [time(hour=15, minute=30),
                                     time(hour=23, minute=59)]
                shifts["3_ночь"] = [time(hour=23, minute=59),
                                    time(hour=7, minute=0)]
            report_date = task_time.date()
            report_shift = None
            for shift in shifts:
                if shifts[shift][0] < shifts[shift][1]:
                    if shifts[shift][0] <= task_time.time() < shifts[shift][1]:
                        report_shift = shift
                else:
                    if task_time.time() < shifts[shift][1]:
                        report_shift = shift
                        report_date = (task_time - timedelta(1)).date()
                    if task_time.time() >= shifts[shift][0]:
                        report_shift = shift

            return str(report_date) + " Смена№" + str(report_shift) + " "

        if date_from is None:
            date_from = self.start_time

        date_to = date_from + timedelta(tasks_period_hours / 24)

        report = []
        for batch_id in self.batches:
            batch = self.batches[batch_id]
            for transaction in batch.transactions:
                if transaction.equipment.equipment_id == equipment_id:
                    pass
                else:
                    continue
                if date_from <= transaction.start_time < date_to:
                    pass
                else:
                    continue

                row = dict()

                row["НАИМЕНОВАНИЕ"] = batch.product.product_id
                row["КОЛ-ВО"] = round(transaction.average_size, ndigits=3)
                row["НОМЕР ЗАКАЗА"] = batch.order
                row["НОМЕР ОПЕРАЦИИ"] = transaction.operation_number[0:25]
                # row["ТИП ОПЕРАЦИИ"] = transaction.operation_type
                # row["НАЧАЛО"] = transaction.start_time.replace(tzinfo=None)
                # row["ЗАВЕРШЕНИЕ"] = transaction.stop_time.replace(tzinfo=None)
                row["СМЕНА"] = get_shift(
                    transaction.start_time.replace(tzinfo=None))
                row["МАШ.ВРЕМЯ, ЧАС"] = str(round(
                    transaction.workload_time.total_seconds() // 3600)) + ":" + \
                                        str(round(
                                            transaction.workload_time.total_seconds() % 3600 / 60)).zfill(
                                            2)
                row["ТЕКУЩАЯ ОПЕРАЦИЯ"] = batch.current_operation[0:25]
                row["Смена_факт"] = ""
                row["Обор-е"] = ""
                row["ТехПроц"] = ""
                row["Мат-лы"] = ""
                row["Орг-я"] = ""
                row["Комментарии      "] = ""
                report.append(row)

        report = sorted(report, key=lambda i: i["СМЕНА"])

        return report

    def launch_report(self, date_from, date_to, product=None, period="day"):
        from collections import defaultdict
        launch = {}
        all_products = []

        all_date = set()

        for batch in self.batches.values():
            all_date.add(batch.start_date_as_str[period])

        for date in sorted(all_date):
            launch[date] = defaultdict(float)

        for batch_id in self.batches:
            batch = self.batches[batch_id]
            product_id = batch.product.product_id
            if (batch.product != product) and (product is not None):
                continue
            if date_from <= batch.start_date < date_to:
                pass
            else:
                continue
            start_date = batch.start_date_as_str[period]
            launch[start_date][product_id] += batch.initial_batch_size
            if product_id not in all_products:
                all_products.append(product_id)

        report = []
        for product_id in all_products:
            total = 0
            row = dict()
            row["PRODUCT"] = product_id
            for start_date in launch.keys():
                row[start_date] = launch[start_date][product_id]
                total += launch[start_date][product_id]
            row["TOTAL"] = total
            report.append(row)

        return report

    def wip_report(self, date_from, date_to, product=None, period="day"):
        from collections import defaultdict
        wip = {}
        total_per_day = {
            'PRODUCT': '#TOTAL'
        }
        all_products = []

        all_date = set()

        for batch in self.batches.values():
            all_date.add(batch.start_date_as_str[period])
            all_date.add(batch.finish_date_as_str[period])

        for date in sorted(all_date):
            wip[date] = defaultdict(float)
            total_per_day[date] = 0

        for batch_id in self.batches:
            batch = self.batches[batch_id]
            product_id = batch.product.product_id
            if (batch.product != product) and (product is not None):
                continue
            if date_from <= batch.start_date < date_to:
                pass
            else:
                continue
            date = batch.start_date_as_str[period]
            wip[date][product_id] += batch.initial_batch_size

            date = batch.finish_date_as_str[period]
            wip[date][product_id] -= batch.final_batch_size

            if product_id not in all_products:
                all_products.append(product_id)

        report = []
        for product_id in all_products:
            total = 0
            row = dict()
            row["PRODUCT"] = product_id
            for start_date in wip.keys():
                total += wip[start_date][product_id]
                row[start_date] = round(total, 1)
                total_per_day[start_date] += row[start_date]
            report.append(row)
        report.append(total_per_day)

        return report

    def release_report(self, date_from, date_to, product=None, period="day"):
        from collections import defaultdict
        release = {}
        all_products = []

        all_date = set()

        for batch in self.batches.values():
            all_date.add(batch.finish_date_as_str[period])

        for date in sorted(all_date):
            release[date] = defaultdict(float)

        for batch_id in self.batches:
            batch = self.batches[batch_id]
            product_id = batch.product.product_id
            if (batch.product != product) and (product is not None):
                continue
            if date_from <= batch.start_date < date_to:
                pass
            else:
                continue
            release_date = batch.finish_date_as_str[period]
            release[release_date][product_id] += batch.final_batch_size
            if product_id not in all_products:
                all_products.append(product_id)

        report = []

        for product_id in all_products:
            total = 0
            row = dict()
            row["PRODUCT"] = product_id
            for release_date in release.keys():
                row[release_date] = release[release_date][product_id]
                total += release[release_date][product_id]
            row["TOTAL"] = total
            report.append(row)

        return report

    def read_from_xlsx(self, xlsx, worksheet, columns=None,
                       sum_batch_size_in_subtasks=False):
        if columns is None:
            columns = self.TRANSACTIONS_COLUMNS
        data_list = xlsx_reader.XlsxReadData(xlsx=xlsx, worksheet=worksheet,
                                             columns=columns)
        for row in data_list.rows:
            if row["BATCH"] is None:
                continue
            product = self.add_product(row["PRODUCT"])
            operation = self.add_operation(row["OPERATION"])
            equipment = self.add_equipment(row["EQUIPMENT"])
            order = row["ORDER"]
            batch = self.add_batch(batch_id=row["BATCH"], product=product,
                                   order=order,
                                   sum_batch_size_in_subtask=sum_batch_size_in_subtasks)
            batch.add_transaction(start_time=row["START_TIME"],
                                  stop_time=row["STOP_TIME"],
                                  equipment=equipment,
                                  size=row["SIZE"], operation=operation,
                                  operation_number=row["NOP"])
        for batch in self.batches.values():
            batch.sort_operations()

    def get_main_session(self, url, login, password):
        import requests
        import json

        data = dict()
        data["action"] = "login"
        data["data"] = dict()
        data["data"]["login"] = login
        data["data"]["password"] = password
        data = json.dumps(data)
        headers = {'content-type': 'application/json'}
        s = requests.Session()
        try:
            print("Попытка подключение к системе:")
            r = s.post(url=url + "/action/login", headers=headers,
                       data=data).json()
            print(r)
            print("Получение главной сессии моделирования")
            r = s.get(url=url + "/action/primary_simulation_session").json()
        finally:
            print("Закрываем HTTP-сессию")
            s.close()
        return r["data"]

    def read_from_rest(self, url, login, password, tz, session):
        import requests
        import json
        from dateutil import parser

        data = dict()
        data["action"] = "login"
        data["data"] = dict()
        data["data"]["login"] = login
        data["data"]["password"] = password
        data = json.dumps(data)
        headers = {'content-type': 'application/json'}
        s = requests.Session()
        try:
            print("Попытка подключение к системе:")
            r = s.post(url=url + "/action/login", headers=headers,
                       data=data).json()
            print(r)
            print("Получение главной сессии моделирования")
            r = s.get(url=url + "/action/primary_simulation_session").json()
            print(r)
            print("Отправка запроса на получение данных")
            if session == "main":
                cur_session = r["data"]
            else:
                cur_session = session
            schedule_request = "/rest/collection/simulation_equipment?" \
                               "order_by=simulation_operation_task_equipment.simulation_operation_task.start_date&asc=true&" \
                               "order_by=simulation_operation_task_equipment.simulation_operation_task_id&asc=true&" \
                               "with=equipment&with_strict=false&with=equipment_class&" \
                               "with=simulation_operation_task_equipment&with=department&" \
                               "with=simulation_operation_task_equipment.simulation_operation_task&" \
                               "with=simulation_operation_task_equipment.simulation_operation_task.simulation_entity_batch&" \
                               "with=simulation_operation_task_equipment.simulation_operation_task.operation&" \
                               "with=simulation_operation_task_equipment.simulation_operation_task.operation.entity_route&" \
                               "with=simulation_operation_task_equipment.simulation_operation_task.simulation_entity_batch.entity&" \
                               "with=simulation_operation_task_equipment.simulation_operation_task.simulation_entity_batch.simulation_order_entity_batch&" \
                               "with_strict=false&with=simulation_operation_task_equipment.simulation_operation_task.simulation_entity_batch.simulation_order_entity_batch.order&" \
                               "filter={{simulation_session_id eq {}}}".format(
                cur_session)

            r = s.get(url=url + schedule_request, headers=headers).json()
        finally:
            print("Закрываем HTTP-сессию")
            s.close()

        print("Данные получены, начинаем разбор", r["meta"])
        report = {}
        for key in r:
            if key != "meta":
                report[key] = {}
                for row in r[key]:
                    if row["id"] in report[key].keys():
                        print("ALARM!!! {} {}".format(key, row["id"]))
                    report[key].update({row["id"]: row})

        equipment_class_report = report["equipment_class"]
        department_report = report["department"]
        # equipment_report = report["equipment"]
        simulation_equipment_report = report["simulation_equipment"]
        entity_report = report["entity"]
        operation_report = report["operation"]
        simulation_entity_batch_report = report["simulation_entity_batch"]
        simulation_operation_task_report = report["simulation_operation_task"]
        simulation_operation_task_equipment_report = report[
            "simulation_operation_task_equipment"]
        draft_simulation_order_entity_batch_report = report[
            "simulation_order_entity_batch"]
        simulation_order_entity_batch_report = {}
        for row in draft_simulation_order_entity_batch_report.values():
            key = row["simulation_entity_batch_id"]
            simulation_order_entity_batch_report[key] = row

        order_report = report["order"]
        progress = 0
        count = 0
        for row in simulation_operation_task_equipment_report.values():
            count += 1
            if count / r["meta"]["count"] >= progress / 100:
                print("Прогресс {:.0%}".format(count / r["meta"]["count"]))
                progress += 10
            simulation_operation_task_id = row["simulation_operation_task_id"]
            simulation_entity_batch_id = \
            simulation_operation_task_report[simulation_operation_task_id][
                "simulation_entity_batch_id"]
            entity_id = \
            simulation_entity_batch_report[simulation_entity_batch_id][
                "entity_id"]

            product = self.add_product(
                str(entity_report[entity_id]["code"]) + "_" +
                str(entity_report[entity_id]["name"]))

            simulation_operation_task_equipment_id = row["id"]
            operation_id = \
            simulation_operation_task_report[simulation_operation_task_id][
                "operation_id"]

            operation = self.add_operation(
                str(operation_report[operation_id]["nop"]) + "_" +
                str(operation_report[operation_id]["name"]))

            simulation_equipment_id = \
            simulation_operation_task_equipment_report[
                simulation_operation_task_equipment_id
            ]["simulation_equipment_id"]
            # equipment_id = simulation_equipment_report[simulation_equipment_id]["equipment_id"]
            equipment_class_id = \
            simulation_equipment_report[simulation_equipment_id][
                "equipment_class_id"]
            # equipment_class_id = equipment_report[equipment_id]["equipment_class_id"]
            department_id = \
            simulation_equipment_report[simulation_equipment_id][
                "department_id"]

            # department_id = equipment_report[equipment_id]["department_id"]

            equipment = self.add_equipment(
                equipment_class_report[equipment_class_id]["name"] + "_" +
                equipment_class_report[equipment_class_id]["identity"] + "_" +
                department_report[department_id]["identity"],
                department_report[department_id]["identity"] + "_" +
                department_report[department_id]["name"])

            batch_identity = \
            simulation_entity_batch_report[simulation_entity_batch_id][
                "identity"]
            batch_operation_id = \
            simulation_entity_batch_report[simulation_entity_batch_id][
                "previous_operation_id"]
            if batch_operation_id:
                if batch_operation_id in operation_report:
                    batch_operation = str(
                        operation_report[batch_operation_id]["nop"]) + "_" + \
                                      str(operation_report[batch_operation_id][
                                              "name"])
                else:
                    batch_operation = "НЕТ ДАННЫХ"
            else:
                batch_operation = "НЕ ЗАПУЩЕНА"

            if simulation_entity_batch_id in simulation_order_entity_batch_report:
                order_id = simulation_order_entity_batch_report[
                    simulation_entity_batch_id]["order_id"]
                order_name = order_report[order_id]["name"]
            else:
                order_name = ""

            batch = self.add_batch(batch_id=batch_identity, product=product,
                                   sum_batch_size_in_subtask=False,
                                   order=order_name,
                                   current_operation=batch_operation)

            start_time = parser.parse(
                simulation_operation_task_report[simulation_operation_task_id][
                    "start_date"])
            stop_time = parser.parse(
                simulation_operation_task_report[simulation_operation_task_id][
                    "stop_date"])

            start_time = start_time.astimezone(tz=tz)
            stop_time = stop_time.astimezone(tz=tz)

            batch_size = \
            simulation_operation_task_report[simulation_operation_task_id][
                "entity_amount"]
            operation_type = \
            simulation_operation_task_report[simulation_operation_task_id][
                "type"]
            batch.add_transaction(start_time=start_time, stop_time=stop_time,
                                  equipment=equipment,
                                  size=batch_size, operation=operation,
                                  operation_number=str(
                                      operation_report[operation_id][
                                          "nop"] + "_" +
                                      operation_report[operation_id]["name"]),
                                  operation_type=operation_type)
            self.start_time = min(start_time, self.start_time)

        for batch in self.batches.values():
            batch.sort_operations()
