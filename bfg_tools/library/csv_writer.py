import csv
from time import time


def csv_write_data(csv_file, data):
    """
    Запись словаря в csv файл
    """

    t = time()
    print("Начинаем считывание csv файла \"{}\"".format(csv_file))
    with open(csv_file, 'w', encoding='utf-8') as file:
        ws = csv.writer(file)

        ws.writerow(key for key in data[0])
        for entry in data:
            ws.writerow(entry[key] for key in data[0])

    print("Сохранение на вкладке в файл \"{}\" успешно "
          "завершено за {:.2f} секунд".format(csv_file, time() - t))
