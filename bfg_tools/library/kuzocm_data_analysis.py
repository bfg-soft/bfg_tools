import statistics

from bfg_tools.library import csv_reader, xlsx_reader, xlsx_writer


class CastingCalcSession(object):
    SPEC_COLUMNS = {
        'PARENT_CODE': 'PARENT_CODE',
        'PARENT_NAME': 'PARENT_NAME',
        'PARENT_IDENTITY': 'PARENT_IDENTITY',
        'CODE': 'CODE',
        'NAME': 'NAME',
        'IDENTITY': 'IDENTITY',
        'AMOUNT': 'AMOUNT',
        'DIAMETER': 'DIAMETER'
    }

    TECH_COLUMNS = {
        'ROUTE_ID': 'ROUTE_ID',
        'T_SHT': 'T_SHT',
        'T_PZ': 'T_PZ',
        'T_NAL': 'T_NAL',
        'EQUIPMENT': 'EQUIPMENT_ID',
        'DEPARTMENT': 'DEPT_ID',
        'NOP': 'NOP',
        'NAME': 'NAME'
    }

    PLAN_COLUMNS = {
        'ORDER': 'ORDER',
        'CODE': 'CODE',
        'IDENTITY': 'IDENTITY',
        'NAME': 'NAME',
        'AMOUNT': 'AMOUNT',
        'DATE': 'DATE'
    }

    ROUTE_COLUMNS = {'CODE': 'CODE', 'ROUTE_ID': 'ROUTE_ID'}

    EQUIPMENT_ALIASES = {
        'Илк-1.6 №21_12* 1_2': 'Илк-1.6 №12_21 1_2',
        'Илк-1.6 №12_21* 1_2': 'Илк-1.6 №12_21 1_2',
        'Илк-1.6 №12* 1_2': 'Илк-1.6 №12_21 1_2',
        'Илк-1.6 №21* 1_2': 'Илк-1.6 №12_21 1_2',
        'Илк-1.6 с2 №3_16_71 1_2': 'Илк-1.6 №3_16_71 1_2',
        'Илк-1.6 с2 №3 1_2': 'Илк-1.6 №3_16_71 1_2',
        'Илк-1.6 с2 №16 1_2': 'Илк-1.6 №3_16_71 1_2',
        'Илк-1.6 с2 №71 1_2': 'Илк-1.6 №3_16_71 1_2',
        'Печь №14* 1_2': 'Печь №14 1_2',
        "Установка 'CALAMARI'": 'CALAMARI 1_3',
        'Upcast установка': 'Upcast 1_4',
        'Пульвер.установка №1': 'Пульверизационная установка 5_0'
    }

    EQUIPMENT_BATCH_SIZE = {
        'Илк-1.6 №12_21 1_2': 2400,
        'Илк-1.6 №3_16_71 1_2': 1200,
        'CALAMARI 1_3': 2400,
        'Печь №14* 1_2': 2400,
        'Upcast 1_4': 2400,
        'Пульверизационная установка 5_0': 10000
    }

    OPERATION_ALIASES = ['Плавление и литье', 'Литье', 'Пульверизация']

    def __init__(self):
        self.alloy_groups = {}
        self.factory = {}
        self.cast_bars = {}
        self.products = {}
        self.plan = Plan()
        self.routes = {}

    def add_equipment(self, identity, name, amount):

        if identity in self.factory:
            self.factory[identity]['AMOUNT'] += amount
        else:
            self.factory[identity] = {
                'AMOUNT': amount,
                'EQUIPMENT': Equipment(identity=identity, name=name)
            }

    def add_alloy(self, identity, name, alloy_group_id):
        if alloy_group_id in self.alloy_groups:
            if identity in self.alloy_groups[alloy_group_id].alloys:
                return
        else:
            self.alloy_groups[alloy_group_id] = AlloyGroup(alloy_group_id)
        self.alloy_groups[alloy_group_id].alloys[identity] = Alloy(
            identity=identity,
            name=name,
            alloy_group_id=alloy_group_id
        )

    def get_alloy_with_identity(self, alloy_id):
        for each_group in self.alloy_groups:
            if alloy_id in self.alloy_groups[each_group].alloys:
                return self.alloy_groups[each_group][alloy_id]

    def add_entity(self, identity, code, name, alloy_id, diameter):
        if identity in self.cast_bars:
            return
        alloy = self.get_alloy_with_identity(alloy_id)
        self.cast_bars[identity] = CastBar(
            identity=identity,
            code=code,
            name=name,
            alloy=alloy,
            diameter=diameter
        )

    def add_product(self, identity, code, name):
        if identity not in self.products:
            self.products[identity] = Product(
                identity=identity,
                code=code,
                name=name
            )

    def get_product_with_id(self, identity):
        if identity in self.products:
            return self.products[identity]
        else:
            return None

    def get_route_with_id(self, route_id):
        if route_id in self.routes:
            return self.routes[route_id]
        else:
            return None

    def get_product_with_code_name(self, code, name):
        if (code is None) and (name is None):
            return None
        for entity in self.products.values():
            if str(entity.code) == str(code) \
                    and str(entity.name) == str(name):
                return entity
        return None

    def get_product_with_code(self, code):
        if code is None:
            return None
        for entity in self.products.values():
            if str(entity.code) == str(code):
                return entity
        return None

    def add_child(self, parent_id: str, child_id: str, amount: float):
        parent = self.get_product_with_id(parent_id)
        child = self.get_product_with_id(child_id)
        if (parent is None) or (child is None):
            return
        if child not in parent.product_entry:
            parent.product_entry[child] = amount

    def apply_diameter(self, product_id: str, diameter: float):
        if diameter:
            self.get_product_with_id(product_id).diameter = diameter

    def add_process(self, product_id, process_identity,
                    furnace_charge, equipment_id, batch_size, casting_time):
        product = self.cast_bars[product_id]
        if process_identity not in product.process:
            furnace = self.factory[equipment_id]['EQUIPMENT']
            product.process[
                process_identity
            ] = CastBarTechProcess(
                identity=process_identity,
                furnace_charge=furnace_charge,
                furnace=furnace,
                batch_size=batch_size,
                casting_time=casting_time
            )

    def add_plan_entry(self, product_id, order_name,
                       amount, date, code=None, name=None):
        # добавить строку в план
        product = self.get_product_with_id(product_id)
        if product is None:
            print('Не найден продукт {}, пробуем '
                  'определять по поляем IDENTITY и NAME'.format(product_id))
            product = self.get_product_with_code_name(code, name)
            if product is None:
                print('Не найден продукт {} {}, пробуем '
                      'определить только по IDENTITY'.format(code, name))
                product = self.get_product_with_code(code)
                if product is None:
                    print('Не найден продукт {}'.format(code))
                    return
                else:
                    print('Найден продукт {}'.format(product.identity))
            else:
                print('Найден продукт {}'.format(product.identity))
        if amount == 0:
            print('Нулевое количество')
        if product is not None:
            self.plan.plan_entry.append(
                PlanEntry(
                    product=product,
                    amount=amount,
                    entry_date=date,
                    order_name=order_name,
                    initial_name=name,
                    initial_code=code,
                    initial_order=order_name
                )
            )

    def operation_time_report(self):
        report = []
        for entity in self.cast_bars.values():
            temp = {}
            for tech_proc in entity.process.values():
                if tech_proc.furnace not in temp:
                    temp[tech_proc.furnace] = []
                temp[tech_proc.furnace].append(tech_proc.casting_time)

            for equipment_id in temp:
                report_row = {
                    'ENTITY': '{} {} {}'.format(
                        entity.identity,
                        entity.name,
                        equipment_id
                    ),
                    'MIN CASTING TIME': round(
                        min(temp[equipment_id]),
                        ndigits=3
                    ),
                    'MAX CASTING_TIME': round(
                        max(temp[equipment_id]),
                        ndigits=3
                    ),
                    'AVERAGE_CASTING_TIME': round(
                        statistics.mean(temp[equipment_id]),
                        ndigits=3
                    ),
                    'MEDIAN_CASTING_TIME': round(
                        statistics.median(temp[equipment_id]),
                        ndigits=3
                    )
                }
                report.append(report_row)

        return report

    def read_spec_from_xlsx(self, xlsx,
                            spec_columns=None, spec_worksheet='spec'):

        if spec_columns is None:
            spec_columns = self.SPEC_COLUMNS

        try:
            xlsx_data = xlsx_reader.XlsxReadData(
                xlsx=xlsx,
                worksheet=spec_worksheet,
                columns=spec_columns
            ).worksheets[spec_worksheet]
        except:
            print('Считать как xlsx не удалось, пробуем считать как csv')
            xlsx_data = csv_reader.CsvReadData(
                csv_file=xlsx,
                columns=spec_columns
            ).rows

        for row in xlsx_data:
            if row['ROW_NUMBER'] == 1:
                continue
            parent_id = str(row['PARENT_CODE']).upper()
            parent_code = str(row['PARENT_IDENTITY']).upper()
            parent_name = str(row['PARENT_NAME']).upper()
            parent_diameter = row['DIAMETER']
            child_id = str(row['CODE']).upper()
            child_code = str(row['IDENTITY']).upper()
            child_name = str(row['NAME']).upper()
            try:
                if type(row['AMOUNT']) is str:
                    amount = float(row['AMOUNT'].replace(',', '.'))
                else:
                    amount = float(row['AMOUNT'])
            except(ValueError, AttributeError):
                amount = 0
                print(
                    'Неверное значение ячейки столбца '
                    'AMOUNT в строке {}, значение в ячейке \'{}\''.format(
                        row['ROW_NUMBER'],
                        row['AMOUNT']
                    )
                )

            self.add_product(parent_id, parent_code, parent_name)
            self.add_product(child_id, child_code, child_name)

            if parent_id != child_id:
                self.add_child(
                    parent_id=parent_id,
                    child_id=child_id,
                    amount=amount
                )
            self.apply_diameter(parent_id, parent_diameter)

    def read_alloys_from_xlsx(self, xlsx, worksheet):
        xlsx_data = xlsx_reader.XlsxReadData(
            xlsx=xlsx,
            worksheet=worksheet
        ).worksheets[worksheet]
        for row in xlsx_data:
            if row['ROW_NUMBER'] == 1:
                continue
            alloy_group = str(row['КАТЕГОРИЯ']).upper()
            alloy_identity = str(row['СПЛАВ']).upper()
            self.add_alloy(
                identity=alloy_identity,
                name=alloy_identity,
                alloy_group_id=alloy_group
            )

    def read_route_from_xlsx(self, xlsx, columns=None, worksheet='route'):

        if columns is None:
            columns = self.ROUTE_COLUMNS

        try:
            xlsx_data = xlsx_reader.XlsxReadData(
                xlsx=xlsx,
                worksheet=worksheet,
                columns=columns
            ).worksheets[worksheet]
        except:
            print('Считать как xlsx не удалось, пробуем считать как csv')
            xlsx_data = csv_reader.CsvReadData(
                csv_file=xlsx,
                columns=columns
            ).rows

        for row in xlsx_data:
            if row['ROW_NUMBER'] == 1:
                continue
            product_id = row['CODE']
            route_id = row['ROUTE_ID']
            product = self.get_product_with_id(product_id)
            if product is None:
                print(product_id)
                continue
            if route_id not in product.routes:
                product.routes[route_id] = ProductRoute(route_id)
                self.routes[route_id] = product.routes[route_id]

    def read_tech_from_xlsx(self, xlsx, columns=None, worksheet='tech-nal'):
        def get_as_float(dictionary, key):
            try:
                if type(dictionary[key]) is str:
                    result = float(dictionary[key].replace(',', '.'))
                else:
                    result = float(dictionary[key])
            except(ValueError, AttributeError, TypeError):
                result = 0
                if dictionary[key] is not None:
                    print('Неверное значение ячейки столбца '
                          '{} в строке {}, значение в ячейке \'{}\''.format(
                        key,
                        dictionary['ROW_NUMBER'],
                        dictionary[key])
                    )
            return result

        def get_as_string(dictionary, key, digits):
            try:
                if type(dictionary[key]) is int:
                    result = str(dictionary[key]).zfill(digits)
                else:
                    result = dictionary[key]
            except TypeError:
                result = str(0).zfill(digits)
                print(
                    'Неверное значение ячейки столбца '
                    '{} в строке {}, значение в ячейке \'{}\''.format(
                        key,
                        dictionary['ROW_NUMBER'],
                        dictionary[key]
                    )
                )
            return result

        if columns is None:
            columns = self.TECH_COLUMNS

        try:
            xlsx_data = xlsx_reader.XlsxReadData(
                xlsx=xlsx,
                worksheet=worksheet,
                columns=columns
            ).worksheets[worksheet]
        except:
            print('Считать как xlsx не удалось, пробуем считать как csv')
            xlsx_data = csv_reader.CsvReadData(
                csv_file=xlsx,
                columns=columns
            ).rows

        for row in xlsx_data:
            if row['ROW_NUMBER'] == 1:
                continue
            route_id = row['ROUTE_ID']
            name = row['NAME']
            operation_id = row['NOP']
            equipment_id = str(row['EQUIPMENT'])
            department_id = str(row['DEPARTMENT'])
            t_sht = get_as_float(row, 'T_SHT')
            t_pz = get_as_float(row, 'T_PZ')
            t_nal = get_as_float(row, 'T_NAL')
            nop = get_as_string(row, 'NOP', 5)

            route = self.get_route_with_id(route_id)
            if route is not None:
                if operation_id not in route.operations:
                    route.operations[operation_id] = ProductRouteOperation(
                        identity=operation_id,
                        t_pz=t_pz,
                        t_sht=t_sht,
                        t_nal=t_nal,
                        equipment_id=equipment_id,
                        department_id=department_id,
                        nop=nop,
                        name=name
                    )
            else:
                print('Не найдено маршрута с ROUTE_ID {}'.format(route_id))

    def read_plan_from_xlsx(self, xlsx, columns=None, worksheet='plan'):
        from datetime import datetime

        def get_as_float(dictionary, key):
            try:
                if type(dictionary[key]) is str:
                    result = float(dictionary[key].replace(',', '.'))
                else:
                    result = float(dictionary[key])
            except(ValueError, AttributeError, TypeError):
                result = 0
                if dictionary[key] is not None:
                    print(
                        'Неверное значение ячейки столбца '
                        '{} в строке {}, значение в ячейке \'{}\''.format(
                            key,
                            dictionary['ROW_NUMBER'],
                            dictionary[key]
                        )
                    )
            return result

        if columns is None:
            columns = self.PLAN_COLUMNS

        try:
            xlsx_data = xlsx_reader.XlsxReadData(
                xlsx=xlsx,
                worksheet=worksheet,
                columns=columns
            ).worksheets[worksheet]
        except:
            print('Считать как xlsx не удалось, пробуем считать как csv')
            xlsx_data = csv_reader.CsvReadData(
                csv_file=xlsx,
                columns=columns
            ).rows

        for row in xlsx_data:
            if row['ROW_NUMBER'] == 1:
                continue
            order_name = row['ORDER']
            product_id = row['CODE']
            product_name = row['NAME']
            product_code = row['IDENTITY']
            entry_amount = get_as_float(row, 'AMOUNT')

            try:
                entry_date = datetime.combine(datetime.date(row['DATE']),
                                              datetime.time(row['DATE']))
            except(TypeError, ValueError, AttributeError):
                entry_date = datetime.now()
                print(
                    'Неверное значение ячейки столбца '
                    'DATE в строке {}, значение ячейки {}'.format(
                        row['ROW_NUMBER'],
                        row['DATE']
                    )
                )

            if entry_amount > 0:
                self.add_plan_entry(
                    product_id=product_id,
                    amount=entry_amount,
                    date=entry_date,
                    code=product_code,
                    name=product_name,
                    order_name=order_name
                )
            else:
                print(
                    'Изделие {} из заказа {} в плане с '
                    'нулевым количеством. При расчете учтено не будет'.format(
                        product_id, order_name
                    )
                )

    def get_cast_bars_from_product(self, product, equipment_name='Илк',
                                   operation_name='Плавление и литье',
                                   amount=1, report=None) -> dict:
        if report is None:
            report = {}
        is_casting_operation = False
        is_casting_equipment = False
        for route in product.routes.values():
            for operation in route.operations.values():
                is_casting_operation = \
                    operation.name[0:len(operation_name)] == operation_name
                is_casting_equipment = \
                    operation.equipment_id[
                    0:len(equipment_name)] == equipment_name
                if is_casting_operation and is_casting_equipment:
                    if product in report:
                        report[product] += amount
                    else:
                        report[product] = amount
                    break
            if is_casting_operation and is_casting_equipment:
                break

        for child in product.product_entry:
            print(product, child)
            child_amount = product.product_entry[child]
            self.get_cast_bars_from_product(
                product=child,
                equipment_name=equipment_name,
                operation_name=operation_name,
                amount=amount * child_amount,
                report=report
            )

        return report

    def read_cast_bars(self, equipment_name='Илк',
                       operation_name='Плавление и литье'):
        for product in self.products.values():
            if product.identity == 'И95713_ТМ030278':
                a = 1
                print(product)
            report = session.get_cast_bars_from_product(
                product=product,
                equipment_name=equipment_name,
                operation_name=operation_name
            )

            if not report:
                continue

            for entity in report:

                alloy = self.get_alloy(entity)
                if alloy is None:
                    print(
                        'Не удалось установить сплав для изделия {} {}'.format(
                            entity.identity, entity.name))
                    continue
                alloy_identity = '{}_{}'.format(
                    alloy.identity,
                    str(entity.diameter)
                )
                #  добавим новые отливки
                if alloy_identity not in self.cast_bars:
                    self.cast_bars[alloy_identity] = CastBar(
                        identity=alloy_identity,
                        code=alloy.alloy_group_id,
                        name=alloy.name,
                        alloy=alloy,
                        diameter=entity.diameter
                    )
                new_entity = self.cast_bars[alloy_identity]

                #  на будущее свяжем деталь с заготовкой литейного производства
                entity.cast_bar = new_entity
                #  запомним, что элементы его спецификации -- шихта
                for entry in entity.product_entry:
                    entry.scrap_material = True

                #  запомним сколько отливок идет на изделие
                new_entity.products[product.identity] = report[entity]
                product.cast_bar_blank = new_entity

                #  добавим варианты плавления с разными шихтовками к отливке
                for route in entity.routes.values():
                    for operation in route.operations.values():
                        for equipment in self.EQUIPMENT_ALIASES:
                            if not ((equipment in operation.equipment_id) and
                                    (operation.name in self.OPERATION_ALIASES)):
                                continue
                            furnace = self.EQUIPMENT_ALIASES[equipment]
                            batch_size = self.EQUIPMENT_BATCH_SIZE[furnace]
                            casting_time_hr = operation.t_sht * batch_size / 60
                            new_entity.process[
                                route.identity] = CastBarTechProcess(
                                identity=route.identity,
                                furnace_charge=entity.product_entry,
                                furnace=furnace,
                                batch_size=batch_size,
                                casting_time=casting_time_hr
                            )

    def get_alloy(self, product):
        for alloy_group in self.alloy_groups.values():
            for alloy in alloy_group.alloys.values():
                if (alloy.name + ' ') in product.name:
                    return alloy
        return None

    def unique_products_list(self):
        report = []
        for product in self.products.values():
            if product.scrap_material:
                continue
            if product.cast_bar:
                continue
            if not product.routes and not product.product_entry:
                continue
            if product.alias:
                continue
            new_product = True
            for unique_product in report:
                if product.is_product_equal_to(unique_product):
                    print('Продукт {} полностью идентичен продукту {}'.format(
                        product.identity,
                        unique_product.identity))
                    new_product = False
                    product.alias = unique_product
                    break
            if new_product:
                report.append(product)

        return report

    def remove_all_operations_in_department(self, department_id):
        for product in self.products.values():
            for route in product.routes.values():
                delete_list = []
                for operation_id in route.operations:
                    operation = route.operations[operation_id]
                    if operation.department_id == department_id:
                        delete_list.append(operation_id)
                for operation_id in delete_list:
                    del route.operations[operation_id]

    def remove_empty_products(self):
        #  удалим пустые маршруты
        delete_list = []
        for route_id in self.routes:
            route = self.routes[route_id]
            if not route.operations:
                delete_list.append(route_id)
        for route_id in delete_list:
            del self.routes[route_id]

        #  удалим пустые изделия
        delete_list = []
        for product_id in self.products:
            product = self.get_product_with_id(product_id)
            route_delete_list = []
            for route in product.routes.values():
                if not route.operations:
                    route_delete_list.append(route.identity)
            for route_id in route_delete_list:
                del product.routes[route_id]
            if product.is_product_empty():
                delete_list.append(product_id)
        for product_id in delete_list:
            del self.products[product_id]

    def write_mill_spec_to_xlsx(self, xlsx, worksheet, overwrite):
        report = []
        relationships_list = []
        for product in self.unique_products_list():
            if product.alias is None:
                main_parent = product
            else:
                main_parent = product.alias
            for child in product.product_entry:
                if child.alias is None:
                    main_child = child
                else:
                    main_child = child.alias
                if main_child.cast_bar:
                    main_child = main_child.cast_bar
                if main_parent.identity + main_child.identity in relationships_list:
                    continue
                row = {
                    'PARENT_CODE': main_parent.identity,
                    'PARENT_NAME': main_parent.name,
                    'PARENT_IDENTITY': main_parent.code,
                    'CODE': main_child.identity,
                    'NAME': main_child.name,
                    'IDENTITY': main_child.code,
                    'AMOUNT': product.product_entry[child]
                }
                report.append(row)
                relationships_list.append(product.identity + child.identity)

        xlsx_writer.xlsx_write_data(xlsx=xlsx, worksheet=worksheet, data=report,
                                    overwrite=overwrite)

    def write_mill_route_to_xlsx(self, xlsx, worksheet, overwrite):
        report = []
        product_list = []
        for product in self.unique_products_list():
            for route_id in product.routes:
                if product in product_list:
                    alternative = 1
                else:
                    alternative = 0
                row = {
                    'CODE': product.identity,
                    'ROUTE_ID': route_id,
                    'ALTERNATIVE': alternative
                }
                report.append(row)

        xlsx_writer.xlsx_write_data(xlsx=xlsx, worksheet=worksheet, data=report,
                                    overwrite=overwrite)

    def write_mill_tech_to_xlsx(self, xlsx, worksheet, overwrite):
        report = []
        for product in self.unique_products_list():
            for route_id in product.routes:
                route = self.get_route_with_id(route_id)
                if route is None:
                    print('Не найден маршрут {} для изделия {}'.format(route_id,
                                                                       product.identity))
                    continue
                for operation in route.operations.values():
                    row = {
                        'ROUTE_ID': route_id,
                        'CODE': product.identity,
                        'ID': route_id + '_' + operation.nop,
                        'NAME': operation.name,
                        'NOP': operation.nop,
                        'DEPT_ID': operation.department_id,
                        'EQUIPMENT_ID': operation.equipment_id,
                        'T_PZ': operation.t_pz,
                        'T_NAL': operation.t_nal,
                        'T_SHT': operation.t_sht
                    }
                    report.append(row)

        xlsx_writer.xlsx_write_data(xlsx=xlsx, worksheet=worksheet, data=report,
                                    overwrite=overwrite)

    def write_mill_plan_to_xlsx(self, xlsx, worksheet, overwrite):
        report = []
        for plan_entry in self.plan.plan_entry:
            product = plan_entry.product
            if product is None:
                continue
            if product.is_product_empty():
                continue
            if product.alias:
                product = product.alias
            if product.cast_bar_blank is None:
                alloy = ''
                alloy_group = ''
                alloy_amount = 0
            else:
                alloy = product.cast_bar_blank.alloy.identity
                alloy_group = product.cast_bar_blank.alloy.alloy_group_id
                alloy_amount = round(product.cast_bar_blank.products[
                                         product.identity] * plan_entry.amount,
                                     ndigits=3)
            row = {
                'ORDER': plan_entry.order_name,
                'IDENTITY': product.code,
                'NAME': product.name,
                'DATE': plan_entry.date,
                'AMOUNT': round(plan_entry.amount, ndigits=3),
                'CODE': product.identity,
                'ALLOY': alloy,
                'ALLOY_GROUP': alloy_group,
                'ALLOY_AMOUNT': alloy_amount
            }
            report.append(row)

        xlsx_writer.xlsx_write_data(
            xlsx=xlsx,
            worksheet=worksheet,
            data=sorted(report, key=lambda i: i['DATE']),
            overwrite=overwrite)

    def write_product_alloy_relation_to_xlsx(self, xlsx, worksheet, overwrite):
        report = []
        for product in self.products.values():
            # if product is None:
            #     continue
            # if product.is_product_empty():
            #     continue
            # if product.alias:
            #     product = product.alias
            if product.cast_bar_blank is None:
                continue
            alloy = product.cast_bar_blank.alloy.identity
            diameter = product.cast_bar_blank.diameter
            alloy_group = product.cast_bar_blank.alloy.alloy_group_id
            alloy_amount = round(
                product.cast_bar_blank.products[product.identity],
                ndigits=3
            )
            if alloy_amount == 0:
                print(product.identity)
            row = {
                'CODE': product.identity,
                'NAME': product.name,
                'ALLOY': alloy,
                'ALLOY_GROUP': alloy_group,
                'AMOUNT': alloy_amount,
                'ALLOY_DIAMETER': diameter
            }
            report.append(row)

        xlsx_writer.xlsx_write_data(
            xlsx=xlsx,
            worksheet=worksheet,
            data=sorted(report, key=lambda i: i['CODE']),
            overwrite=overwrite
        )

    def write_casting_alloy_group_report_to_xlsx(self, xlsx):
        from collections import defaultdict
        alloy_groups = []
        month_report = {}
        ten_days_report = {}
        week_report = {}
        day_report = {}
        quarter_report = {}
        day_list = []
        week_list = []
        tend_day_list = []
        month_list = []
        quarter_list = []
        for plan_entry in sorted(self.plan.plan_entry, key=lambda i: i.date):
            product = plan_entry.product
            if product is None:
                continue
            if product.is_product_empty():
                continue
            if product.alias:
                product = product.alias
            if product.cast_bar_blank is None:
                continue
            else:
                alloy_group = product.cast_bar_blank.alloy.alloy_group_id
                alloy_amount = round(product.cast_bar_blank.products[
                                         product.identity] * plan_entry.amount,
                                     ndigits=3)

            month = str(plan_entry.date.year) + '/' + str(
                plan_entry.date.month).zfill(2)
            quarter = str(plan_entry.date.year) + '/' + str(
                (plan_entry.date.month - 1) // 3 + 1)
            if plan_entry.date.day // 10 == 3:
                ten_day = str(plan_entry.date.year) + '/' + str(
                    plan_entry.date.month) + '/21'
            else:
                ten_day = str(plan_entry.date.year) + '/' + str(
                    plan_entry.date.month) + '/' + \
                          str(plan_entry.date.day // 10 * 10 + 1)
            week = str(plan_entry.date.year) + '/' + str(
                plan_entry.date.isocalendar()[1]).zfill(2)
            day = plan_entry.date.date()

            if alloy_group not in alloy_groups:
                alloy_groups.append(alloy_group)
                month_report[alloy_group] = defaultdict(float)
                ten_days_report[alloy_group] = defaultdict(float)
                week_report[alloy_group] = defaultdict(float)
                quarter_report[alloy_group] = defaultdict(float)
                day_report[alloy_group] = defaultdict(float)

            if week not in week_list:
                week_list.append(week)
            if ten_day not in tend_day_list:
                tend_day_list.append(ten_day)
            if month not in month_list:
                month_list.append(month)
            if quarter not in quarter_list:
                quarter_list.append(quarter)
            if day not in day_list:
                day_list.append(day)

            week_report[alloy_group][week] += alloy_amount
            ten_days_report[alloy_group][ten_day] += alloy_amount
            month_report[alloy_group][month] += alloy_amount
            quarter_report[alloy_group][quarter] += alloy_amount
            day_report[alloy_group][day] += alloy_amount

        report = []
        for alloy_group in week_report:
            row = {
                'Группа сплавов': alloy_group
            }
            for week in week_list:
                row[week] = round(week_report[alloy_group][week] / 1000,
                                  ndigits=1)
            report.append(row)
        xlsx_writer.xlsx_write_data(xlsx=xlsx, worksheet='week',
                                    data=sorted(report, key=lambda i: i[
                                        'Группа сплавов']), overwrite=True)

        report = []
        for alloy_group in month_report:
            row = {
                'Группа сплавов': alloy_group
            }
            for month in month_list:
                row[month] = round(month_report[alloy_group][month] / 1000,
                                   ndigits=1)
            report.append(row)
        xlsx_writer.xlsx_write_data(xlsx=xlsx, worksheet='month',
                                    data=sorted(report, key=lambda i: i[
                                        'Группа сплавов']), overwrite=False)

        report = []
        for alloy_group in quarter_report:
            row = {
                'Группа сплавов': alloy_group
            }
            for quarter in quarter_list:
                row[quarter] = round(
                    quarter_report[alloy_group][quarter] / 1000, ndigits=1)
            report.append(row)
        xlsx_writer.xlsx_write_data(xlsx=xlsx, worksheet='quarter',
                                    data=sorted(report, key=lambda i: i[
                                        'Группа сплавов']), overwrite=False)

        report = []
        for alloy_group in ten_days_report:
            row = {
                'Группа сплавов': alloy_group
            }
            for ten_day in tend_day_list:
                row[ten_day] = round(
                    ten_days_report[alloy_group][ten_day] / 1000, ndigits=1)
            report.append(row)
        xlsx_writer.xlsx_write_data(xlsx=xlsx, worksheet='ten_days',
                                    data=sorted(report, key=lambda i: i[
                                        'Группа сплавов']), overwrite=False)

    def write_casting_alloy_report_to_xlsx(self, xlsx):

        def get_month(date):
            return str(date.year) + '/' + str(date.month).zfill(2)

        def get_quarter(date):
            return str(date.year) + '/' + str((date.month - 1) // 3 + 1)

        def get_ten_day(date):
            if (plan_entry.date.day - 1) // 10 == 3:
                return str(date.year) + '/' + str(date.month) + '/21'
            else:
                return str(date.year) + '/' + str(date.month) + '/' + str(
                    (date.day - 1) // 10 * 10 + 1)

        def get_week(date):
            return str(date.year) + '/' + str(date.isocalendar()[1]).zfill(2)

        def get_day(date):
            return date.date()

        def write_group_alloy_report_to_xlsx(_xlsx, _worksheet, _period_report,
                                             _period_list, _overwrite):
            result_report = []
            for each_alloy in _period_report:
                row = {
                    'Сплав': each_alloy.identity,
                    'Группа сплавов': each_alloy.alloy_group_id
                }
                for _period in _period_list:
                    row[_period] = round(
                        _period_report[each_alloy][_period] / 1000, ndigits=1)
                result_report.append(row)
            result_report = sorted(result_report, key=lambda i: i['Сплав'])
            xlsx_writer.xlsx_write_data(xlsx=_xlsx, worksheet=_worksheet,
                                        data=sorted(result_report,
                                                    key=lambda i: i[
                                                        'Группа сплавов']),
                                        overwrite=_overwrite)

        from collections import defaultdict
        reports = {
            'week': get_week,
            'ten_days': get_ten_day,
            'month': get_month,
            'quarter': get_quarter,
            'day': get_day
        }

        alloys = []
        period_report = {}
        period_list = {}

        for key in reports:
            period_report[key] = {}
            period_list[key] = []

        for plan_entry in sorted(self.plan.plan_entry, key=lambda i: i.date):
            product = plan_entry.product
            if product is None:
                continue
            if product.is_product_empty():
                continue
            if product.alias:
                product = product.alias
            if product.cast_bar_blank is None:
                continue
            else:
                alloy_amount = round(product.cast_bar_blank.products[
                                         product.identity] * plan_entry.amount,
                                     ndigits=3)
                alloy = product.cast_bar_blank.alloy

            if alloy not in alloys:
                alloys.append(alloy)
                for key in period_report:
                    period_report[key][alloy] = defaultdict(float)

            for key in reports:
                period = reports[key](plan_entry.date)
                if period not in period_list[key]:
                    period_list[key].append(period)
                period_report[key][alloy][period] += alloy_amount

        is_first_report = True
        for key in reports:
            write_group_alloy_report_to_xlsx(_xlsx=xlsx, _worksheet=key,
                                             _period_report=period_report[key],
                                             _period_list=period_list[key],
                                             _overwrite=is_first_report)
            is_first_report = False

    def write_casting_route_to_xlsx(self, xlsx, worksheet, overwrite,
                                    equipment_list=None):
        report = []
        for cast_bar in self.cast_bars.values():
            casting_process = {}
            for process in cast_bar.process.values():
                if process.furnace not in casting_process:
                    casting_process[process.furnace] = process
                    continue
                if process.casting_time < casting_process[
                    process.furnace].casting_time:
                    casting_process[process.furnace] = process
                elif len(casting_process[
                             process.furnace].furnace_charge) < 2 <= len(
                        process.furnace_charge):
                    casting_process[process.furnace] = process
            alternative = 0
            for furnace in casting_process:
                report.append({
                    'ROUTE_ID': casting_process[furnace].identity,
                    'CODE': cast_bar.identity,
                    'ALTERNATIVE': alternative,
                    'FURNACE': furnace
                })
                alternative = 1
            for furnace in equipment_list:
                if furnace not in casting_process:
                    real_furnace = list(casting_process.keys())[0]
                    report.append({
                        'ROUTE_ID': casting_process[
                                        real_furnace].identity + '_new',
                        'CODE': cast_bar.identity,
                        'ALTERNATIVE': alternative,
                        'FURNACE': furnace
                    })

        xlsx_writer.xlsx_write_data(xlsx=xlsx, worksheet=worksheet,
                                    data=sorted(report,
                                                key=lambda i: i['CODE']),
                                    overwrite=overwrite)

    def write_casting_tech_to_xlsx(self, xlsx, worksheet, overwrite,
                                   equipment_list):
        report = []
        for cast_bar in self.cast_bars.values():
            casting_process = {}
            for process in cast_bar.process.values():
                if process.furnace not in casting_process:
                    casting_process[process.furnace] = process
                    continue
                if process.casting_time < casting_process[
                    process.furnace].casting_time:
                    casting_process[process.furnace] = process
                elif len(casting_process[
                             process.furnace].furnace_charge) < 2 <= len(
                        process.furnace_charge):
                    casting_process[process.furnace] = process
            for furnace in casting_process:
                report.append({
                    'ROUTE_ID': casting_process[furnace].identity,
                    'CODE': cast_bar.identity,
                    'ALLOY': cast_bar.alloy.name,
                    'ALLOY_GROUP': cast_bar.alloy.alloy_group_id,
                    'ID': casting_process[furnace].identity,
                    'NAME': 'Плавление и литье',
                    'NOP': '000',
                    'DEPT_ID': '1_2',
                    'EQUIPMENT_ID': furnace,
                    'T_NAL': 0,
                    'T_PZ': 0,
                    'T_SHT': round(casting_process[furnace].casting_time * 60 /
                                   casting_process[furnace].batch_size,
                                   ndigits=3),
                    'CASTING_TIME_HR': casting_process[furnace].casting_time,
                    'BATCH_SIZE_KG': casting_process[furnace].batch_size
                })
            for furnace in equipment_list:
                if furnace not in casting_process:
                    real_furnace = list(casting_process.keys())[0]
                    report.append({
                        'ROUTE_ID': casting_process[
                                        real_furnace].identity + '_new',
                        'CODE': cast_bar.identity,
                        'ALLOY': cast_bar.alloy.name,
                        'ALLOY_GROUP': cast_bar.alloy.alloy_group_id,
                        'ID': casting_process[real_furnace].identity + '_new',
                        'NAME': 'Плавление и литье',
                        'NOP': '000',
                        'DEPT_ID': '1_2',
                        'EQUIPMENT_ID': furnace,
                        'T_NAL': 0,
                        'T_PZ': 0,
                        'T_SHT': round(
                            casting_process[real_furnace].casting_time * 60 /
                            self.EQUIPMENT_BATCH_SIZE[furnace], ndigits=3),
                        'CASTING_TIME_HR': casting_process[
                            real_furnace].casting_time,
                        'BATCH_SIZE_KG': self.EQUIPMENT_BATCH_SIZE[furnace]
                    })

        xlsx_writer.xlsx_write_data(xlsx=xlsx, worksheet=worksheet,
                                    data=sorted(report,
                                                key=lambda i: i['CODE']),
                                    overwrite=overwrite)

    def write_casting_spec_to_xlsx(self, xlsx, worksheet, overwrite):
        report = []
        for cast_bar in self.cast_bars.values():
            casting_process = {}
            for process in cast_bar.process.values():
                if process.furnace not in casting_process:
                    casting_process[process.furnace] = process
                    continue
                if process.casting_time < casting_process[
                    process.furnace].casting_time:
                    casting_process[process.furnace] = process
                elif len(casting_process[
                             process.furnace].furnace_charge) < 2 <= len(
                        process.furnace_charge):
                    casting_process[process.furnace] = process
            for furnace in casting_process:
                for entry in casting_process[furnace].furnace_charge:
                    report.append({
                        'PARENT_CODE': cast_bar.identity,
                        'PARENT_NAME': cast_bar.alloy.alloy_group_id,
                        'PARENT_IDENTITY': cast_bar.code,
                        'PARENT_DIAMETER': cast_bar.diameter,
                        'PARENT_ALLOY': cast_bar.alloy.name,
                        'CODE': entry.identity,
                        'NAME': entry.name,
                        'IDENTITY': entry.code,
                        'AMOUNT': casting_process[furnace].furnace_charge[entry]
                    })
                if not casting_process[furnace].furnace_charge:
                    report.append({
                        'PARENT_CODE': cast_bar.identity,
                        'PARENT_NAME': cast_bar.alloy.alloy_group_id,
                        'PARENT_IDENTITY': cast_bar.code,
                        'PARENT_DIAMETER': cast_bar.diameter,
                        'PARENT_ALLOY': cast_bar.alloy.name,
                        'CODE': 'НЕТ ИНФОРМАЦИИ',
                        'NAME': '',
                        'IDENTITY': '',
                        'AMOUNT': 0
                    })
                break

        xlsx_writer.xlsx_write_data(xlsx=xlsx, worksheet=worksheet,
                                    data=sorted(report,
                                                key=lambda i: i['PARENT_CODE']),
                                    overwrite=overwrite)

    def write_casting_plan_to_xlsx(self, xlsx, worksheet, overwrite):
        alloy_plan = {}

        for plan_entry in self.plan.plan_entry:
            product = plan_entry.product
            if product is None:
                continue
            if product.is_product_empty():
                continue
            if product.alias:
                product = product.alias
            if product.cast_bar_blank is None:
                continue
            else:
                cast_bar = product.cast_bar_blank
                cast_bar_amount = round(product.cast_bar_blank.products[
                                            product.identity] * plan_entry.amount,
                                        ndigits=3)
            if plan_entry.date not in alloy_plan:
                alloy_plan[plan_entry.date] = {}
            if cast_bar not in alloy_plan[plan_entry.date]:
                alloy_plan[plan_entry.date][cast_bar] = 0
            alloy_plan[plan_entry.date][cast_bar] += cast_bar_amount

        report = []
        for date in alloy_plan:
            for cast_bar in alloy_plan[date]:
                report.append({
                    'ORDER': str(date.date()) + '_' + cast_bar.identity,
                    'IDENTITY': cast_bar.alloy.alloy_group_id,
                    'NAME': cast_bar.alloy.name,
                    'AMOUNT': round(alloy_plan[date][cast_bar], 3),
                    'DATE': date,
                    'CODE': cast_bar.identity,
                    'ALLOY_GROUP': cast_bar.alloy.alloy_group_id,
                    'DIAMETER': cast_bar.diameter,
                })

        xlsx_writer.xlsx_write_data(xlsx=xlsx, worksheet=worksheet,
                                    data=sorted(report,
                                                key=lambda i: i['DATE']),
                                    overwrite=overwrite)

    def write_casting_plan_no_collapse_to_xlsx(self, xlsx, worksheet,
                                               overwrite):
        report = []

        for plan_entry in self.plan.plan_entry:
            product = plan_entry.product
            if product is None:
                continue
            if product.is_product_empty():
                continue
            # if product.alias:
            #     product = product.alias
            if product.cast_bar_blank is None:
                continue
            else:
                cast_bar = product.cast_bar_blank
                cast_bar_amount = round(product.cast_bar_blank.products[
                                            product.identity] * plan_entry.amount,
                                        ndigits=3)
            report.append({
                'ORDER': str(plan_entry.date) + '_' + product.identity,
                'IDENTITY': cast_bar.alloy.alloy_group_id,
                'NAME': cast_bar.alloy.name,
                'AMOUNT': round(cast_bar_amount, 3),
                'DATE': plan_entry.date,
                'CODE': cast_bar.identity,
                'ALLOY_GROUP': cast_bar.alloy.alloy_group_id,
                'DIAMETER': cast_bar.diameter,
            })

        xlsx_writer.xlsx_write_data(xlsx=xlsx, worksheet=worksheet,
                                    data=sorted(report,
                                                key=lambda i: i['DATE']),
                                    overwrite=overwrite)


class AlloyGroup(object):
    def __init__(self, group_name):
        self.identity = group_name
        self.alloys = {}


class Alloy(object):
    def __init__(self, identity, name, alloy_group_id):
        self.identity = identity
        self.name = name
        self.alloy_group_id = alloy_group_id

    def __repr__(self):
        return self.identity + self.name + self.alloy_group_id


class CastBar(object):
    def __init__(self, identity, code, name, alloy, diameter):
        self.identity = identity
        self.code = code
        self.name = name
        self.alloy = alloy
        self.diameter = diameter
        self.process = {}
        self.products = {}

    def __repr__(self):
        report = self.identity + ' ' + self.code + ' ' + self.name + ' ' + str(
            self.diameter) + ' '
        if self.alloy is not None:
            report += 'СПЛАВ:' + self.alloy.identity + ' ' + self.alloy.name + ' ' + self.alloy.alloy_group_id
        else:
            report += 'СПЛАВ: не известен'
        return report


class CastBarTechProcess(object):
    def __init__(self, identity, furnace_charge, furnace, batch_size,
                 casting_time):
        self.identity = identity
        self.furnace_charge = furnace_charge
        self.furnace = furnace
        self.batch_size = batch_size
        self.casting_time = casting_time

    def __repr__(self):
        return self.furnace + ' ' + str(self.casting_time)


class Equipment(object):
    def __init__(self, identity, name):
        self.identity = identity
        self.name = name


class Plan(object):
    def __init__(self, name=''):
        self.name = name
        self.plan_entry = []


class PlanEntry(object):
    def __init__(self, order_name, product, amount, entry_date, initial_code,
                 initial_name, initial_order):
        self.order_name = order_name
        self.product = product
        self.amount = amount
        self.date = entry_date
        self.initial_code = initial_code
        self.initial_name = initial_name
        self.initial_order = initial_order


class Product(object):
    def __init__(self, identity, code, name):
        self.identity = identity
        self.code = code
        self.name = name
        self.product_entry = {}
        self.routes = {}
        self.cast_bar = None
        self.scrap_material = False
        self.alias = None
        self.cast_bar_blank = None
        self.diameter = 0

    def __repr__(self):
        report = self.identity + '_' + self.name
        return report

    def is_product_empty(self):
        #  проверяем есть ли операции в маршруте у продукта
        operations_in_routes = False
        for route in self.routes.values():
            if route.operations:
                operations_in_routes = True
        #  если операции есть, то изделие не пустое
        if operations_in_routes:
            return False

        #  если нет спецификации и маршрутов, то изделие пустое
        if not self.product_entry and not self.routes:
            return True
        #  если нет спецификации и все маршруты пустые (без операций), то изделие пустое
        if not self.product_entry and not operations_in_routes:
            return True

        #  проверяем, если все потомки пустые, то и само изделие пустое
        for entry in self.product_entry:
            if not entry.is_product_empty():
                return False

        return True

    def is_product_equal_to(self, another_product):
        #  Если не совпадают по коду, то дальше не проверяем
        if self.code != another_product.code:
            return False

        #  если проверяемые номенклатуры -- отливки, то сравниваем их только по 'марке-диаметру'
        if self.cast_bar:
            if another_product.is_product_empty():
                return True
            if another_product.cast_bar is None:
                return False
            if another_product.cast_bar == self.cast_bar:
                return True
            else:
                return False
        if another_product.cast_bar:
            if self.is_product_empty():
                return True
            if self.cast_bar is None:
                return False

        #  проверяем по потомкам (все потомки должны быть одинаковые)
        for self_entry in self.product_entry:
            i = False
            for another_entry in another_product.product_entry:
                if not (self_entry.is_product_equal_to(another_entry)):
                    continue
                #  если элементы спецификации совпадают, то проверяемся по количеству
                if self.product_entry[self_entry] != \
                        another_product.product_entry[another_entry]:
                    i = True
                    break
            if not i:
                return False

        for another_entry in another_product.product_entry:
            i = False
            for self_entry in self.product_entry:
                if self_entry.is_product_equal_to(another_entry):
                    i = True
                    break
            if not i:
                return False

        for self_route in self.routes.values():
            i = False
            for another_route in another_product.routes.values():
                if self_route.is_route_equal_to(another_route):
                    i = True
                    break
            if not i:
                return False

        for another_route in another_product.routes.values():
            i = False
            for self_route in self.routes.values():
                if self_route.is_route_equal_to(another_route):
                    i = True
                    break
            if not i:
                return False

        return True


class ProductRoute(object):
    def __init__(self, identity):
        self.identity = identity
        self.operations = {}

    def is_route_equal_to(self, another_route):
        self_operations_list = sorted(self.operations.values(),
                                      key=lambda i: i.nop)
        another_operations_list = sorted(another_route.operations.values(),
                                         key=lambda i: i.nop)
        for self_operation, another_operation in zip(self_operations_list,
                                                     another_operations_list):
            if not (self_operation.is_operation_equal_to(another_operation)):
                return False
        return True


class ProductRouteOperation(object):
    def __init__(self, identity, t_pz, t_sht, t_nal, equipment_id,
                 department_id, nop, name):
        self.identity = identity
        self.t_pz = t_pz
        self.t_sht = t_sht
        self.t_nal = t_nal
        self.equipment_id = equipment_id
        self.department_id = department_id
        self.nop = nop
        self.name = name

    def is_operation_equal_to(self, another_operation):
        if another_operation.t_pz != self.t_pz:
            return False
        if another_operation.t_sht != self.t_sht:
            return False
        if another_operation.t_nal != self.t_nal:
            return False
        if another_operation.equipment_id != another_operation.equipment_id:
            return False
        if another_operation.department_id != another_operation.department_id:
            return False
        return True


if __name__ == '__main__':
    session = CastingCalcSession()
    furnaces = ('Илк-1.6 №12_21 1_2', 'Илк-1.6 №3_16_71 1_2')
    session.read_spec_from_xlsx(
        xlsx='/home/work/Downloads/Формат исходных данных_Обновление от 25.01.19.xlsx',
        spec_worksheet='spec')
    session.read_alloys_from_xlsx(xlsx='../input/kuzocm/CODE_ALLOY.xlsx',
                                  worksheet='Sheet1')
    session.read_route_from_xlsx(
        xlsx='/home/work/Downloads/Формат исходных данных_Обновление от 25.01.19.xlsx',
        worksheet='марш')
    session.read_tech_from_xlsx(
        xlsx='/home/work/Downloads/Формат исходных данных_Обновление от 25.01.19.xlsx',
        worksheet='tech-nal')
    session.read_plan_from_xlsx(xlsx='../input/kuzocm/plan.xlsx',
                                worksheet='plan (2)')
    session.read_cast_bars()
    session.remove_all_operations_in_department('05.0')
    session.remove_all_operations_in_department('1_4')
    session.remove_all_operations_in_department('1_3')
    session.remove_all_operations_in_department('1_1')
    session.remove_empty_products()

    # entity_report = sorted(session.operation_time_report(), key=lambda i: i['ENTITY'])
    # xlsx_writer.xlsx_write_data(
    #     xlsx='../input/kuzocm/ilk_report.xlsx',
    #     worksheet='report',
    #     data=entity_report,
    #     overwrite=True
    # )

    session.write_product_alloy_relation_to_xlsx(
        xlsx='../input/kuzocm/product-alloy.xlsx',
        worksheet='spec',
        overwrite=True
    )

    # session.write_casting_spec_to_xlsx(
    #     xlsx='../input/kuzocm/casting_spec.xlsx',
    #     worksheet='spec',
    #     overwrite=True
    # )
    #
    # session.write_casting_plan_to_xlsx(xlsx='../input/kuzocm/casting_plan.xlsx', worksheet='plan', overwrite=True)
    # session.write_casting_plan_no_collapse_to_xlsx(xlsx='../input/kuzocm/casting_plan_no_collapse.xlsx',
    #                                                worksheet='plan', overwrite=True)
    # session.write_casting_route_to_xlsx(xlsx='../input/kuzocm/casting_entity-route.xlsx', worksheet='entity-route',
    #                                     overwrite=True, equipment_list=furnaces)
    # session.write_casting_tech_to_xlsx(xlsx='../input/kuzocm/casting_tech-nal.xlsx', worksheet='tech-nal',
    #                                    overwrite=True, equipment_list=furnaces)
    #
    # session.write_casting_alloy_group_report_to_xlsx(xlsx='../input/kuzocm/casting_alloy_group_report.xlsx')
    # session.write_casting_alloy_report_to_xlsx(xlsx='../input/kuzocm/casting_alloy_report.xlsx')
    #
    # session.write_mill_spec_to_xlsx(xlsx='../input/kuzocm/mill_spec.xlsx', worksheet='spec', overwrite=True)
    # session.write_mill_route_to_xlsx(xlsx='../input/kuzocm/mill_route.xlsx', worksheet='entity-route', overwrite=True)
    # session.write_mill_tech_to_xlsx(xlsx='../input/kuzocm/mill_tech.xlsx', worksheet='tech-nal', overwrite=True)
    # session.write_mill_plan_to_xlsx(xlsx='../input/kuzocm/mill_plan.xlsx', worksheet='plan', overwrite=True)
    #
