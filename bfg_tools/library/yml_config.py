from datetime import datetime

import yaml
from dateutil import parser


class ConfigChapter(object):

    def __init__(self, data, columns=()):
        self.columns = {}
        self.file = data["file"]

        if "worksheet" in data:
            self.worksheet = data["worksheet"]
        else:
            self.worksheet = "Sheet 1"

        if "period" in data:
            self.period = data["period"]

        if "columns" in data:
            data_columns = data["columns"]
            for key in columns:
                self.columns[key] = data_columns[key]


class PlanExplosionConfig(object):
    PLAN_COLUMNS = (
    "ORDER", "AMOUNT", "DATE", "CODE", "NAME", "IDENTITY", "NUMBER_OF_BATCHES")
    SPEC_COLUMNS = (
    "PARENT_CODE", "PARENT_NAME", "PARENT_IDENTITY", "CODE", "NAME", "IDENTITY",
    "AMOUNT")
    BATCH_SIZE_COLUMNS = ("CODE", "BATCH_SIZE")
    TECH_COLUMNS = (
    "OP_ID", "PRODUCT", "T_SHT", "T_PZ", "EQUIPMENT", "DEPARTMENT")
    OUTPUT_COLUMNS = ("ORDER", "IDENTITY", "NAME", "AMOUNT", "DATE", "CODE")
    EQUIPMENT_COLUMNS = (
    "EQUIPMENT_ID", "AMOUNT", "UTILIZATION", "NAME", "DEPT_ID")

    def __init__(self, config):
        with open(config, 'r', encoding="utf-8") as stream:
            data = yaml.load(stream)

        self.plan = ConfigChapter(data["plan"], self.PLAN_COLUMNS)
        self.spec = ConfigChapter(data["spec"], self.SPEC_COLUMNS)
        self.tech = ConfigChapter(data["tech"], self.TECH_COLUMNS)
        self.equipment = ConfigChapter(data["equipment"],
                                       self.EQUIPMENT_COLUMNS)
        self.batch_size = ConfigChapter(data["batch_size"],
                                        self.BATCH_SIZE_COLUMNS)
        self.output = ConfigChapter(data["output"], self.OUTPUT_COLUMNS)
        self.rules = data["rules"]
        self.rules.update(
            {"start": datetime.strptime(self.rules["start"], "%d/%m/%Y %H:%M")})
        temp = []
        for day in self.rules["holidays"]:
            day = parser.parse(day)
            temp.append(day)
        self.rules["holidays"] = temp


class PlanPriorityConfig(object):
    PLAN_COLUMNS = ("ORDER", "AMOUNT", "DATE", "CODE", "NAME", "IDENTITY")
    SPECIAL_ORDERS_COLUMNS = ("ORDER", "PRIORITY")
    OUTPUT_COLUMNS = ("ORDER", "IDENTITY", "NAME", "AMOUNT", "DATE", "CODE")

    def __init__(self, config):
        with open(config, 'r', encoding="utf-8") as stream:
            data = yaml.load(stream)

        self.plan = ConfigChapter(data["plan"], self.PLAN_COLUMNS)
        self.special_orders = ConfigChapter(data["special_orders"],
                                            self.SPECIAL_ORDERS_COLUMNS)
        self.output = ConfigChapter(data["output"], self.OUTPUT_COLUMNS)


class PlanTransformConfig(object):
    PLAN_COLUMNS = ("ORDER", "AMOUNT", "DATE", "CODE", "NAME", "IDENTITY")

    def __init__(self, config):
        with open(config, 'r', encoding="utf-8") as stream:
            data = yaml.load(stream)

        self.plan = ConfigChapter(data["plan"], self.PLAN_COLUMNS)
        self.output = ConfigChapter(data["output"])


class ScheduleAnalyserConfig(object):
    TRANSACTIONS_COLUMNS = (
    "START_TIME", "STOP_TIME", "OPERATION", "PRODUCT", "EQUIPMENT", "BATCH",
    "SIZE", "NOP")

    def __init__(self, config):
        with open(config, 'r', encoding="utf-8") as stream:
            data = yaml.load(stream)

        self.transactions = ConfigChapter(data["input"],
                                          self.TRANSACTIONS_COLUMNS)
        self.output_product = ConfigChapter(data["output_product"])
        self.output_equipment = ConfigChapter(data["output_equipment"])
        self.output_production_launch = ConfigChapter(
            data["output_production_launch"])
        self.output_production_release = ConfigChapter(
            data["output_production_release"])
        self.output_plan = ConfigChapter(data["output_plan"])
        self.output_tasks = ConfigChapter(data["output_tasks"])
        self.rules = data["rules"]
        self.rules.update({"from": parser.parse(self.rules["from"])})
        self.rules.update({"to": parser.parse(self.rules["to"])})
        self.rules["tasks_from"] = parser.parse(self.rules["tasks_from"])
        self.rules["tasks_to"] = parser.parse(self.rules["tasks_to"])


class ScheduleAnalyserRestConfig(object):

    def __init__(self, config):
        with open(config, 'r', encoding="utf-8") as stream:
            data = yaml.load(stream)

        self.input = data["input"]
        self.output_product = ConfigChapter(data["output_product"])
        self.output_equipment = ConfigChapter(data["output_equipment"])
        self.output_equipment_machinetime = ConfigChapter(
            data["output_equipment_machinetime"])
        self.output_production_launch = ConfigChapter(
            data["output_production_launch"])
        self.output_production_release = ConfigChapter(
            data["output_production_release"])
        self.output_production_wip = ConfigChapter(
            data["output_production_wip"])
        self.output_plan = ConfigChapter(data["output_plan"])
        self.output_tasks = ConfigChapter(data["output_tasks"])
        self.rules = data["rules"]
        self.rules["from"] = parser.parse(self.rules["from"])
        self.rules["to"] = parser.parse(self.rules["to"])
        try:
            self.rules["tasks_from"] = parser.parse(self.rules["tasks_from"])
        except ValueError:
            print("Значение {} в поле rules|tasks_from не "
                  "удалось распознать в качестве даты".format(
                self.rules["tasks_from"]))


# проверяем библиотеку
if __name__ == "__main__":
    cur_config = PlanExplosionConfig("../plan_explosion.yml")
    print(cur_config.plan.file)
    print(cur_config.plan.columns)
    print(cur_config.tech.columns)
    print(cur_config.rules)
