import math
from collections import defaultdict
from datetime import datetime, timedelta

from openpyxl.utils import exceptions

from bfg_tools.library import csv_reader, xlsx_reader


def calculate_finish_date(start: datetime, work_time_hours: float,
                          holidays: list, day_off: list):
    work_hours = 0
    current_date = start
    holidays_list = [x.date() for x in holidays]
    while work_hours < work_time_hours:
        current_date += timedelta(1)
        if current_date.date() not in holidays_list and current_date.isoweekday() not in day_off:
            work_hours += 24
    return current_date


def calculate_start_date(finish: datetime, work_time_hours: float,
                         holidays: list, day_off: list):
    work_hours = 0
    current_date = finish
    holidays_list = [x.date() for x in holidays]
    while work_hours < work_time_hours:
        current_date -= timedelta(1)
        if current_date.date() not in holidays_list and current_date.isoweekday() not in day_off:
            work_hours += 24
    return current_date


class Entities(object):
    SPEC_COLUMNS = {"PARENT_CODE": "PARENT_CODE", "PARENT_NAME": "PARENT_NAME",
                    "PARENT_IDENTITY": "PARENT_IDENTITY", "CODE": "CODE",
                    "NAME": "NAME",
                    "IDENTITY": "IDENTITY", "AMOUNT": "AMOUNT"}

    BATCH_SIZE_COLUMNS = {"CODE": "CODE", "BATCH_SIZE": "BATCH_SIZE"}

    def __init__(self):
        self.rows = {}

    def get_product_with_id(self, product_id):
        try:
            return self.rows[str(product_id)]
        except KeyError:
            return None

    def get_product_with_code_name(self, code, name):
        if (code is None) and (name is None):
            return None
        for entity in self.rows:
            if (str(self.rows[entity].code) == str(code)) and (
                    str(self.rows[entity].name) == str(name)):
                return self.rows[entity]
        return None

    def get_product_with_code(self, code):
        if code is None:
            return None
        for entity in self.rows:
            if str(self.rows[entity].code) == str(code):
                return self.rows[entity]
        return None

    def add(self, product_id, product_code, product_name, batch_size=0):
        if product_id is None:
            return
        if product_name is None:
            product_name = ""
        if product_code is None:
            product_code = ""
        if self.get_product_with_id(product_id) is None:
            self.rows[str(product_id)] = Product(str(product_id),
                                                 str(product_code),
                                                 str(product_name), batch_size)

    def read_batch_size_from_xlsx(self, xlsx, batch_size_columns=None,
                                  batch_size_worksheet="batch-size"):

        if batch_size_columns is None:
            batch_size_columns = self.BATCH_SIZE_COLUMNS

        try:
            xlsx_data = xlsx_reader.XlsxReadData(xlsx=xlsx,
                                                 worksheet=batch_size_worksheet,
                                                 columns=batch_size_columns).worksheets[
                batch_size_worksheet]
        except exceptions.InvalidFileException:
            xlsx_data = csv_reader.CsvReadData(csv_file=xlsx,
                                               columns=batch_size_columns).rows

        for row in xlsx_data:
            product_id = row["CODE"]
            try:
                if type(row["BATCH_SIZE"]) is str:
                    batch_size = float(row["BATCH_SIZE"].replace(",", "."))
                else:
                    batch_size = float(row["BATCH_SIZE"])
            except(ValueError, AttributeError):
                batch_size = 0
                print("Неверное значение ячейки столбца "
                      "BATCH SIZE в строке {}, значение в ячейке \"{}\"".format(
                    row["ROW_NUMBER"], row["BATCH_SIZE"]))

            if self.get_product_with_id(product_id) is None:
                pass
            else:
                self.get_product_with_id(product_id).batch_size = batch_size

    def read_spec_from_xlsx(self, xlsx, spec_columns=None,
                            spec_worksheet="spec"):

        if spec_columns is None:
            spec_columns = self.SPEC_COLUMNS

        try:
            xlsx_data = xlsx_reader.XlsxReadData(xlsx=xlsx,
                                                 worksheet=spec_worksheet,
                                                 columns=spec_columns).worksheets[
                spec_worksheet]
        except exceptions.InvalidFileException:
            print("Считать как xlsx не удалось, пробуем считать как csv")
            xlsx_data = csv_reader.CsvReadData(csv_file=xlsx,
                                               columns=spec_columns).rows

        for row in xlsx_data:
            if row["ROW_NUMBER"] == 1:
                continue
            parent_id = row["PARENT_CODE"]
            parent_code = row["PARENT_IDENTITY"]
            parent_name = row["PARENT_NAME"]
            child_id = row["CODE"]
            child_code = row["IDENTITY"]
            child_name = row["NAME"]
            try:
                if type(row["AMOUNT"]) is str:
                    amount = float(row["AMOUNT"].replace(",", "."))
                else:
                    amount = float(row["AMOUNT"])
            except(ValueError, AttributeError):
                amount = 0
                print("Неверное значение ячейки столбца "
                      "AMOUNT в строке {}, значение в ячейке \"{}\"".format(
                    row["ROW_NUMBER"], row["AMOUNT"]))

            self.add(parent_id, parent_code, parent_name)
            self.add(child_id, child_code, child_name)

            parent = self.get_product_with_id(parent_id)
            child = self.get_product_with_id(child_id)

            if (parent is not None) and (child is not None):
                parent.add_child(child, amount)

    def recalculate_entities_workload(self):
        for product_id in self.rows:
            self.get_product_with_id(product_id).recalculate_workload()


class Product(object):  # изделие
    def __init__(self, prod_id, code, name, batch_size=0):
        self.id = prod_id
        self.code = code
        self.name = name
        self.batch_size = batch_size
        self.tech_proc = TechProcess()
        self.product_entry = []
        self.whole_workload = {}
        self.whole_operation_workload = {}
        self.whole_preparing_workload = {}

    def __repr__(self):
        return self.description

    def __iter__(self):
        return iter(self.tech_proc.operations)

    @property
    def is_raw_material(self):
        if self.tech_proc.operations or self.product_entry:
            return False
        else:
            return True

    @property
    def is_simplest_part(self):
        for each in self.product_entry:
            if each.product.is_raw_material:
                pass
            else:
                return False
        return True

    @property
    def description(self):
        return "ИД:" + self.id + " Шифр:" + self.code + " Имя:" + self.name

    @property
    def max_t_sht(self):
        result = 0
        for each_operation in self.tech_proc.operations:
            result = max(result, each_operation.t_sht)
        return result

    def calculate_batch_size(self, default_batch_operation_hours):
        if self.batch_size == 0:
            batch_size = max(round(
                default_batch_operation_hours / max(self.max_t_sht, 0.0001)), 1)
            for child in self.product_entry:
                batch_size = min(batch_size,
                                 round(child.product.calculate_batch_size(
                                     default_batch_operation_hours) /
                                       child.amount))
                batch_size = max(batch_size, 1)
            print(
                "Для изделия {} в исходных данных не был задан размер партии. Размер партии расчитан "
                "автоматически и составляет {}".format(self, batch_size))
            self.batch_size = batch_size
        return self.batch_size

    def specification_report(self, _i=0):
        report = "\n" + "-" * _i
        if _i == 0:
            report += self.description + "\n" + "-"
        _i += 1

        for child in self.product_entry:
            report += str(child.product.description) + " Количество:" + str(
                child.amount) \
                      + "шт" + child.product.specification_report(_i)

        _i -= 1
        return report

    @property
    def labor_per_part(self):  # трудоемкость детали
        labor = 0
        for operation in self:
            labor += operation.operation_hours(self.batch_size)
        for child in self.product_entry:
            labor += child.product.labor_per_part * child.amount
        return labor

    def add_child(self, child, amount):
        if child is None:
            return
        if amount == 0:
            return
        if not (self.is_product_in_spec(child)):
            self.product_entry.append(ProductEntry(child, amount))

    def is_product_in_spec(self, child):
        for spec_row in self.product_entry:
            if spec_row.product == child:
                return True
        return False

    def recalculate_workload(self):
        self.whole_workload = self.__whole_workload()
        self.whole_operation_workload = self.__whole_operation_workload()
        self.whole_preparing_workload = self.__whole_preparing_workload()

    def __whole_workload(self,
                         _report=None):  # загрузка выбранного рабочего центра по продукту
        if _report is None:
            _report = defaultdict(float)
        for operation in self.tech_proc:
            _report[operation.work_center.id] += operation.operation_hours(
                self.batch_size)
        for child in self.product_entry:
            child.product.__whole_workload(_report)
        return _report

    def __whole_operation_workload(self,
                                   _report=None):  # загрузка выбранного рабочего центра по продукту
        if _report is None:
            _report = defaultdict(float)
        for operation in self.tech_proc:
            _report[operation.work_center.id] += operation.t_sht
        for child in self.product_entry:
            child.product.__whole_operation_workload(_report)
        return _report

    def __whole_preparing_workload(self, _report=None,
                                   amount=1):  # загрузка выбранного рабочего центра по продукту
        if _report is None:
            _report = defaultdict(float)
        for operation in self.tech_proc:
            _report[operation.work_center.id] += operation.t_pz / amount
        for child in self.product_entry:
            child.product.__whole_preparing_workload(_report,
                                                     amount=amount * child.amount)
        return _report

    def labor_per_plan(self, plan):  # трудоемкость продукта в плане
        return plan.labor_per_product(self)

    def simplify_specification_dict(self, amount, _report=None):
        """
        Пробую сделать функцию, где на выходе словарь, состоящий только из деталей низшего уровня изделия
        :param amount: количество деталей в спецификации
        :param _report:
        :return:
        """
        if _report is None:
            _report = defaultdict(float)

        if self.is_raw_material:
            return _report

        if self.is_simplest_part:
            _report[self] += amount
            return _report

        for spec_row in self.product_entry:
            _report.update(spec_row.product.simplify_specification_dict(
                amount * spec_row.amount, _report))

        return _report


class ProductEntry(object):
    def __init__(self, product, amount):
        self.product = product
        self.amount = amount


class WorkCenter(object):  # рабочий центр
    def __init__(self, wc_id, name="", amount=1, utilization_factor=1):
        self.id = wc_id
        self.name = name
        self.amount = amount
        self.utilization_factor = utilization_factor

    def workload(self, plan, date_from=None, date_to=None):
        # загрузка рабочего центра по заданному плану
        workload = 0
        for entry in plan:
            if date_from is None and date_to is None:
                workload += entry.workload(self)
                continue
            if date_from < entry.date <= date_to:
                workload += entry.workload(self)
        return workload

    def workload_percents(self, plan,
                          period_hours):  # загрузка выбранного рабочего центра в %
        if period_hours == 0:
            period_hours = 1
        return round(self.workload(plan) * 100 / self.amount / period_hours, 1)


class Factory(
    object):  # завод, который состоит из большого количества рабочих центров
    EQUIPMENT_COLUMNS = {"EQUIPMENT_ID": "EQUIPMENT_ID", "AMOUNT": "AMOUNT",
                         "UTILIZATION": "UTILIZATION", "NAME": "NAME",
                         "DEPT_ID": "DEPT_ID"}

    def __init__(self):
        self.workcenter = []
        self.wc_sorted_list = {}

    def __iter__(self):
        return iter(self.workcenter)

    def __repr__(self):
        report = ""
        for each_workcenter in self:
            report += each_workcenter.id
        return report

    def add(self, wc_id, amount, utilization_factor, name=None):
        if name is None:
            name = wc_id
        if not self.get_workcenter_with_id(wc_id):
            self.workcenter.append(
                WorkCenter(wc_id=str(wc_id), amount=amount, name=str(name),
                           utilization_factor=utilization_factor))
        else:
            self.get_workcenter_with_id(wc_id).amount += amount

    def get_workcenter_with_id(self, wc_id):
        for each_work_center in self:
            if each_work_center.id == wc_id:
                return each_work_center

    def workcenter_sorted_list(self, plan):
        if plan in self.wc_sorted_list:
            return self.wc_sorted_list[plan]
        from operator import itemgetter
        wc_workload = {}
        for each_work_center in self:
            wc_workload[each_work_center] = each_work_center.workload(plan) / \
                                            each_work_center.amount / \
                                            each_work_center.utilization_factor
        # wc_workload = sorted(wc_workload.items(), key=lambda t: t[1], reverse=True)
        wc_workload = sorted(wc_workload.items(), key=itemgetter(1),
                             reverse=True)
        wc_sorted_list = []
        for i in wc_workload:
            wc_sorted_list.append(i[0])
        self.wc_sorted_list[plan] = wc_sorted_list
        return wc_sorted_list

    def read_from_xlsx(self, xlsx, equipment_columns=None,
                       equipment_worksheet="equipment"):
        if equipment_columns is None:
            equipment_columns = self.EQUIPMENT_COLUMNS

        try:
            xlsx_data = xlsx_reader.XlsxReadData(xlsx=xlsx,
                                                 worksheet=equipment_worksheet,
                                                 columns=equipment_columns).worksheets[
                equipment_worksheet]
        except exceptions.InvalidFileException:
            xlsx_data = csv_reader.CsvReadData(csv_file=xlsx,
                                               columns=equipment_columns).rows

        for row in xlsx_data:
            if row["ROW_NUMBER"] == 1:
                continue
            equipment_id = str(row["EQUIPMENT_ID"]) + "_" + str(row["DEPT_ID"])
            try:
                amount = int(row["AMOUNT"])
            except(ValueError, AttributeError):
                print("Неверное значение ячейки столбца "
                      "AMOUNT в строке {}, значение в ячейке \"{}\"".format(
                    row["ROW_NUMBER"], row["AMOUNT"]))
                continue
            try:
                if type(row["UTILIZATION"]) is str:
                    utilization_factor = float(
                        row["UTILIZATION"].replace(",", "."))
                else:
                    utilization_factor = float(row["UTILIZATION"])
            except(ValueError, AttributeError):
                print("Неверное значение ячейки столбца "
                      "AMOUNT в строке {}, значение в ячейке \"{}\"".format(
                    row["ROW_NUMBER"], row["UTILIZATION"]))
                continue
            name = row["NAME"]

            self.add(wc_id=equipment_id, amount=amount, name=name,
                     utilization_factor=utilization_factor)


class Operation(object):  # операция
    def __init__(self, work_center, t_sht, t_pz):
        self.work_center = work_center
        self.t_sht = t_sht
        self.t_pz = t_pz

    def operation_hours(self, batch_size):
        if batch_size == 0:
            return 0
        return self.t_sht + self.t_pz / batch_size


class TechProcess(object):  # техпроцесс
    def __init__(self):
        self.operations = []

    def __repr__(self):
        report = ""
        for operation in self:
            report += str(operation.work_center.id) + " Тпз:" + str(
                operation.t_pz) + "ч. Тшт" \
                      + str(operation.t_sht) + "\n"
        return report

    def __iter__(self):
        return iter(self.operations)

    def workload(self, work_center,
                 batch_size):  # загрузка выбранного рабочего центра по техпроцессу
        workload = 0
        for operation in self:
            if operation.work_center == work_center:
                workload += operation.operation_hours(batch_size)
        return workload

    def add_operation(self, work_center, t_pz,
                      t_sht):  # добавляем операцию в технологический процесс
        self.operations.append(Operation(work_center, t_pz=t_pz, t_sht=t_sht))


class PlanEntry(object):  # строка плана
    def __init__(self, product, amount, order_name, initial_name=None,
                 initial_order=None, initial_code=None,
                 entry_date=datetime.now(),
                 min_number_of_batches=1):
        self.product = product
        self.amount = amount
        self.date = entry_date
        self.order_name = order_name
        self.initial_name = initial_name
        self.initial_code = initial_code
        self.initial_order = initial_order
        self.min_number_of_batches = min_number_of_batches
        self.number_of_batches = math.ceil(
            round(self.amount, 2) / round(self.product.batch_size, 2))

    def __repr__(self):
        return self.product.description + "Количество:" + str(
            self.amount) + " Дата:" + str(self.date) + "\n"

    @property
    def description(self):
        return self.product.description + "Количество:" + str(
            self.amount) + " \nДата:" + str(self.date) + "\n \n"

    @property
    def labor(self):
        return self.product.labor_per_part * self.amount

    def workload(self,
                 work_center):  # загрузка выбранного оборудования по строке плана
        workload = self.product.whole_operation_workload[
                       work_center.id] * self.amount
        workload += self.product.whole_preparing_workload[
                        work_center.id] * self.number_of_batches
        return workload


class Plan(object):  # план
    PLAN_COLUMNS = {"ORDER": "ORDER", "CODE": "CODE",
                    "AMOUNT": "AMOUNT", "DATE": "DATE"}
    PLAN_TITLE = ("ORDER", "IDENTITY", "NAME", "AMOUNT", "DATE", "CODE")

    def __init__(self, entities, name=""):
        self.name = name
        self.plan_entry = []
        self.entities = entities

    def __repr__(self):
        report = "План:" + self.name + "\n"
        for entry in self.plan_entry:
            report += entry.description
        return report

    def __iter__(self):
        return iter(self.plan_entry)

    def concatenate_with(self, other):
        for entry in other.plan_entry:
            self.plan_entry.append(entry)

    @property
    def start_date(self):
        result = datetime.max
        for entry in self.plan_entry:
            result = min(result, entry.date)
        return result

    @property
    def finish_date(self):
        result = datetime.min
        for entry in self.plan_entry:
            result = max(result, entry.date)
        return result

    @property
    def number_of_entries(self):  # количество строк в плане
        return len(self.plan_entry)

    @property
    def total_amount(self):  # количество штук/метров в плане
        total_amount = 0
        for entry in self.plan_entry:
            total_amount += entry.amount
        return total_amount

    @property
    def product_set(self):  # множество уникальных продуктов в плане
        # product_set = set()
        # for entry in self.plan_entry:
        #     product_set.add(entry.product)
        # return product_set
        return set(self.plan_entry)

    @property
    def sorted_product_set(
            self):  # множество уникальных продуктов в плане, сортированных по трудоемкости
        return sorted(self.product_set,
                      key=lambda product: product.labor_per_plan(self),
                      reverse=True)

    @property
    def sorted_plan_entry_set(self):
        # строки плана, сортированные по трудомекости
        return sorted(self.plan_entry, key=lambda entry: entry.labor,
                      reverse=True)

    def exclude_dayoffs(self, weekdays, holidays_dates):
        add_days = 0
        for entry in sorted(self.plan_entry, key=lambda x: x.date):
            entry.date += timedelta(days=add_days)
            while (entry.date.isoweekday() in weekdays) or (
                    entry.date in holidays_dates):
                add_days += 1
                entry.date += timedelta(days=1)

    def simple_plan(self):
        """
        На входе план со сложными изделиями, на выходе план только с деталями в количестве,
        необходимом для производства всех изделий плана
        :return:
        """
        result_plan = Plan(self.entities)
        for entry in self.plan_entry:
            for part in entry.product.simplify_specification_dict(entry.amount):
                amount = \
                entry.product.simplify_specification_dict(entry.amount)[part]
                part_id = part.id
                result_plan.add(part_id, amount, entry.date)
        return result_plan

    def sorted_by_bottleneck_plan_entry_set(self, factory=Factory(),
                                            accuracy=1):
        # строки плана, сортированные по загрузке самых загруженных классов РЦ
        wc_sorted_list = factory.workcenter_sorted_list(self)
        result = self.plan_entry
        # сортируем последовательно от незагруженных к самым загруженным рабочим центрам
        for i in range(len(wc_sorted_list) - 1, -1, -1):
            result = sorted(result, key=lambda entry: round(
                entry.workload(wc_sorted_list[i]) / accuracy / entry.amount),
                            reverse=True)
        # result = sorted(result, key=lambda entry: entry.date)

        return result

    def add(self, product_id, amount=0.0, date=datetime.now(),
            code=None, name=None, order_name=None,
            min_number_of_batches=1):  # добавить строку в план
        product = self.entities.get_product_with_id(product_id)
        if product is None:
            print(
                "Не найден продукт {}, пробуем определять по поляем IDENTITY и NAME".format(
                    product_id))
            product = self.entities.get_product_with_code_name(code, name)
            if product is None:
                print(
                    "Не найден продукт {} {}, пробуем определить только по IDENTITY".format(
                        code, name))
                product = self.entities.get_product_with_code(code)
                if product is None:
                    print("Не найден продукт {}".format(code))
                    return
                else:
                    print("Найден продукт {}".format(product.id))
            else:
                print("Найден продукт {}".format(product.id))
        if amount == 0:
            print("Нулевое количество")
            return
        self.plan_entry.append(
            PlanEntry(product=product, amount=amount, entry_date=date,
                      order_name=order_name,
                      initial_name=name, initial_code=code,
                      initial_order=order_name,
                      min_number_of_batches=min_number_of_batches))

    def clean(self):  # удалить все строки из плана
        self.plan_entry.clear()

    def remove(self, entry):
        self.plan_entry.remove(entry)

    def copy(self, from_date, to_date):
        """
        Копирует строки плана в другой план, с ограничением по дате запуска строк плана
        :param from_date: от этой даты
        :type from_date: datetime
        :param to_date: и до этой даты
        :type to_date: datetime
        :return: возвращает план
        """
        temp_copy = Plan(self.entities)
        for each in self.plan_entry:
            if from_date <= each.date < to_date:
                temp_copy.add(product_id=each.product.id, amount=each.amount,
                              date=each.date, order_name=each.order_name,
                              code=each.initial_code, name=each.initial_name,
                              min_number_of_batches=each.min_number_of_batches)
        return temp_copy

    def workload(self, work_center):
        workload = 0
        for entry in self:
            workload += entry.workload(work_center)
        return workload

    def amount_of_products(self, product, date_from=datetime.min,
                           date_to=datetime.max):
        count = 0
        for entry in self:
            if (entry.product == product) and (
                    date_from < entry.date <= date_to):
                count += entry.amount
        return count

    def labor_per_product(self, product):
        labor = 0
        for entry in self:
            if entry.product == product:
                labor += entry.product.labor_per_part * entry.amount
        return labor

    def get_tech_proc_of_product_with_id(self, product_id):
        return self.get_product_with_id(product_id).tech_proc

    def get_product_with_id(self, product_id):
        for entry in self:
            if entry.product.id == product_id:
                return entry.product
        return None

    def read_plan_from_xlsx(self, xlsx, plan_columns=None,
                            plan_worksheet="plan",
                            default_batch_operation_hours=8):
        # считываем из xlsx файла план
        if plan_columns is None:
            plan_columns = self.PLAN_COLUMNS

        try:
            xlsx_data = xlsx_reader.XlsxReadData(xlsx=xlsx,
                                                 worksheet=plan_worksheet,
                                                 columns=plan_columns).worksheets[
                plan_worksheet]
        except exceptions.InvalidFileException:
            xlsx_data = csv_reader.CsvReadData(csv_file=xlsx,
                                               columns=plan_columns).rows

        for row in xlsx_data:
            if row["ROW_NUMBER"] == 1:
                continue
            order_name = row["ORDER"]
            product_id = row["CODE"]
            product_name = row["NAME"]
            product_code = row["IDENTITY"]
            try:
                if type(row["AMOUNT"]) is str:
                    entry_amount = float(row["AMOUNT"].replace(",", "."))
                else:
                    entry_amount = float(row["AMOUNT"])
            except(ValueError, AttributeError):
                print("Неверное значение ячейки столбца "
                      "AMOUNT в строке {}, значение ячейки {}".format(
                    row["ROW_NUMBER"], row["AMOUNT"]))
                continue

            try:
                min_number_of_batches = int(row["NUMBER_OF_BATCHES"])
            except(ValueError, AttributeError):
                print("Неверное значение ячейки столбца "
                      "NUMBER_OF_BATCHES в строке {}, значение ячейки {}".format(
                    row["ROW_NUMBER"],
                    row["NUMBER_OF_BATCHES"]))
                continue

            try:
                entry_date = datetime.combine(datetime.date(row["DATE"]),
                                              datetime.time(row["DATE"]))
            except(TypeError, ValueError, AttributeError):
                entry_date = datetime.now()
                print("Неверное значение ячейки столбца "
                      "DATE в строке {}, значение ячейки {}".format(
                    row["ROW_NUMBER"], row["DATE"]))
            if entry_amount > 0:
                product = self.entities.get_product_with_id(product_id)
                if product is not None:
                    if product.batch_size == 0:
                        product.calculate_batch_size(
                            default_batch_operation_hours=default_batch_operation_hours)
                self.add(product_id=product_id, amount=entry_amount,
                         date=entry_date,
                         code=product_code, name=product_name,
                         order_name=order_name,
                         min_number_of_batches=min_number_of_batches)

            else:
                print("Изделие {} из заказа {} в плане с "
                      "нулевым количеством. При расчете учтено не будет".format(
                    product_id, order_name))

        self.entities.recalculate_entities_workload()

    def collapse(self):
        """
        метод для "сворачиания" плана -- в нем остаются только уникальные изделия
        с суммами по количеству и самой ранней датой из всех строк
        """
        product_basket = defaultdict(
            lambda: {"Amount": 0.0, "Date": datetime.max})
        # product_basket = {"Amount": defaultdict(float),
        #                   # просто максимальная дата
        #                   "Date": defaultdict(lambda: datetime.max)}
        for entry in self:
            product_basket[entry.product]["Amount"] += entry.amount
            product_basket[entry.product]["Date"] = min(entry.date,
                                                        product_basket[
                                                            entry.product][
                                                            "Date"])

        self.clean()  # почистим все entry в заказе
        for entry in product_basket.keys():  # и заполним заново
            entry_amount = product_basket[entry]["Amount"]
            entry_date = product_basket[entry]["Date"]
            self.add(entry.id, entry_amount, entry_date)

        print("Сворачивание плана проведено успешно")

    def moveto(self, other_plan, entry, date=datetime.now(), log=False,
               number_of_batches=1):

        num = 1

        while num <= number_of_batches:

            other_plan.add(product_id=entry.product.id,
                           amount=min(round(entry.product.batch_size, 2),
                                      round(entry.amount, 2)),
                           date=date, order_name=entry.order_name,
                           code=entry.initial_code, name=entry.initial_name)
            if log:
                print(
                    "Добавлено изделие {} из заказа {}, размер партии {}".format(
                        entry.product.code, entry.order_name,
                        min(entry.product.batch_size,
                            round(entry.amount, 2))))
            if entry.amount > round(entry.product.batch_size, 2):
                entry.amount -= round(entry.product.batch_size, 2)
            else:
                entry.amount = 0
                self.remove(entry)
                break

            num += 1

        if (
                entry.amount >= number_of_batches * entry.product.batch_size) or entry.amount == 0:
            return

        while entry:

            other_plan.add(product_id=entry.product.id,
                           amount=min(round(entry.product.batch_size, 2),
                                      round(entry.amount, 2)),
                           date=date, order_name=entry.order_name,
                           code=entry.initial_code, name=entry.initial_name)
            if log:
                print(
                    "Добавлено изделие {} из заказа {}, размер партии {}".format(
                        entry.product.code, entry.order_name,
                        min(entry.product.batch_size,
                            round(entry.amount, 2))))
            if entry.amount > round(entry.product.batch_size, 2):
                entry.amount -= round(entry.product.batch_size, 2)
            else:
                entry.amount = 0
                self.remove(entry)
                break

    def write_to_xlsx(self, xlsx, worksheet="plan"):  # запись плана в xlsx
        from bfg_tools.library import xlsx_writer

        i = 0
        report = []
        for entry in self:
            i += 1
            report.append({
                "INITIAL_ORDER": str(entry.initial_order),
                "INITIAL_IDENTITY": str(entry.initial_code),
                "INITIAL_NAME": str(entry.initial_name),
                "AMOUNT": entry.amount,
                "DATE": entry.date,
                "ORDER": str(entry.order_name) + "_" + str(
                    i) + "_" + entry.product.code,
                "IDENTITY": entry.product.code,
                "NAME": entry.product.name,
                "CODE": entry.product.id
            })
        xlsx_writer.xlsx_write_data(xlsx=xlsx, worksheet=worksheet, data=report)


class CalcSession(
    object):  # сессия расчета объединяет план и завод для расчета загрузки

    TECH_COLUMNS = {"OP_ID": "OP_ID", "PRODUCT": "CODE", "T_SHT": "T_SHT",
                    "T_PZ": "T_PZ", "EQUIPMENT": "EQUIPMENT_ID",
                    "DEPARTMENT": "DEPT_ID"}

    def __init__(self, entities=None, plan=None, factory=None):
        if plan is None:
            self.plan = Plan(entities, "Новый план")
        else:
            self.plan = plan
        if factory is None:
            self.factory = Factory()
        else:
            self.factory = factory
        self.entities = self.plan.entities
        self.overload = dict()

    def calculate_workcenter_utilization(self, period_hours=1):
        report = {}
        for each_work_center in self.factory:
            report[each_work_center.id + "_" + each_work_center.name] = \
                each_work_center.workload_percents(self.plan, period_hours)
        return sorted(report.items(), key=lambda t: t[1], reverse=True)

    def big_workload_report(self, start_date, holidays, day_off, accuracy=1):
        """
        Генерация отчета по загрузке всех рабочих центров по плану в разрезе всех изделий плана
        Изделия сортированы по загрузке узкого места, рабочие центры сортированы по загрузке
        :return:
        """
        report = "\n \n ### Ниже представлен отчет по загрузке самых загруженных классов РЦ ###"
        i = 0
        for each_wc in self.factory.workcenter_sorted_list(self.plan):
            i += 1
            if i > 5:
                break
            last_date = calculate_finish_date(start=start_date,
                                              holidays=holidays,
                                              day_off=day_off,
                                              work_time_hours=(each_wc.workload(
                                                  self.plan) /
                                                               each_wc.amount / each_wc.utilization_factor)).date()
            report += "\nРЦ: {} - {}, Кол-во: {} шт., " \
                      "Загрузка средняя на 1 станок: {:.2f} ч.," \
                      "Ожидаемая дата завершения работ {} \n".format(each_wc.id,
                                                                     each_wc.name,
                                                                     each_wc.amount,
                                                                     each_wc.workload(
                                                                         self.plan) / each_wc.amount,
                                                                     last_date
                                                                     )

            for each_entry in self.plan.sorted_by_bottleneck_plan_entry_set(
                    self.factory, accuracy=accuracy):
                whole_workload = each_entry.product.whole_operation_workload[
                                     each_wc.id] * each_entry.amount + \
                                 each_entry.product.whole_preparing_workload[
                                     each_wc.id] * each_entry.number_of_batches
                if each_entry.amount * each_entry.product.whole_workload[
                    each_wc.id] > 0:
                    report += "    Заказ: {} Номенклатура: {} - {}, Кол-во в плане: {}, " \
                              "Загрузка от 1 штуки: {:.5f}, " \
                              "Загрузка на весь план: {:.2f} \n".format(
                        each_entry.order_name,
                        each_entry.product.id, each_entry.product.name,
                        each_entry.amount,
                        each_entry.product.whole_workload[each_wc.id],
                        whole_workload)

        return report

    @property
    def calculate_workcenter_workload(self):
        report = {}
        for each_work_center in self.factory:
            report[each_work_center.id] = each_work_center.workload(self.plan)
        return report

    @property
    def product_entry_labor_report(self):
        report = {}
        for entry in self.plan:
            report[entry.product] = entry.labor
        return sorted(report.items(), key=lambda t: t[1], reverse=True)

    def get_workcenters_workload(self, date_from=datetime.min,
                                 date_to=datetime.max):
        report = defaultdict(float)
        # plan = filter(lambda x: date_from < x.date <= date_to, self.plan)
        # for entry in sorted(self.plan, key=lambda i: i.date, reverse=True):
        for entry in reversed(self.plan.plan_entry):
            if not date_from < entry.date <= date_to:
                break
            for wc_id in entry.product.whole_workload:
                report[wc_id] += entry.product.whole_operation_workload[
                                     wc_id] * entry.amount
                report[wc_id] += entry.product.whole_preparing_workload[
                                     wc_id] * entry.number_of_batches

        return report

    def add_operation(self, product_id, wc_id, t_pz=0.0, t_sht=0.0):
        product = self.entities.get_product_with_id(product_id=product_id)
        wc = self.factory.get_workcenter_with_id(wc_id)

        if wc is not None:
            product.tech_proc.add_operation(wc, t_pz=t_pz, t_sht=t_sht)

    def read_tech_from_xlsx(self, xlsx, tech_columns=None,
                            tech_worksheet="tech"):
        from bfg_tools.library import xlsx_reader

        if tech_columns is None:
            tech_columns = self.TECH_COLUMNS

        try:
            xlsx_data = xlsx_reader.XlsxReadData(xlsx=xlsx,
                                                 worksheet=tech_worksheet,
                                                 columns=tech_columns).worksheets[
                tech_worksheet]
        except exceptions.InvalidFileException:
            xlsx_data = csv_reader.CsvReadData(csv_file=xlsx,
                                               columns=tech_columns).rows

        unique_op_id = []

        for row in xlsx_data:
            if row["ROW_NUMBER"] == 1:  # Пропускаем строку с заголовком
                continue

            if row["OP_ID"] in unique_op_id:
                print(
                    "Операция с ID {} встретилась повторно и была проигнорирована".format(
                        row["OP_ID"]))
                continue
            unique_op_id.append(row["OP_ID"])

            product_id = row["PRODUCT"]
            try:
                if type(row["T_SHT"]) is str:
                    tsht = float(row["T_SHT"].replace(",", "."))
                else:
                    tsht = float(row["T_SHT"])
            except (ValueError, TypeError):
                tsht = 0
                if row["T_SHT"] is not None:
                    print("Неверное значение ячейки столбца T_SHT в строке {}, "
                          "значение в ячейке {}".format(row["ROW_NUMBER"],
                                                        row["T_SHT"]))
            try:
                if type(row["T_PZ"]) is str:
                    tpz = float(row["T_PZ"].replace(",", "."))
                else:
                    tpz = float(row["T_PZ"])
            except (ValueError, TypeError):
                tpz = 0
                if row["T_PZ"] is not None:
                    print("Неверное значение ячейки столбца T_PZ в строке {}, "
                          "значение в ячейке {}".format(row["ROW_NUMBER"],
                                                        row["T_PZ"]))
            wc_id = str(row["EQUIPMENT"]) + "_" + str(row["DEPARTMENT"])
            product = self.entities.get_product_with_id(product_id)
            if product is not None:
                self.add_operation(product_id, wc_id, t_pz=tpz / 60,
                                   t_sht=tsht / 60)

    def is_moveto_allowed(self, entry, time_hours, from_plan, max_product_share,
                          current_date, wc_share=False,
                          factory_workload=None, basic_plan=None, log=False):
        """
        проверяем можно ли изделие из указанной строки плана перекинуть в целевой план
        загрузку рабочих центров проверяем по периоду от текущей даты на time_hours часов назад
        :param entry: строка плана, изделие из которой (в размере батча или остатка) хотим перекинут в новый план
        :param from_plan: из этого плана забираем это изделие
        :param time_hours: шаг плана
        :param max_product_share: ограничение, что на каждом ресурсе доля одного изделия должна быть не более этого
         значения
        :param current_date: дата, на которую проверяем возможность добавления изделий в план
        :param wc_share: автоматически расчитанное ограничение на загрузку станков одной номенклатурой
        :param basic_plan: исходный план, чтоб посчитать wc_share
        :param factory_workload: загрузка всех станков, чтоб каждый раз не пересчитывать
        :param log: сыпем ли подробно в консоль информацию по принятому решению
        :return: на выходе True, если изделие можно добавить в план и False, если нельзя
        """
        date_from = current_date - timedelta(time_hours / 24)
        date_to = current_date

        if basic_plan is None:
            basic_plan = from_plan

        # если изделия уже нет в плане, то его и нельзя переместить
        if entry not in from_plan:
            if log:
                print(
                    "Изделие {} в плане закончилось".format(entry.product.name))
            return False
        if factory_workload is None:
            factory_workload = self.get_workcenters_workload(
                date_from=date_from, date_to=date_to)

        # проверяем по каждому станку соблюдение всех условий загрузки сортируем от самого загруженного
        for workcenter in self.factory.workcenter_sorted_list(basic_plan):

            wc_workload = factory_workload[workcenter.id]
            batch = min(entry.product.batch_size, entry.amount)

            #  Фонд допустимой загрузки станка
            wc_time_fund = time_hours * workcenter.utilization_factor * workcenter.amount
            #  Вычтем из фонда доступного времени перегрузку прошлого дня
            wc_time_fund_without_overhead = wc_time_fund - \
                                            self.overload[workcenter.id][-1]

            if (wc_workload > wc_time_fund_without_overhead) and \
                    (entry.product.whole_workload[workcenter.id] > 0):
                # даже если станок перегружен, но изделие не занимает этот станок совсем, то его (изделие)
                # можно в план включить
                if log:
                    print("Изделие {} из заказа {} не будет "
                          "добавлено в план потому, что станок {} уже перегружен".format(
                        entry.product.code,
                        entry.order_name,
                        workcenter.name))
                return False

            if wc_share:
                try:
                    # Определяем сколько станков можем занять одной номенклатурой
                    tmp_ = workcenter.amount * (
                                entry.product.whole_workload[workcenter.id] *
                                basic_plan.amount_of_products(entry.product) /
                                workcenter.workload(basic_plan))
                    wc_amount = math.ceil(
                        tmp_)  # округление до ближайшего большего числа
                    if wc_amount == 0:
                        wc_amount = workcenter.amount
                except ZeroDivisionError:
                    wc_amount = workcenter.amount
                # сколько добавляемое изделие уже загружает этот станок за рассматриваемый период
                # сравниваем с предельной загрузкой расчетного количества станков
                if entry.product.whole_workload[workcenter.id] * \
                        (self.plan.amount_of_products(entry.product,
                                                      date_from=date_from,
                                                      date_to=date_to) + batch) \
                        > time_hours * workcenter.utilization_factor * wc_amount:
                    if log:
                        print(
                            "Изделие {} не будет добавлено в план потому, что ему "
                            "нельзя загружать более {} станков в классе {}".format(
                                entry.product.code,
                                wc_amount,
                                workcenter.name))
                    return False

            # сколько добавляемое изделие уже загружает этот станок за рассматриваемый период
            # сравниваем с предельной загрузкой с учетом максимальной загрузки станка одной номенклатурой
            if entry.product.whole_workload[workcenter.id] * \
                    self.plan.amount_of_products(entry.product,
                                                 date_from=date_from,
                                                 date_to=date_to) \
                    > time_hours * workcenter.utilization_factor * max_product_share * workcenter.amount:
                # проверяем соответствует ли условию по ограничению занятия ресурса одной номенклатурой
                if log:
                    print(
                        "Изделие {} не будет добавлено в план потому, что ему "
                        "нельзя загружать рабочий центр {} с долей больше {}".format(
                            entry.product.code,
                            workcenter.name,
                            max_product_share))
                return False

        return True

    def get_exploded_session(self, rules, from_date=datetime.min,
                             to_date=datetime.max):
        """
        метод проведения разбиения плана по различным логикам
        :param from_date начала диапазона, из которого можно брать заказы в распределение
        :param to_date конец диапазона, из которого можно брать заказы в распределение
        :param rules: словарь с правилами планирования
        ["step"] шаг планирования
        ["recalculate_priority"]: пересчитываем ли приоритеты строк плана на каждом шаге
        ["max_product_share"]: ограничение на занятие ресурса одной номенклатурой
        ["with_overhead"]: Планируем "с перебором" или с недобором
        ["deep_steps"]: на какую глубину плана анализируем загрузку рабочих центров
        ["start_date"]: стартовая дата начала выполнения плана
        ["simplify_plan"]: схлопывать ли план до деталей перед расчетом
        :return: на выходе расчетная сессия и количество дней,
            которое понадобилось чтоб запустить все изделия в производство
        """
        from time import time
        print("Начинаем формирование плана запуска")
        step_duration_hours = rules["step"]
        log = rules["log"]
        max_product_share = rules["max_product_share"]
        if from_date == datetime.min:
            start_date = rules["start"]
        else:
            start_date = from_date  # если задано from_date, то с него начинаем распределение, "start" игнорируем
        wc_share = rules["wc_share"]
        manual_priority = rules["manual_priority"]
        simplify_plan = rules["simplify_plan"]
        collapse_plan = rules["collapse_plan"]

        work_plan = self.plan.copy(from_date, to_date)
        basic_plan = self.plan.copy(from_date, to_date)

        if simplify_plan:
            work_plan = work_plan.simple_plan()
            # for entry in work_plan:
            #     print(entry.product, entry.amount)
        # создаем временную копию плана, чтоб с ней проводить манипуляции, а основной план оставить в исходном виде
        result_session = CalcSession(factory=self.factory,
                                     plan=Plan(name="план после explosion",
                                               entities=self.entities))
        hour = 0
        # запускаем счетчик часов и шагов

        if collapse_plan:
            work_plan.collapse()
        # # схлопываем план перед разбиением (все одинаковые изделия сводим в одну строку)

        # сортируем план по приоритету (в данном случае по загрузке УМ)
        if not manual_priority:
            sorted_plan = work_plan.sorted_by_bottleneck_plan_entry_set(
                self.factory, accuracy=rules["priority_accuracy"])
        else:
            sorted_plan = work_plan.plan_entry
        total_plan_amount = work_plan.total_amount

        for workcenter in result_session.factory.workcenter:
            result_session.overload.update({workcenter.id: [0]})

        holidays = [x.date() for x in rules["holidays"]]

        while work_plan.plan_entry:
            t = time()
            current_date = start_date + timedelta(hour / 24)
            while current_date.date() in holidays or current_date.isoweekday() in \
                    rules["day-off"]:
                hour += 1
                current_date += timedelta(1 / 24)
            date_from = current_date - timedelta(step_duration_hours / 24)
            date_to = current_date
            factory_workload = result_session.get_workcenters_workload(
                date_from=date_from, date_to=date_to)
            # здесь надо посчитать перегрузку с прошлого периода, которую надо учесть в этом периоде
            # перебираем строки плана в исходной сессии в порядке приоритета
            not_allowed_list = []
            for entry in sorted_plan:
                if entry.product in not_allowed_list:
                    continue
                entry_start_date = calculate_start_date(finish=entry.date,
                                                        work_time_hours=rules[
                                                                            "max_advance_period"] * 24,
                                                        holidays=rules[
                                                            "holidays"],
                                                        day_off=rules[
                                                            "day-off"])
                if entry_start_date > current_date:
                    continue
                if entry.amount == 0:
                    continue
                # проверяем можно ли еще один батч закинуть в план
                number_of_batches = entry.min_number_of_batches
                while result_session.is_moveto_allowed(entry=entry,
                                                       from_plan=work_plan,
                                                       time_hours=step_duration_hours,
                                                       max_product_share=max_product_share,
                                                       current_date=current_date,
                                                       wc_share=wc_share,
                                                       factory_workload=factory_workload,
                                                       basic_plan=basic_plan,
                                                       log=log):
                    # если влазит еще один батч (по загрузке оборудования), то добавляем
                    work_plan.moveto(entry=entry,
                                     other_plan=result_session.plan,
                                     date=(start_date + timedelta(hour / 24)),
                                     log=log,
                                     number_of_batches=number_of_batches)
                    number_of_batches = 1
                    # и пересчитываем загрузку
                    factory_workload = result_session.get_workcenters_workload(
                        date_from=date_from, date_to=date_to)
                if entry in work_plan:
                    not_allowed_list.append(entry.product)
            not_allowed_list = []
            if max_product_share != 1 or wc_share:
                for entry in sorted_plan:
                    if entry.product in not_allowed_list:
                        continue

                    entry_start_date = calculate_start_date(finish=entry.date,
                                                            work_time_hours=
                                                            rules[
                                                                "max_advance_period"] * 24,
                                                            holidays=rules[
                                                                "holidays"],
                                                            day_off=rules[
                                                                "day-off"])
                    if entry_start_date > current_date:
                        continue

                    if entry.amount == 0:
                        continue
                    # проверяем можно ли еще один батч закинуть в план
                    while result_session.is_moveto_allowed(entry=entry,
                                                           from_plan=work_plan,
                                                           time_hours=step_duration_hours,
                                                           max_product_share=1,
                                                           current_date=current_date,
                                                           wc_share=False,
                                                           factory_workload=factory_workload,
                                                           basic_plan=basic_plan,
                                                           log=log):
                        # если влазит еще один батч (по загрузке оборудования), то добавляем
                        work_plan.moveto(entry=entry,
                                         other_plan=result_session.plan,
                                         date=(start_date + timedelta(
                                             hour / 24)), log=log)
                        # и пересчитываем загрузку
                        factory_workload = result_session.get_workcenters_workload(
                            date_from=date_from, date_to=date_to)
                    if entry in work_plan:
                        not_allowed_list.append(entry.product)

            work_hours = 0

            while work_hours < step_duration_hours:
                hour += 1  # счетчик часов
                calc_date = start_date + timedelta(hour / 24)
                if calc_date.isoweekday() not in rules[
                    "day-off"] and calc_date.date() not in holidays:
                    work_hours += 1
            for workcenter in result_session.factory.workcenter:
                wc_id = workcenter.id
                #  считаем перегрузку текущего шага (если отрицательное число, то недогрузка)
                overload = factory_workload[wc_id]
                overload -= step_duration_hours * workcenter.utilization_factor * workcenter.amount

                #  если текущая перегрузка плюс вчерашняя больше нуля, то сохраняем ее и учтем на следующий шаг
                if overload + result_session.overload[wc_id][-1] > 0:
                    result_session.overload[wc_id].append(
                        overload + result_session.overload[wc_id][-1])
                #  если меньше нуля, то недогрузка на следующий день не переносится
                else:
                    result_session.overload[wc_id].append(0)

            print(hour, " часов", current_date)
            print("Прогресс: {:.2%}".format(
                result_session.plan.total_amount / total_plan_amount))
            print(
                "Формирование плана на один шаг заняло {:.2f} секунд \n".format(
                    time() - t))
        print("План запуска сформирован, все изделия плана "
              "будут запущены в производство за {} часов".format(str(hour)))
        report = result_session.calculate_workcenter_utilization(
            period_hours=hour)
        for row in report:
            print(row, end=" ")

        return result_session

    def get_exploded_session_with_separated_months(self, rules):
        result_session = CalcSession(entities=self.entities,
                                     factory=self.factory)

        from_date = self.plan.start_date

        while from_date <= self.plan.finish_date:
            to_date = datetime(from_date.year + from_date.month // 12,
                               ((from_date.month % 12) + 1), 1)
            print(from_date, to_date)
            exploded_session = self.get_exploded_session(rules,
                                                         from_date=from_date,
                                                         to_date=to_date)
            result_session.plan.concatenate_with(exploded_session.plan)
            from_date = datetime(from_date.year + int(from_date.month / 12),
                                 ((from_date.month % 12) + 1), 1)

        return result_session
