import sys
from datetime import datetime

import yaml

from bfg_tools.library import xlsx_writer
from bfg_tools.library.casting_calc_session import CalcSession, \
    optimize_schedule
from bfg_tools.library.casting_data.casting_plan import Plan
from bfg_tools.library.casting_data.casting_shop import CastingShop


class ConfigChapter(object):

    def __init__(self, data, columns=()):
        self.columns = {}
        self.file = data['file']

        if 'worksheet' in data:
            self.worksheet = data['worksheet']
        else:
            self.worksheet = 'Sheet 1'

        if 'period' in data:
            self.period = data['period']

        if 'columns' in data:
            data_columns = data['columns']
            for key in columns:
                self.columns[key] = data_columns[key]


class CastingConfig(object):
    PLAN_COLUMNS = ('AMOUNT', 'DATE', 'CODE')

    STORE_COLUMNS = ('AMOUNT', 'ALLOY')

    SPEC_COLUMNS = (
        'DIAMETER',
        'ALLOY',
        'NAME',
        'CHARGE_NAME',
        'CHARGE_IDENTITY',
        'CHARGE_AMOUNT'
    )

    TECH_COLUMNS = ('CODE', 'EQUIPMENT_ID', 'CASTING_TIME_HR')

    OUTPUT_COLUMNS = ('ORDER', 'IDENTITY', 'NAME', 'AMOUNT', 'DATE', 'CODE')

    EQUIPMENT_COLUMNS = (
        'ID',
        'EQUIPMENT_ID',
        'NAME',
        'BATCH_SIZE',
        'INITIAL_ALLOY'
    )

    SETUPS_COLUMNS = ('EQUIPMENT_ID', 'FROM', 'TO', 'AMOUNT')

    SCHEDULE_COLUMNS = (
        'EQUIPMENT_ID',
        'ALLOY',
        'AMOUNT',
        'SETUP_TIME',
        'MACHINE_TIME',
        'START_TIME'
    )

    def __init__(self, config):
        with open(config, 'r', encoding='utf-8') as stream:
            data = yaml.load(stream)

        self.plan = ConfigChapter(data['plan'], self.PLAN_COLUMNS)

        self.store = ConfigChapter(data['store'], self.STORE_COLUMNS)

        self.spec = ConfigChapter(data['spec'], self.SPEC_COLUMNS)

        self.tech = ConfigChapter(data['tech'], self.TECH_COLUMNS)

        self.setups = ConfigChapter(data['setups'], self.SETUPS_COLUMNS)

        self.equipment = ConfigChapter(
            data['equipment'],
            self.EQUIPMENT_COLUMNS
        )

        self.output = ConfigChapter(data['output'], self.OUTPUT_COLUMNS)

        self.rules = data['rules']

        self.rules['plan_from'] = datetime.strptime(
            self.rules['plan_from'],
            '%Y/%m/%d'
        )

        self.rules['plan_to'] = datetime.strptime(
            self.rules['plan_to'],
            '%Y/%m/%d'
        )

        self.rules['start_date'] = datetime.strptime(
            self.rules['start_date'],
            '%Y/%m/%d'
        )


if __name__ == '__main__':

    config = CastingConfig('casting_scheduling.yml')

    shop = CastingShop('1_2')
    shop.read_equipment_from_xlsx(
        xlsx=config.equipment.file,
        worksheet=config.equipment.worksheet
    )

    for each_equipment in shop.casting_machines:
        print(each_equipment, shop.casting_machines[each_equipment])

    plan = Plan(name='План', casting_shop=shop)

    plan.read_spec_from_xlsx(
        xlsx=config.spec.file,
        worksheet=config.spec.worksheet
    )

    plan.read_tech_from_xlsx(
        xlsx=config.tech.file,
        worksheet=config.tech.worksheet
    )

    plan.read_setups_from_xlsx(
        xlsx=config.setups.file,
        worksheet=config.setups.worksheet
    )

    plan.read_plan_from_xlsx(
        xlsx=config.plan.file,
        worksheet=config.plan.worksheet,
        date_from=config.rules['plan_from'],
        date_to=config.rules['plan_to']
    )

    calc_session = CalcSession(
        plan=plan,
        no_setup_number_of_batches=config.rules['no_setup_number_of_batches']
    )

    if config.store.file is not None:
        calc_session.read_store_from_xlsx(
            xlsx=config.store.file,
            worksheet=config.store.worksheet
        )

    time_fund = config.rules['time_fund'] - 50
    setup_volume_from = 0
    batch_size = 24000 * 4
    while True:
        best_result = {
            'time_fund': 0,
            'setup_volume_from': 0,
            'batch_size': 0,
            'total_setup_volume': sys.maxsize
        }
        while True:
            while True:
                best_schedule, entry_bin = calc_session.schedule_generator(
                    time_fund=time_fund,
                    volume_from=setup_volume_from,
                    volume_to=config.rules['setup_volume_to'],
                    batch_size=batch_size,
                    reverse=True
                )

                if not entry_bin:
                    optimize_schedule(best_schedule, time_fund)
                    total_setup_volume = 0
                    for each_equipment in best_schedule:
                        total_setup_volume += round(
                            best_schedule[each_equipment].setup_volume
                        )
                    if total_setup_volume < best_result['total_setup_volume']:
                        best_result['time_fund'] = time_fund
                        best_result['total_setup_volume'] = total_setup_volume
                        best_result['setup_volume_from'] = setup_volume_from
                        best_result['batch_size'] = batch_size
                        best_result['reverse'] = True
                        print(
                            total_setup_volume,
                            time_fund,
                            setup_volume_from,
                            batch_size,
                            True
                        )

                best_schedule, entry_bin = calc_session.schedule_generator(
                    time_fund=time_fund,
                    volume_from=setup_volume_from,
                    volume_to=config.rules['setup_volume_to'],
                    batch_size=batch_size,
                    reverse=False
                )

                if not entry_bin:
                    optimize_schedule(best_schedule, time_fund)
                    total_setup_volume = 0
                    for each_equipment in best_schedule:
                        total_setup_volume += round(
                            best_schedule[each_equipment].setup_volume)
                    if total_setup_volume < best_result['total_setup_volume']:
                        best_result['time_fund'] = time_fund
                        best_result['total_setup_volume'] = total_setup_volume
                        best_result['setup_volume_from'] = setup_volume_from
                        best_result['batch_size'] = batch_size
                        best_result['reverse'] = False
                        print(
                            total_setup_volume,
                            time_fund,
                            setup_volume_from,
                            batch_size,
                            False
                        )

                if setup_volume_from > config.rules['setup_volume_from']:
                    setup_volume_from = 0
                    break
                setup_volume_from += 500
            batch_size = batch_size // 2
            if batch_size <= 2400:
                batch_size = 24000 * 4
                break
        if best_result['time_fund'] == time_fund:
            best_schedule, entry_bin = calc_session.schedule_generator(
                time_fund=best_result['time_fund'],
                volume_from=best_result['setup_volume_from'],
                volume_to=config.rules['setup_volume_to'],
                batch_size=best_result['batch_size'],
                reverse=best_result['reverse']
            )
            optimize_schedule(best_schedule, best_result['time_fund'])

            total_setup_volume = 0
            total_machine_volume = 0
            report = []
            report_per_batch = []
            frozen_period_report = {
                'tasks': [],
                'setup_state': [],
                'not_done': [],
                'store': []
            }

            calc_session.calculate_store_after_demand()

            for each_equipment in best_schedule:
                print('\n\n', each_equipment)
                print('Итоговые результаты плана: время переналадки {} ч, '
                      'объем переходных плавок {} кг, время работы {} ч'
                      .format(round(best_schedule[each_equipment].setup_time),
                              round(best_schedule[each_equipment].setup_volume),
                              round(
                                  best_schedule[each_equipment].machine_time)))

                total_setup_volume += round(
                    best_schedule[each_equipment].setup_volume
                )

                total_machine_volume += round(
                    best_schedule[each_equipment].machine_volume
                )
                print(best_schedule[each_equipment])
                report_per_batch.extend(
                    best_schedule[each_equipment].generate_per_batch_report(
                        config.rules['start_date']
                    )
                )

                report.extend(
                    best_schedule[each_equipment].generate_report(
                        config.rules['start_date']
                    )
                )

                froz_rep = best_schedule[
                    each_equipment].frozen_period_result_report(
                    config.rules['start_date'],
                    config.rules['frozen_hours'],
                    calc_session.store
                )

                frozen_period_report['tasks'].extend(froz_rep['tasks'])
                frozen_period_report['setup_state'].append(
                    froz_rep['setup_state'])
                frozen_period_report['not_done'].extend(froz_rep['not_done'])

            for alloy in calc_session.store:
                frozen_period_report['store'].append({
                    'ALLOY': alloy.name,
                    'AMOUNT': calc_session.store[alloy]
                })

            print('Общий объем потерь на наладку {} кг, Всего отлито {} кг'
                  .format(total_setup_volume, total_machine_volume))

            xlsx_writer.xlsx_write_data(
                data=report,
                xlsx='{}{}.xlsx'.format(config.output.file, str(time_fund)),
                worksheet=config.output.worksheet
            )

            xlsx_writer.xlsx_write_data(
                data=report_per_batch,
                xlsx='{}{}.xlsx'.format(config.output.file, str(time_fund)),
                worksheet='{}_per_batch'.format(config.output.worksheet),
                overwrite=False
            )

            xlsx_writer.xlsx_write_data(
                data=frozen_period_report['setup_state'],
                xlsx='{}{}.xlsx'.format(config.output.file, str(time_fund)),
                worksheet='equipment',
                overwrite=False
            )

            xlsx_writer.xlsx_write_data(
                data=frozen_period_report['tasks'],
                xlsx='{}{}.xlsx'.format(config.output.file, str(time_fund)),
                worksheet=config.output.worksheet + '_done',
                overwrite=False
            )

            xlsx_writer.xlsx_write_data(
                data=frozen_period_report['not_done'],
                xlsx='{}{}.xlsx'.format(config.output.file, str(time_fund)),
                worksheet=config.output.worksheet + '_not_done',
                overwrite=False
            )

            xlsx_writer.xlsx_write_data(
                data=frozen_period_report['store'],
                xlsx='{}{}.xlsx'.format(config.output.file, str(time_fund)),
                worksheet=config.output.worksheet + '_store',
                overwrite=False
            )
        if (
                time_fund > config.rules['time_fund'] + 50
        ) and (
                best_result['total_setup_volume'] < sys.maxsize
        ):
            break

        time_fund += 10
        print(time_fund)

    # best_schedule, entry_bin = calc_session.schedule_generator(
    #     time_fund=best_result['time_fund'],
    #     volume_from=best_result['setup_volume_from'],
    #     volume_to=config.rules['setup_volume_to'],
    #     batch_size=best_result['batch_size'],
    #     reverse=best_result['reverse']
    # )

    # for each_equipment in best_schedule:
    #     print('\n\n', each_equipment)
    #     print('Итоговые результаты плана: время переналадки {} ч, '
    #           'объем переходных плавок {} кг, время работы {} ч'
    #           .format(round(best_schedule[each_equipment].setup_time),
    #                   round(best_schedule[each_equipment].setup_volume),
    #                   round(best_schedule[each_equipment].machine_time)))
    #     print(best_schedule[each_equipment])

    # optimize_schedule(best_schedule, best_result['time_fund'])
