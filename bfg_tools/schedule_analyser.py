from bfg_tools.library import analyse_transactions, xlsx_writer, yml_config


def main():
    cur_config = yml_config.ScheduleAnalyserConfig("schedule_analyser.yml")

    session = analyse_transactions.TransactionSession()
    session.read_from_xlsx(xlsx=cur_config.transactions.file,
                           worksheet=cur_config.transactions.worksheet,
                           columns=cur_config.transactions.columns,
                           sum_batch_size_in_subtasks=cur_config.rules[
                               "sum_subtask_size"])
    date_from = cur_config.rules["from"]
    date_to = cur_config.rules["to"]

    plan = sorted(session.get_plan(date_from, date_to), key=lambda i: i["DATE"])
    xlsx_writer.xlsx_write_data(xlsx=cur_config.output_plan.file,
                                worksheet=cur_config.output_plan.worksheet,
                                data=plan)

    launch_report = sorted(session.launch_report(date_from, date_to),
                           key=lambda i: i["DATE"])
    xlsx_writer.xlsx_write_data(xlsx=cur_config.output_production_launch.file,
                                worksheet=cur_config.output_production_launch.worksheet,
                                data=launch_report)

    release_report = sorted(session.release_report(date_from, date_to),
                            key=lambda i: i["DATE"])
    xlsx_writer.xlsx_write_data(xlsx=cur_config.output_production_release.file,
                                worksheet=cur_config.output_production_release.worksheet,
                                data=release_report)

    equipment_report = sorted(session.equipment_report(date_from, date_to),
                              key=lambda i: i["EQUIPMENT_ID"])
    xlsx_writer.xlsx_write_data(xlsx=cur_config.output_equipment.file,
                                worksheet=cur_config.output_equipment.worksheet,
                                data=equipment_report)

    product_report = sorted(session.product_report(date_from, date_to),
                            key=lambda i: i["NUMBER_OF_BATCHES"],
                            reverse=True)
    xlsx_writer.xlsx_write_data(xlsx=cur_config.output_product.file,
                                worksheet=cur_config.output_product.worksheet,
                                data=product_report)

    # for product in session.products.values():
    #     equipment_report = sorted(session.equipment_report(year, month, product), key=lambda i: i["EQUIPMENT_ID"])
    #     if len(equipment_report) == 0:
    #         continue
    #     xlsx_writer.xlsx_write_data(xlsx="../output/equipment_report_{}.xlsx".format(product.product_id),
    #                                 worksheet="plan", data=equipment_report)


main()
