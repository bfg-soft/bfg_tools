from datetime import timedelta

from bfg_tools.library import calc, xlsx_writer, yml_config


def main():
    cur_config = yml_config.PlanExplosionConfig("plan_explosion.yml")

    session = calc.CalcSession(entities=calc.Entities())
    # читаем все данные
    session.entities.read_spec_from_xlsx(
        cur_config.spec.file,
        cur_config.spec.columns,
        cur_config.spec.worksheet
    )
    session.factory.read_from_xlsx(
        cur_config.equipment.file,
        cur_config.equipment.columns,
        cur_config.equipment.worksheet
    )
    session.read_tech_from_xlsx(
        cur_config.tech.file,
        cur_config.tech.columns,
        cur_config.tech.worksheet
    )
    session.entities.read_batch_size_from_xlsx(
        cur_config.batch_size.file,
        cur_config.batch_size.columns,
        cur_config.batch_size.worksheet
    )
    session.plan.read_plan_from_xlsx(
        cur_config.plan.file,
        cur_config.plan.columns,
        cur_config.plan.worksheet,
        default_batch_operation_hours=cur_config.rules["default_batch_size"]
    )

    print(
        session.big_workload_report(
            cur_config.rules["start"],
            cur_config.rules["holidays"],
            cur_config.rules["day-off"],
            cur_config.rules["priority_accuracy"]
        )
    )

    if cur_config.rules["separate_months"]:
        result_session = session.get_exploded_session_with_separated_months(
            cur_config.rules
        )
    else:
        result_session = session.get_exploded_session(
            cur_config.rules
        )

    # print("Аналитика по загрузке оборудования по дням плана (по запуску)")
    report = []
    for wc in result_session.factory.workcenter_sorted_list(
            result_session.plan
    ):
        # print(wc.name)
        row = {
            "WORKCENTER": "{} - {} ({} шт, КИ {})".format(
                wc.id,
                wc.name,
                str(wc.amount),
                str(round(wc.utilization_factor, 2))),
            "NAME": wc.name,
            "AMOUNT": wc.amount,
            "UTILIZATION": round(wc.utilization_factor, 2)
        }

        i = 0
        while cur_config.rules["start"] + timedelta(i) <= \
                result_session.plan.finish_date:
            period_from = cur_config.rules["start"] + timedelta(i - 1)
            period_to = cur_config.rules["start"] + timedelta(i)
            wc_workload = wc.workload(result_session.plan,
                                      period_from, period_to) / 24 / wc.amount
            # if wc_workload > 0:
            # print("День {} ({}): {:.2%}".format(i, period_to, wc_workload))
            row[period_to.date()] = round(wc_workload * 100, ndigits=2)
            i += 1
        report.append(row)

    result_session.plan.write_to_xlsx(
        xlsx=cur_config.output.file,
        worksheet=cur_config.output.worksheet
    )

    xlsx_writer.xlsx_write_data(
        xlsx=cur_config.output.file,
        worksheet="wc_workload",
        data=report,
        overwrite=False
    )


main()
