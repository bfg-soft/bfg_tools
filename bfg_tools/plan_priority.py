from bfg_tools.library import plan_transformer, yml_config


def main():
    cur_config = yml_config.PlanPriorityConfig("plan_priority.yml")
    initial_plan = plan_transformer.Plan()
    initial_plan.read_plan_from_xlsx(cur_config.plan.file,
                                     cur_config.plan.columns,
                                     cur_config.plan.worksheet)
    initial_plan.read_priority_from_xlsx(cur_config.special_orders.file,
                                         cur_config.special_orders.columns,
                                         cur_config.special_orders.worksheet)

    result_plan = initial_plan.sort_order()
    result_plan.write_to_csv(csv_file=cur_config.output.file)


if __name__ == '__main__':
    main()
