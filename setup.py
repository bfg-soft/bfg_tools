from setuptools import find_packages, setup

setup(
    name='bfg_tools',
    version='0.1.0',
    packages=find_packages(),
    url='-',
    license='-',
    author='BFG-Soft LLC',
    author_email='info@bfg-soft.ru',
    description='Plan explosion ultra logic.',
    install_requires=[
        'requests',
        'PyYAML',
        'python-dateutil',
        'openpyxl',
        'requests_toolbelt',
    ]
)
